﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="listas.aspx.cs" Inherits="Insigte.listas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function chkconfirm() {
        return confirm("<%= getResource("ConfApagarAutor") %>");
    }

</script>
<style type="text/css">
    
    a
    {
        text-decoration:none;
        color: #000;
    }
    
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    } 
    
    .item_autor
    {
        float:left;
        margin: 2px 0px 2px 0px;
        width: 100%;
        border: 2px solid #FF0000;
        background-color: #ffffff;
    }   
    .items
    {
        float:left;
        width: 100%;
        background-color: #ffffff;
    }
    .doc_link
    {
        float:left;
        height:64px;
        width:100%;
        color: #000000;
        line-height:1.5em;
    }
    
    .doc_link:hover
    {
        background-color: #f0f0f0;
    }
    
    .doc_link a
    {
        color: #000000;
    }
    .social
    {
        float:left;
        width:100%;
    }
    .firstDesc
    {
        float:left;
        margin: 5px 2px 2px 2px;
    }
</style>


<script type="text/javascript">
    $(function () {
        $('.show_on_hover').hide();
        $('.doc_link').hover(function () {
            $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
        },
        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
        <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
            <div style="margin-top:12px">
                <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
            </div>
        </div>
        <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
            <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
                <div id="caixa" style="float:left;width:80%;">
                    &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" <%$Resources:insigte.language,LTprList%>" />
                </div>
                <div id="help" style="float:left;width:20%; ">
                    <div style="float:right;padding-right:5px;">
                        <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                    </div>
                </div>
            </div>

            <div style="float:left;width:100%;">
                <div style="margin: 5px 10px 5px 10px">
                    <asp:DropDownList ID = "DDLists" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLists_OnSelectedIndexChanged" ></asp:DropDownList>
                    <BR>
                    <BR>
                    <asp:label ID="gerirlistas" runat="server" Text="<%$Resources:insigte.language,PRsemlistas%>"></asp:label>
                    <asp:LinkButton ID="gerirlistas2" runat="server" Text="<%$Resources:insigte.language,PRsemlistas2%>" OnClientClick="openWindow('listscfg.aspx')" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Underline="true" Font-Bold="true"></asp:LinkButton>
                    <asp:label ID="gerirlistas3" runat="server" Text="<%$Resources:insigte.language,PRsemlistas3%>"></asp:label>
                </div>
            </div>


            <div style="float:left; width:100%; ">
                <div style="margin: 5px 5px 5px 5px; ">
                    <asp:Repeater runat="server" ID="rpt_lists">
                        <ItemTemplate>
                            <div class="doc_link">
                                <div>
                                    <div style="float:left;width:8%;height:32px;">
                                        <div>
                                            <img src='<%# "../ficheiros/autores/" + Eval("photo") %>' alt="" style="height:62px; width:100%;" />
                                        </div>
                                    </div>
                                    <div style="float:left;width:1%;height:32px;">
                                        <div>
                                            &nbsp;
                                        </div>
                                    </div>
                                    <div style="float:left; width:27%;">
                                        <div class="firstDesc">
                                            <%--<asp:LinkButton runat="server" ID="lnk" OnCommand="lnk_download_Command" CommandArgument=''><%# DataBinder.Eval(Container.DataItem, "name") %></asp:LinkButton><br />--%>
                                            <%# DataBinder.Eval(Container.DataItem, "name") %><br />
                                            <small><%# DataBinder.Eval(Container.DataItem, "email") %></small><br />
                                            <small><%# DataBinder.Eval(Container.DataItem, "funcao") %></small>
                                        </div>
                                    </div>
                                    <div style="float:left; width:30%; text-align:left;">
                                        <div class="firstDesc">
                                            <div style="padding:2px 2px 2px 2px;">
                                                <small><%# DataBinder.Eval(Container.DataItem, "description") %></small>
                                            </div>
                                        </div>    
                                    </div>
                                    <div style="float:left; width:20%; text-align:left;">
                                        <div class="firstDesc">
                                            <small><%# getEditors(Eval("authorid").ToString()) %></small>
                                        </div>    
                                    </div>
                                    <div style="float:left; width:11%; text-align:right">
                                        <div class="social">
                                            <div style="margin: 5px 2px 2px 0px;">
                                                <%# getSocial(Eval("authorid").ToString()) %>
                                            </div>
                                        </div>
                                    </div>
                                    <div style="float:right; width:3%; text-align:right">
                                        <div class="social">
                                            <div style="margin: 5px 2px 2px 0px;">
                                                <asp:LinkButton runat="server" ID="Lnk_delete" OnClientClick='return chkconfirm();' OnCommand="lnk_delete_Command" CommandArgument='<%# Eval("authorid") %>'><img src="Imgs/icons/black/png/delete_icon_16.png" alt="" height="16px" width="16px" /></asp:LinkButton>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            
                            <%--<%# getMorangos(Eval("ID_AUTOR").ToString())%>--%>
                            
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
            <div style="float:left; width:100%; margin: 20px 0px 20px 0px;">
                <div style="margin: 20px 5px 20px 5px; ">

                    <div style="float:left; width:20%; ">
                        <asp:ImageButton runat="server" ID="ImageButton1" Text="Exportar Excel" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />
                        <asp:LinkButton runat="server" ID="LinkButton1" OnClick="btnExportXML_Click" Text="Exportar Excel" Font-Names="Arial" Font-Size="12px" Font-Underline="false" ForeColor="#000000"/>
                    </div>
                    <div style="float:left; width:20%; ">
                        <asp:ImageButton runat="server" ID="ImageButton2" Text="Exportar Excel" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />
                        <asp:Label ID="MailTo" runat="server" ></asp:Label>
                    </div>

                </div>
                <div style="margin: 20px 5px 20px 5px; ">
                </div>
            </div>
        </div>
    </div>
</asp:Content>
