﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="Insigte._Default" %>
<asp:Content ID="HeaderContent" runat="server" ContentPlaceHolderID="HeadContent">
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>
</asp:Content>
<asp:Content ID="BodyContent" runat="server" ContentPlaceHolderID="MainContent">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgNewsACN" AutoGenerateColumns="false" AllowPaging="True" 
        GridLines="None" BorderWidth="0px" BorderStyle="None" CellPadding="0" CellSpacing="0"
        Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="40" AlternatingItemStyle-BackColor="#EEEFF0"
        PagerStyle-BackColor="#8D8D8D" PagerStyle-Font-Size="12px" OnPageIndexChanged="dgNewsACN_PageIndexChanged"
        PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle"
        PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:TemplateColumn>
                <HeaderTemplate>
                    <div>
                        <asp:CheckBox ID="chkSelAll" runat="server" OnCheckedChanged="chkSelAll_changed" AutoPostBack="true" Checked="false" />
                    </div>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:CheckBox Checked='false' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible='True' /> 
                </ItemTemplate>
            </asp:TemplateColumn>
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" 
                ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# sReturnImgAttach(Eval("attach").ToString())%>
                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" title="<%# string.Format("{0}", DataBinder.Eval(Container.DataItem, "text")) %>" target="_self" style="color: #000000; text-decoration: none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';">
                        <%--<%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), global_asax.InsigteManager[Session.SessionID].Translate(DataBinder.Eval(Container.DataItem, "TITLE").ToString(),"pt",global_asax.InsigteManager[Session.SessionID].inUser.CodLanguage))%></a>--%>
                        <%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), global_asax.InsigteManager[Session.SessionID].Translate(DataBinder.Eval(Container.DataItem, "TITLE").ToString(),"pt","pt"))%></a>
                </ItemTemplate>
            </asp:TemplateColumn>

            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left">
                <ItemTemplate>
                    <%# getEditorInfoLink(Eval("editorId").ToString(), Eval("EDITOR").ToString()) %>
                </ItemTemplate>
            </asp:TemplateColumn>
             
            <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" />
            <asp:BoundColumn DataField="DATE" HeaderText="Hora" DataFormatString="{0:t}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px"  Visible="false" />
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="left" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="150px">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server" >
                            <asp:TableCell ID="TableCell3" runat="server">
                                <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell1" runat="server">
                                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;">
                                        <img src="Imgs/icons/black/png/doc_lines_stright_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesTexto") %>" title="<%= getResource("defColAccoesTexto") %>" style="border-width:0px;"/>
                                    </a>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell2" runat="server">
                                    <a id="share-news" onclick="shareTheNews(<%# "'" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() +  "','" +  DataBinder.Eval(Container.DataItem, "TITLE").ToString().Replace("'"," ").Replace("\""," ") + "'" %>)" title='<%# DataBinder.Eval(Container.DataItem, "TITLE").ToString() %>' style="color:#000000; text-decoration:none;" href="#">
                                        <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                    </a> 
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell4" runat="server">
                                    <asp:ImageButton ID="bt_AddArquivo" runat="server" 
                                        OnCommand="bt_AddArquivo_Command" 
                                        CommandArgument='<%# sReturnIdLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesClips") %>' title='<%# getResource("defColAccoesClips") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell5" runat="server" Visible='<%# pdfIsVisable() %>'>
                                    <asp:ImageButton ID="bt_AddPDF" runat="server" 
                                        OnCommand="bt_AddPDF_Command" 
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        Visible='<%# pdfIsVisable() %>'
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesDossier") %>' title='<%# getResource("defColAccoesDossier") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                           <%-- <asp:TableCell ID="TCChkBox" runat="server">
                                    <asp:CheckBox Checked='<%# checkDossier(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible='<%# chkIsVisable() %>' /> 
                            </asp:TableCell>--%>
                            <asp:TableCell ID="Dossier" runat="server">
                                    <asp:ImageButton ID="ImageButton1" runat="server" 
                                        OnCommand="bt_AddPDF_Command" 
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesDossier") %>' title='<%# getResource("defColAccoesDossier") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell8" runat="server">
                                <%# sReturnLinkPasta(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>
                            </asp:TableCell>
                            <asp:TableCell ID="TableCell7" runat="server">
                                    <asp:ImageButton ID="bt_AddComposta" runat="server" 
                                        OnCommand="bt_AddComposta_Command" 
                                        CommandArgument='<%# sReturnIdComposed(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        Visible='<%# sReturnVisComposed() %>'
                                        ImageUrl='<%# sReturnImgComposed(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesEmail") %>' title='<%# getResource("defColAccoesEmail") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                            </asp:TableCell>
                           <%-- <asp:TableCell ID="TableCell6" runat="server" Visible='<%# FbIsVisable() %>'>
                                    <asp:LinkButton ID="btn_face" runat="server" 
                                        PostBackUrl='<%# "FbNews.aspx?fb=" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' 
                                        Visible='<%# sReturnFace(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' >
                                        <img src='<%# sgetfbImg(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' alt="" style='border-width:0px; width:16px; height:16px' />
                                    </asp:LinkButton>
                            </asp:TableCell>--%>
                            <asp:TableCell ID="TableCell10" runat="server">
                                &nbsp;
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
                
            </asp:TemplateColumn>
        </Columns>
        
    </asp:DataGrid>
    </div>
    <div style="float: left; padding: 5px,0px,5px,0px; width:98%">
        <div style="float:left; width:100%"> 
            <div style="float:left;width:40%; vertical-align:middle">
                <asp:ImageButton runat="server" ID="btnExportarXML" Text="<%$Resources:insigte.language,defExcel%>" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />
                <asp:LinkButton runat="server" ID="lbExportarXML" OnClick="btnExportXML_Click" Text="<%$Resources:insigte.language,defExcel%>" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Underline="false" />
                <%--<asp:LinkButton runat="server" Visible="true" ID="LnkReport" CssClass="btn-pdfs" OnClick="btnReport_Click" Text="Relatório" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />--%>
            </div>
            <div style="float:Right; width:60%;vertical-align:middle">
                <div style="float:right;">
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="true" ID="btnAddtoFolders" CssClass="btn-pdfs" OnClick="btnAddtoFolders_Click" Text="<%$Resources:insigte.language,addToFolders%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                     <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnDossier" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossier%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                        <asp:LinkButton runat="server" Visible="false" ID="btnDossierEA" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossierEA%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="true" ID="btnAddtoClip" CssClass="btn-pdfs" OnClick="btnAddtoClip_Click" Text="<%$Resources:insigte.language,AddToClip%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="False" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnNewsLetterCp" CssClass="btn-pdfs" OnClick="btnNewsLetterCp_Click" Text="<%$Resources:insigte.language,addToNewsComposta%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                    <div style="margin-left:5px; float: right;">
                        <asp:LinkButton runat="server" Visible="false" ID="btnSelAll" CssClass="btn-pdfs" OnClick="btnSelAll_Click" Text="<%$Resources:insigte.language,defColAccoesDossierSell%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</asp:Content>
