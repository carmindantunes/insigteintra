﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="publist.aspx.cs" Inherits="Insigte.publist" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-33483716-1']);
        _gaq.push(['_trackPageview']);
        (function () {
            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <script type="text/javascript">
        function openReportWindow(url) {
            var w = window.open(url, '', 'width=1200,height=800,toolbar=0,status=0,location=0,menubar=0,directories=0,resizable=1,scrollbars=1');
            w.focus();
        }
    </script>


<script type="text/javascript">

    $(function () {
        $("#MainContent_TextBox1").datepicker({
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $("#MainContent_TextBox1").datepicker("option", "minDate", selectedDate);
            }
        });
        $("#MainContent_TextBox2").datepicker({
            defaultDate: "+1w",
            changeMonth: true,
            changeYear: true,
            dateFormat: 'yy-mm-dd',
            onClose: function (selectedDate) {
                $("#MainContent_TextBox2").datepicker("option", "maxDate", selectedDate);
            }
        });
    });
</script>


<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px;float:left; width:60%">
            <asp:Label runat="server" ID="Lb_Setor" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Sector" />
            <asp:DropDownList ID="ddStr" Font-Names="Arial" Font-Size="12px" runat="server" AutoPostBack="True" onselectedindexchanged="ddStr_SelectedIndexChanged"></asp:DropDownList>
            &nbsp;
            <asp:Label runat="server" ID="Lb_Tema" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Advertiser" />
            <asp:DropDownList ID="ddAdv" Font-Names="Arial" Font-Size="12px" runat="server" AutoPostBack="True" onselectedindexchanged="ddAdv_SelectedIndexChanged"></asp:DropDownList>
            &nbsp;
            <div style="display:none">
            <asp:Label runat="server" ID="Lb_Camp" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Campanha"  />
            <asp:DropDownList ID="ddCpn" Font-Names="Arial" Font-Size="12px" runat="server" AutoPostBack="True" onselectedindexchanged="ddCpn_SelectedIndexChanged"></asp:DropDownList>
            </div>
        </div>
        <div style="margin-top:8px;float:right; width:40%">
            <asp:Label ID="Label2" runat="server" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,advSMainDataDe%>"></asp:Label>
            <asp:TextBox runat="server" ID="TextBox1" Font-Names="Arial" Font-Size="12px" Width="80px"/>
            <asp:Label ID="Label3" runat="server" Font-Names="Arial" Font-Size="12px" Text="<%$Resources:insigte.language,advSMainDataAte%>"></asp:Label>
            <asp:TextBox runat="server" ID="TextBox2" Font-Names="Arial" Font-Size="12px" Width="80px" />
            <asp:LinkButton runat="server" Visible="true" ID="LinkButton6" CssClass="btn-pdfs" OnClick="btnpesquisa_Click"  Text="<%$Resources:insigte.language,advSMainBtnSearch%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgPub" AutoGenerateColumns="false" AllowPaging="True" 
        GridLines="None" BorderWidth="0px" BorderStyle="None" CellPadding="0" CellSpacing="0"
        Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="40" AlternatingItemStyle-BackColor="#EEEFF0"
        PagerStyle-BackColor="#8D8D8D" PagerStyle-Font-Size="12px" OnPageIndexChanged="dgPub_PageIndexChanged"
        PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle"
        PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:BoundColumn DataField="DesAdvertiser" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed1%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="125px" />
            <asp:BoundColumn DataField="NamBrand" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed2%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="NamProduct" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed3%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="NamEditor" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed4%>" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="Date" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed5%>" DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="60px" />
            <asp:BoundColumn DataField="Format" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed6%>"  HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="90px" />
            <asp:BoundColumn DataField="NumPage" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed8%>" HeaderStyle-HorizontalAlign="right" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" />
            <asp:BoundColumn DataField="Value" HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defHed7%>" HeaderStyle-HorizontalAlign="right" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" />
            <asp:TemplateColumn HeaderText="&nbsp;&nbsp;<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server" >
                            <asp:TableCell ID="TableCell3" runat="server">
                                &nbsp;&nbsp;<%# sReturnIconTipoPasta(Eval("ID_SPOT_SUPORTE").ToString(), "", DataBinder.Eval(Container.DataItem, "CodFile").ToString())%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
        
    </asp:DataGrid>
    </div>
    <div style="float: left; padding: 5px,0px,5px,0px; width:98%; display:none; visibility:hidden;">
        <div style="float:left; width:100%"> 
            <div style="float:left;width:50%; vertical-align:middle">
                <%--<asp:ImageButton runat="server" ID="btnExportarXML" Text="<%$Resources:insigte.language,defExcel%>" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />--%>
                <asp:LinkButton runat="server" Visible="false" ID="LnkReport" CssClass="btn-pdfs" OnClick="btnReport_Click" OnClientClick="openReportWindow('reportingServ.aspx?rld=RL_Publicidade_Listagem')" Text="Relatório de inserções" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false"  />
                <asp:LinkButton runat="server" Visible="false" ID="LinkButton1" CssClass="btn-pdfs" OnClick="btnReport_Click" OnClientClick="openReportWindow('reportingServ.aspx?rld=RL_Publicidade_Advertiser')" Text="Relatórios de anunciante" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                <asp:LinkButton runat="server" Visible="false" ID="LinkButton2" CssClass="btn-pdfs" OnClick="btnReport_Click" OnClientClick="openReportWindow('reportingServ.aspx?rld=RL_Publicidade_Sector')" Text="Relatório de sector" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
            </div>
<%--            <div style="float:left; width:50%;vertical-align:middle">
                <div style="float:right;">
                    <asp:LinkButton runat="server" Visible="true" ID="btnDossier" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossier%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    &nbsp;
                    <asp:LinkButton runat="server" Visible="true" ID="btnSelAll" CssClass="btn-pdfs" OnClick="btnSelAll_Click" Text="<%$Resources:insigte.language,defColAccoesDossierSell%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                </div>
            </div>--%>
        </div>
    </div>
</div>
</asp:Content>
