﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace Insigte
{
    public partial class reportingServ : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            String SReport = Global.InsigteManager[Session.SessionID].inUser.RSVLastReport; // Session["Report"].ToString();
            String qsReport = Request.QueryString["rld"];

            if (qsReport != null && qsReport.Length > 0)
                SReport = "/Publicidade/" + qsReport;
                

            if (!Page.IsPostBack)
            {
                List<ReportParameter> reportParams = new List<ReportParameter>();
                reportParams.Add(new ReportParameter("COD_CLIENT_TEMA", Global.InsigteManager[Session.SessionID].TemaCliente));
                reportParams.Add(new ReportParameter("ID_CLIENT", Global.InsigteManager[Session.SessionID].IdClient));
                reportParams.Add(new ReportParameter("ID_CLIENT_USER", Global.InsigteManager[Session.SessionID].inUser.IdClientUser));

                //Session["Report"] = "/";

                RV_test.ServerReport.ReportPath = SReport;
                RV_test.ServerReport.SetParameters(reportParams);
            }
        }

        protected void RV_test_SubmittingParameters(object sender, ReportParametersEventArgs e)
        {
            //e.Parameters["Cliente"].Values.Add(Global.InsigteManager[Session.SessionID].TemaCliente);
            //e.Parameters["ID_CLIENT"].Values.Add(Global.InsigteManager[Session.SessionID].IdClient);
        }
    }
}