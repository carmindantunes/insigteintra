﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.DataVisualization.Charting;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;

namespace Insigte
{
    public partial class testUserControl : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            DataTable GrConc = new DataTable();
            GrConc.Columns.Add("Age", typeof(int));
            GrConc.Columns.Add("Name", typeof(string));

            // Here we add five DataRows.
            GrConc.Rows.Add(25, "David");
            GrConc.Rows.Add(50, "Sam");
            GrConc.Rows.Add(10, "Christoff");
            GrConc.Rows.Add(21, "Janet");
            GrConc.Rows.Add(100, "Melanie");


            Int32[] yValues = new Int32[GrConc.Rows.Count];
            string[] xValues = new string[GrConc.Rows.Count];


            int counter = 0;
            foreach (DataRow row in GrConc.Rows)
            {
                yValues[counter] = Convert.ToInt32(row[0].ToString());
                xValues[counter] = row[1].ToString();
                counter++;
            }

            Chart1.Series["Series1"].Points.DataBindXY(xValues, yValues);


            Chart1.Series["Series1"].ChartType = SeriesChartType.Pie;// Set the Pie width
            Chart1.Series["Series1"]["PointWidth"] = "0.5";// Show data points labels
            Chart1.Series["Series1"].IsValueShownAsLabel = true;// Set data points label style
            //Chart1.Series["Series1"].Label = "#VALY";
            Chart1.Series["Series1"]["BarLabelStyle"] = "Center";// Show chart as 3D
            //Chart1.ChartAreas["ChartArea1"].Area3DStyle.Enable3D = false;// Draw chart as 3D 
            //Chart1.Series["Series1"]["DrawingStyle"] = "Cylinder";

            //Chart1.Legends.Add("Legend1");
            //Chart1.Legends[0].Enabled = true;
            //Chart1.Legends[0].Docking = Docking.Bottom;
            //Chart1.Legends[0].Alignment = System.Drawing.StringAlignment.Center;

            // Show labels in the legend in the format "Name (### %)"
            //Chart1.Series[0].LegendText = "#VALX [#VALY] (#PERCENT)";
            //Chart1.Series["Series1"].ToolTip = "#VALX [#PERCENT]";

            counter = 0;
            foreach (DataRow row in GrConc.Rows)
            {
                Chart1.Series["Series1"].Points[counter].Url = "~/Subject.aspx?clid=" + row[0] + "&tema=" + row[1];
                counter++;
            }


        }
    }
}