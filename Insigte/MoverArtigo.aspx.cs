﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class MoverArtigo : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string qsIDArtigo = Request.QueryString["ID"];
                string qsIDPasta = Request.QueryString["PID"];

                getPastas(qsIDPasta);

            }
        }


        public void getPastas(string pasta)
        {

            ddTo.Visible = true;

            string cmdQueryPastas = "select id, nome from dbo.CL10D_PASTAS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and id <> " + pasta;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_PASTAS;

                iadvisers_ACN_PASTAS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_PASTAS.Fill(tempSet);

                ddTo.DataSource = tempSet;
                ddTo.DataBind();
                ddTo.DataTextField = "nome";
                ddTo.DataValueField = "id";
                ddTo.DataBind();
                ddTo.Visible = true;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }

        }

        protected void btSalvarMove_Click(object sender, EventArgs e)
        {

            string qsIDArtigo = Request.QueryString["ID"];
            string qsIDPasta = Request.QueryString["PID"];

            //lbinfo.Text = qsIDArtigo + ":" + qsIDPasta;

            string cmdQueryPastas = "update CL10H_PASTAS_ARTICLES set pastaid = " + ddTo.SelectedItem.Value.ToString() + "  where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and articleid =" + qsIDArtigo;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                //lbinfo.Text += " - " + cmdQueryPastas;

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

            Response.Write("<script language='javascript'> { self.close() }</script>");

        }
    }
}