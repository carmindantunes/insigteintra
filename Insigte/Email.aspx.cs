﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.Mail;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class Email : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void lbEnviarMail_Click(object sender, EventArgs e)
        {
            string qsIDArtigo = Request.QueryString["ID"];

            string MailHTMLBody = string.Empty;
            string MailEnviadoPor_Comment = string.Empty;
            string MailSubject = string.Empty;

            if (tbEnviadoPor.Text != "" || tbEnviadoPor.Text != string.Empty)
            {
                MailEnviadoPor_Comment += Resources.insigte.language.defEmailSend + " " + tbEnviadoPor.Text + "<br />" + Environment.NewLine;
            }

            if (tbComments.Text != " " || tbComments.Text != string.Empty)
            {
                MailEnviadoPor_Comment += Resources.insigte.language.defEmailComment + " " + tbComments.Text + "<br />" + Environment.NewLine;
            }

            //Link
            if (rblActions.SelectedValue == "1")
            {
                MailHTMLBody += "<body style='font-family:Arial;'>";

                SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                try
                {
                    conn.Open();

                    String Title = "TITLE";
                    String texto = "TEXT as text";
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                        texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
                    }


                    string cmdQuery = "SELECT " + texto + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID ";
                    cmdQuery += "FROM iadvisers.dbo.metadata with(nolock) INNER JOIN iadvisers.dbo.contents with(nolock) ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                    cmdQuery += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                    SqlCommand cmd = new SqlCommand(cmdQuery);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        MailSubject = "insigte | Media Intelligence - " + reader.GetValue(1).ToString();
                        MailHTMLBody = "<br />" + reader.GetValue(2).ToString() + " | " + reader.GetValue(3).ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                        MailHTMLBody += "<br />" + Resources.insigte.language.defEmailLinkFollow + " <a href='http://www.insigte.com//Article.aspx?ID=" + reader.GetValue(4).ToString() + "' target='_blank' >Link</a> .";
                    }

                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }

                MailHTMLBody += "</body>";


                MailMessage mail = new MailMessage();
                mail.To = tbEnviarPara.Text;
                mail.From = "i advisers <info@iadvisers.info>";
                mail.Bcc = "";
                mail.Subject = MailSubject;
                mail.BodyFormat = MailFormat.Html;
                mail.Body = MailEnviadoPor_Comment + MailHTMLBody;

                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@iadvisers.info");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "iinfo2010");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

                try
                {
                    SmtpMail.SmtpServer = "smtp.iadvisers.info";
                    SmtpMail.Send(mail);

                    //Page.ClientScript.RegisterOnSubmitStatement(typeof(Page), "closePage", "window.onunload = CloseWindow();");
                    //lbEnviarMail.Attributes.Add("OnClientClick", "JavaScript:window.close(); return false;");
                    Response.Write("<script language='javascript'> { self.close() }</script>");
                }
                catch (Exception ex)
                {
                    Response.Write("MESSAGE: " + ex.Message);
                    Response.Write("SOURCE: " + ex.Source);
                    Response.Write("STACK: " + ex.StackTrace);
                }

            }

            //PDF
            if (rblActions.SelectedValue == "2")
            {

                string idsArt = qsIDArtigo;

                string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
                string varSolutionDir = "C:\\files\\";
                string nomeOutFile = "EmailPDFMISC" + timestamp + ".pdf";
                string varOutputFile = "C:\\pdf\\" + nomeOutFile;
                string varIndice = "C:\\pdf\\" + "findiceMISC" + timestamp + ".pdf";
                string varCapa = "C:\\pdf\\template_first.pdf";

                //Gerar Indice
                APToolkitNET.Toolkit objTK = new APToolkitNET.Toolkit();
                int intOpenOutputFile = objTK.OpenOutputFile(varIndice);

                if (intOpenOutputFile != 0)
                {
                    ErrorHandler("OpenOutputFile", intOpenOutputFile);
                }

                objTK.NewPage();
                objTK.SetFont("Helvetica", 18f);
                objTK.PrintText(470f, 700f, "i advisers");

                objTK.SetFont("Helvetica", 18f);
                objTK.PrintText(50f, 640f, "adding value");

                objTK.SetFont("Helvetica", 16f);
                objTK.PrintText(50f, 600f, Resources.insigte.language.defPDFDossierImp);

                objTK.SetFont("Helvetica", 10f);
                objTK.PrintText(50f, 560f, Resources.insigte.language.defPDFDossierImpIndice);

                //Buscar a Listagem

                //lblWarning.Text = idsArt + "<br/>";

                string cmdQuery = "SELECT TEXT as text, TITLE, EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url ";
                cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
                cmdQuery += " INNER JOIN iadvisers.dbo.contents with(nolock) ";
                cmdQuery += " ON iadvisers.dbo.contents.id = iadvisers.dbo.metadata.ID ";
                cmdQuery += " WHERE iadvisers.dbo.metadata.ID = '" + idsArt + "'";
                cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

                SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(cmdQuery);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    float x = 0f;

                    while (reader.Read())
                    {
                        x += 20f;
                        objTK.SetFont("Helvetica", 10f);
                        objTK.PrintText(50f, 560f - x, reader.GetValue(1).ToString());
                        objTK.PrintText(400f, 560f - x, reader.GetValue(2).ToString());
                        objTK.PrintText(500f, 560f - x, reader.GetValue(3).ToString().Substring(0, 10));
                    }
                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }
                objTK.CloseOutputFile();
                objTK = null;

                //Gera Principal
                APToolkitNET.Toolkit TK = new APToolkitNET.Toolkit();
                int varReturn = TK.OpenOutputFile(varOutputFile);

                if (varReturn != 0)
                {
                    ErrorHandler("OpenOutputFile", varReturn);
                }

                string varMergeFile1 = varCapa;

                varReturn = TK.MergeFile(varMergeFile1, 0, 0);

                if (varReturn <= 0)
                {
                    ErrorHandler("MergeFile", varReturn);
                }

                string varMergeFile2 = varIndice;

                varReturn = TK.MergeFile(varMergeFile2, 0, 0);

                if (varReturn <= 0)
                {
                    ErrorHandler("MergeFile", varReturn);
                }

                SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                try
                {
                    conn2.Open();

                    SqlCommand cmd = new SqlCommand(cmdQuery);
                    cmd.Connection = conn2;
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        if (reader.GetValue(5).ToString().Length > 0)
                        {
                            varMergeFile1 = varSolutionDir + reader.GetValue(5).ToString();
                            varReturn = TK.MergeFile(varMergeFile1, 0, 0);
                            if (varReturn <= 0)
                            {
                                ErrorHandler("MergeFile", varReturn);
                            }
                        }
                        else
                        {
                            TK.SetTextColor(0, 84, 132, 0, 0);
                            string varTexto = reader.GetValue(0).ToString();
                            string varEditor = reader.GetValue(2).ToString();
                            string varDate = reader.GetValue(3).ToString().Substring(0, 10);
                            string varTitulo = reader.GetValue(1).ToString();
                            TK.SetFont("Arial", 11f);
                            TK.PrintText(40f, 700f, varEditor);
                            TK.PrintText(40f, 680f, varDate);
                            TK.GreyBar(40f, 660f, 500f, 1f, 0.5f);
                            TK.SetTextColor(0, 0, 0, 0, 0);
                            TK.SetFont("Arial", 11f);
                            TK.PrintText(40f, 640f, varTitulo);
                            TK.PrintMultilineText("Arial", 10f, 40f, 620f, 500f, 680f, varTexto, 0, 0);
                            TK.NewPage();
                        }
                    }

                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }

                TK.CloseOutputFile();
                TK = null;

                File.Copy(varOutputFile, varSolutionDir + nomeOutFile);

                string DownloadPath = "C:\\files\\" + nomeOutFile;

                MailHTMLBody += "<body style='font-family:Arial;'>";

                SqlConnection conn3 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                try
                {
                    conn3.Open();

                    string cmdQuery3 = "SELECT text, TITLE, EDITOR, DATE, iadvisers.dbo.metadata.ID ";
                    cmdQuery3 += "FROM iadvisers.dbo.metadata with(nolock) INNER JOIN iadvisers.dbo.contents with(nolock) ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                    cmdQuery3 += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                    SqlCommand cmd3 = new SqlCommand(cmdQuery3);
                    cmd3.Connection = conn3;
                    cmd3.CommandType = CommandType.Text;

                    SqlDataReader reader3 = cmd3.ExecuteReader();

                    while (reader3.Read())
                    {
                        MailSubject = "i advisers | adding value - " + reader3.GetValue(1).ToString();
                        MailHTMLBody += "<br />" + reader3.GetValue(1).ToString() + "<br />" + Environment.NewLine;
                        MailHTMLBody = "<br />" + reader3.GetValue(2).ToString() + " | " + reader3.GetValue(3).ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                    }

                    cmd3.Connection.Close();
                    cmd3.Connection.Dispose();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }

                MailHTMLBody += "</body>";


                MailMessage mail = new MailMessage();
                mail.To = tbEnviarPara.Text;
                mail.From = "insigte <geral@insigte.com>";
                mail.Bcc = "";
                mail.Subject = MailSubject;
                mail.BodyFormat = MailFormat.Html;
                mail.Body = MailEnviadoPor_Comment + MailHTMLBody;

                mail.Attachments.Add(new MailAttachment(DownloadPath));

                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@insigte.com");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "iinfo2010");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

                try
                {
                    SmtpMail.SmtpServer = "smtp.iadvisers.info";
                    SmtpMail.Send(mail);

                    Response.Write("<script language='javascript'> { self.close() }</script>");

                }
                catch (Exception ex)
                {
                    Response.Write("MESSAGE: " + ex.Message);
                    Response.Write("SOURCE: " + ex.Source);
                    Response.Write("STACK: " + ex.StackTrace);
                }
            }

            //Texto
            if (rblActions.SelectedValue == "3")
            {
                MailHTMLBody += "<body style='font-family:Arial;'>";

                SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                try
                {
                    conn.Open();

                    string cmdQuery = "SELECT text, TITLE, EDITOR, DATE, iadvisers.dbo.metadata.ID ";
                    cmdQuery += "FROM iadvisers.dbo.metadata with(nolock) INNER JOIN iadvisers.dbo.contents with(nolock) ON iadvisers.dbo.metadata.ID=iadvisers.dbo.contents.ID ";
                    cmdQuery += "where iadvisers.dbo.metadata.ID='" + qsIDArtigo + "'";

                    SqlCommand cmd = new SqlCommand(cmdQuery);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader reader = cmd.ExecuteReader();

                    while (reader.Read())
                    {
                        MailSubject = "i advisers | adding value - " + reader.GetValue(1).ToString();
                        MailHTMLBody = "<br />" + reader.GetValue(2).ToString() + " | " + reader.GetValue(3).ToString().Substring(0, 10) + "<br />" + Environment.NewLine;
                        MailHTMLBody += "<br />" + reader.GetValue(0).ToString().Replace(Environment.NewLine, "<br />" + Environment.NewLine);
                    }

                    cmd.Connection.Close();
                    cmd.Connection.Dispose();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }

                MailHTMLBody += "</body>";


                MailMessage mail = new MailMessage();
                mail.To = tbEnviarPara.Text;
                mail.From = "i advisers <info@iadvisers.info>";
                mail.Bcc = "";
                mail.Subject = MailSubject;
                mail.BodyFormat = MailFormat.Html;
                mail.Body = MailEnviadoPor_Comment + MailHTMLBody;

                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpserverport", 25);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusing", 2);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpauthenticate", 1);
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendusername", "info@iadvisers.info");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/sendpassword", "iinfo2010");
                mail.Fields.Add("http://schemas.microsoft.com/cdo/configuration/smtpusessl", "false");

                try
                {
                    SmtpMail.SmtpServer = "smtp.iadvisers.info";
                    SmtpMail.Send(mail);

                    Response.Write("<script language='javascript'> { self.close() }</script>");

                }
                catch (Exception ex)
                {
                    Response.Write("MESSAGE: " + ex.Message);
                    Response.Write("SOURCE: " + ex.Source);
                    Response.Write("STACK: " + ex.StackTrace);
                }
            }
        }

        public static void ErrorHandler(string strMethod, object rtnCode)
        {
            System.Diagnostics.Debug.WriteLine(strMethod + " error:  " + rtnCode.ToString());
        }
    }
}