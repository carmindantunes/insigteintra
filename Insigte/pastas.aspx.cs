﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Collections;
using System.Net;
using System.Text;


namespace Insigte
{
    public partial class pastas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }

            dgPasta.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgPasta.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            
            if (!Page.IsPostBack)
            {
                getPastasContent(getPastas());
            }
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public string getPastaID()
        {
            return tvPastas.SelectedItem.Value;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        public string getPastas()
        {

            tvPastas.Visible = true;

            string cmdQueryPastas = "select id, nome from dbo.CL10D_PASTAS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_PASTAS;

                iadvisers_ACN_PASTAS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_PASTAS.Fill(tempSet);

                tvPastas.DataSource = tempSet;
                tvPastas.DataBind();
                tvPastas.DataTextField = "nome";
                tvPastas.DataValueField = "id";
                tvPastas.DataBind();
                tvPastas.Visible = true;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }

            if (tvPastas.SelectedIndex < 0)
                return string.Empty;
            else
                return tvPastas.SelectedItem.Value;
        }

        public void getPastasContent(string pasta)
        {

            dgPasta.Visible = true;

            string SQLpasta = " select articleid from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid = " + pasta + " ";

            String Title = "TITLE";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
            }

            string cmdQuery = "SELECT " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url, tipo ";
            cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
            cmdQuery += " where iadvisers.dbo.metadata.ID in (" + SQLpasta + ") ";
            cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_NEWS;

                iadvisers_ACN_NEWS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_NEWS.Fill(tempSet);

                dgPasta.DataSource = tempSet;
                dgPasta.DataBind();
                dgPasta.Visible = true;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }

        protected string sReturnLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;
            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('RemNews.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/folderplus32_rem.png' width='16px' height='20px' alt='" + Resources.insigte.language.defColAccoesRemover + "' title='" + Resources.insigte.language.defColAccoesRemover + "' style='border-width:0px;'/></a>";
            return sLink;
        }

        protected string sReturnLinkPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "#" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpapercheck32_color.png' width='16px' height='16px' alt='Notícia Adicionada' title='Notícia Adicionada' style='border-width:0px;'/></a>";
                        }
                        else
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "#" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpapercheck32_color.png' width='16px' height='16px' alt='Notícia Adicionada' title='Notícia Adicionada' style='border-width:0px;'/></a>";
                        }
                        else
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
                        }
                    }
                }
            }
            else
            {
                sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
            }
            return sLink;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/book_side_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/book_side_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected void addLinkPDF(string qsPDFArtigo)
        {

            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["PDF"] != "")
            {
                PDFCookie = cookie.Values["PDF"] + "|" + qsPDFArtigo;
            }
            else
            {
                PDFCookie = qsPDFArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            ValCookie = cookie.Values["ID"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAddPDF.Text = "O artigo de ID: " + qsPDFArtigo + ", foi adicionado com sucesso.";

        }

        protected void bt_Delete_Command(Object sender, CommandEventArgs e)
        {

            string cmdQueryIns = "insert into iadvisers.dbo.CL10H_PASTAS_ARTICLES_DELETED (id_client, id_client_user, pastaid, articleid, nota, dat_criacao, cod_dia_criacao, dat_delete, cod_dia_delete) ";
            cmdQueryIns += "select id_client, id_client_user, pastaid, articleid, nota, dat_criacao, cod_dia_criacao, getdate(), convert(varchar,getdate(),112) from iadvisers.dbo.CL10H_PASTAS_ARTICLES with(nolock) ";
            cmdQueryIns += "where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid=" + tvPastas.SelectedItem.Value.ToString() + " and articleid=" + e.CommandArgument.ToString();

            string cmdQuery = "delete from iadvisers.dbo.CL10H_PASTAS_ARTICLES ";
            cmdQuery += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid=" + tvPastas.SelectedItem.Value.ToString() + " and articleid=" + e.CommandArgument.ToString();

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryIns);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.ExecuteNonQuery();

                SqlCommand cmddel = new SqlCommand(cmdQuery);
                cmddel.Connection = conn;
                cmddel.CommandType = CommandType.Text;
                cmddel.ExecuteNonQuery();

                lblDataToday.Text = Resources.insigte.language.defRemovidoPasta + " " + tvPastas.SelectedItem.Text.ToString();
                getPastasContent(getPastas());

            }
            catch (Exception exp)
            {
                lblDataToday.Text = exp.Message.ToString();

            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

        }

        protected void bt_AddPDF_Command(Object sender, CommandEventArgs e)
        {
            addLinkPDF(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " " + Resources.insigte.language.defAdicionadoSucesso;

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/icons/black/png/book_side_icon_16_sel.png";
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0,4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4,3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;

            }
        }

        protected void tvPastas_SelectedIndexChanged(object sender, EventArgs e)
        {
            getPastasContent(tvPastas.SelectedItem.Value.ToString());
        }

        protected void dgPasta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            //dgPasta.CurrentPageIndex = e.NewPageIndex;
        }

        protected void btnBuildPDF_Click_BCK(object sender, EventArgs e)
        {

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            string[] words = cookie.Values["PDF"].ToString().Split('|');
            string idsArt = string.Empty;

            foreach (string value in words)
            {
                idsArt += "'" + value + "',";
            }

            idsArt = idsArt.Substring(0, idsArt.LastIndexOf(','));

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string varSolutionDir = "C:\\files\\";
            string nomeOutFile = "RevistaImprensaMISC" + timestamp + ".pdf";
            string varOutputFile = "C:\\pdf\\" + nomeOutFile;
            string varIndice = "C:\\pdf\\" + "findiceMISC" + timestamp + ".pdf";
            string varCapa = "C:\\pdf\\template_first.pdf";

            //Gerar Indice
            APToolkitNET.Toolkit objTK = new APToolkitNET.Toolkit();
            int intOpenOutputFile = objTK.OpenOutputFile(varIndice);

            if (intOpenOutputFile != 0)
            {
                ErrorHandler("OpenOutputFile", intOpenOutputFile);
            }

            objTK.NewPage();
            objTK.SetFont("Helvetica", 18f);
            objTK.PrintText(470f, 700f, "insigte");

            objTK.SetFont("Helvetica", 18f);
            objTK.PrintText(50f, 640f, "media intelligence");

            objTK.SetFont("Helvetica", 16f);
            objTK.PrintText(50f, 600f, Resources.insigte.language.defPDFDossierImp);

            objTK.SetFont("Helvetica", 10f);
            objTK.PrintText(50f, 560f, Resources.insigte.language.defPDFDossierImpIndice);

            //Buscar a Listagem

            //lblWarning.Text = idsArt + "<br/>";

            String Title = "TITLE";
            String texto = "TEXT as text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
            }

            string SQLpasta = " select articleid from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid = " + tvPastas.SelectedValue + " ";

            string cmdQuery = "SELECT " + texto + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url ";
            cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
            cmdQuery += " INNER JOIN iadvisers.dbo.contents with(nolock) ";
            cmdQuery += " ON iadvisers.dbo.contents.id = iadvisers.dbo.metadata.ID ";
            cmdQuery += " WHERE iadvisers.dbo.metadata.ID in (" + SQLpasta + ") ";
            cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                float x = 0f;

                while (reader.Read())
                {
                    x += 20f;
                    objTK.SetFont("Helvetica", 10f);

                    String Titulo = reader.GetValue(1).ToString();

                    if (Titulo.Length > 75)
                    {
                        objTK.PrintText(50f, 560f - x, Titulo.Substring(0, Titulo.IndexOf(' ', 65)));
                        objTK.PrintText(400f, 560f - x, reader.GetValue(2).ToString());
                        objTK.PrintText(500f, 560f - x, reader.GetValue(3).ToString().Substring(0, 8));
                        
                        x += 20f;
                        objTK.SetFont("Helvetica", 10f);

                        objTK.PrintText(50f, 560f - x, Titulo.Substring(Titulo.IndexOf(' ', 65), Titulo.Length - (Titulo.IndexOf(' ', 65)) ));
                        objTK.PrintText(400f, 560f - x, " ");
                        objTK.PrintText(500f, 560f - x, " ");
                    }
                    else
                    {
                        objTK.PrintText(50f, 560f - x, reader.GetValue(1).ToString());
                        objTK.PrintText(400f, 560f - x, reader.GetValue(2).ToString());
                        objTK.PrintText(500f, 560f - x, reader.GetValue(3).ToString().Substring(0, 8));
                    }
                }
                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            objTK.CloseOutputFile();
            objTK = null;

            //Gera Principal
            APToolkitNET.Toolkit TK = new APToolkitNET.Toolkit();
            int varReturn = TK.OpenOutputFile(varOutputFile);

            if (varReturn != 0)
            {
                ErrorHandler("OpenOutputFile", varReturn);
            }

            string varMergeFile1 = varCapa;

            varReturn = TK.MergeFile(varMergeFile1, 0, 0);

            if (varReturn <= 0)
            {
                ErrorHandler("MergeFile", varReturn);
            }

            string varMergeFile2 = varIndice;

            varReturn = TK.MergeFile(varMergeFile2, 0, 0);

            if (varReturn <= 0)
            {
                ErrorHandler("MergeFile", varReturn);
            }

            SqlConnection conn2 = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

            try
            {
                conn2.Open();

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn2;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    if (reader.GetValue(5).ToString().Length > 0 && Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt")
                    {
                        varMergeFile1 = varSolutionDir + reader.GetValue(5).ToString();
                        varReturn = TK.MergeFile(varMergeFile1, 0, 0);
                        if (varReturn <= 0)
                        {
                            ErrorHandler("MergeFile", varReturn);
                            //lblWarning.Text += "Bate aqui - Erro1: " + reader.GetValue(4).ToString() + "<br/>";
                        }
                    }
                    else
                    {
                        TK.SetTextColor(0, 84, 132, 0, 0);
                        string varTexto = reader.GetValue(0).ToString();
                        string varEditor = reader.GetValue(2).ToString();
                        string varDate = reader.GetValue(3).ToString().Substring(0, 8);
                        string varTitulo = reader.GetValue(1).ToString();
                        TK.SetFont("Arial", 11f);
                        TK.PrintText(40f, 700f, varEditor);
                        TK.PrintText(40f, 680f, varDate);
                        TK.GreyBar(40f, 660f, 500f, 1f, 0.5f);
                        TK.SetTextColor(0, 0, 0, 0, 0);
                        TK.SetFont("Arial", 11f);
                        TK.PrintText(40f, 640f, varTitulo);

                        TK.PrintMultilineText("Arial", 10f, 40f, 620f, 500f, 600f, varTexto, 0, 0);
                        //varTexto = TK.ClipText; //ClipText guarda o resto do texto que não se conseguio fazer print.

                        while (TK.ClipText != "") 
                        {
                            TK.NewPage();
                            TK.PrintMultilineText("Arial", 10f, 40f, 700f, 500f, 680f, TK.ClipText, 0, 0);
                        } 

                        TK.NewPage();
                        //lblWarning.Text += "Bate aqui 2: " + reader.GetValue(4).ToString() + "<br/>";
                    }
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }

            //CloseOutputFile
            TK.CloseOutputFile();
            //Clear Object
            TK = null;

            File.Copy(varOutputFile, varSolutionDir + nomeOutFile);

            string DownloadPath = "C:\\files\\" + nomeOutFile;
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + nomeOutFile + "\"");
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.WriteFile(DownloadPath);
            HttpContext.Current.Response.End();
        }

        protected void btnBuildPDF_Click(object sender, EventArgs e)
        {

            //DIFINIR AS FONTES DO DOCUMENTO

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            BaseFont bfHelve = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            BaseFont bfArial = BaseFont.CreateFont("C:\\Windows\\Fonts\\arial.ttf", BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
            Font helve = new Font(bfHelve, 18f, Font.NORMAL, BaseColor.BLACK);
            Font ArialSmall = new Font(bfArial, 8f, Font.NORMAL, BaseColor.BLACK);
            Font helveSmall = new Font(bfHelve, 16f, Font.NORMAL, BaseColor.BLACK);
            Font helveVSmall = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLACK);

            Font helveVSmallBlue = new Font(bfHelve, 12f, Font.NORMAL, BaseColor.BLUE);
            helveVSmallBlue.SetColor(0, 173, 217);
            Font helveVVSmallBlue = new Font(bfHelve, 8f, Font.NORMAL, BaseColor.BLUE);
            helveVVSmallBlue.SetColor(0, 173, 217);

            Font helveXSmall = new Font(bfHelve, 6f, Font.NORMAL, BaseColor.BLACK);

            //ABRE O INDICE E PREPARA OD DOCUMENTOS

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string varSolutionDir = "G:\\files\\";
            string varSolutionDirCostum = "G:\\cfiles\\" + Global.InsigteManager[Session.SessionID].IdClient + "\\";

            string varIndice = "G:\\pdf\\findiceMISC" + timestamp + ".pdf";
            string varCapa = "G:\\pdf\\template_first.pdf";

            var docIndice = new Document();
            string path = Server.MapPath("pdf");
            PdfWriter pdfw = PdfWriter.GetInstance(docIndice, new FileStream(varIndice, FileMode.Create));

            docIndice.Open();
            docIndice.NewPage();

            //INICIALIZA O INDICE

            PlaceText(pdfw.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
            PlaceText(pdfw.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);
            PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 710, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);

            iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_insigte_trans.png");

            if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                myImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
            //ClienteLogoImage.SetAbsolutePosition(526f, 775f);
            myImage.SetAbsolutePosition(526f, 795f);
            //myImage.SetAbsolutePosition(500f, 760f);
            //myImage.ScalePercent(36f); 100%
            myImage.ScalePercent(10f);
            //myImage.ScaleToFit(16, 16);
            docIndice.Add(myImage);

            String Title = "TITLE";
            String texto = "TEXT as text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
            }

            string SQLpasta = " select articleid from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid = " + tvPastas.SelectedValue + " ";

            string cmdQuery = "SELECT " + texto + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url, tipo ";
            cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
            cmdQuery += " INNER JOIN iadvisers.dbo.contents with(nolock) ";
            cmdQuery += " ON iadvisers.dbo.contents.id = iadvisers.dbo.metadata.ID ";
            cmdQuery += " WHERE iadvisers.dbo.metadata.ID in (" + SQLpasta + ") ";
            cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

            DataTable News = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "getDadosPdf");

            int vSpace = 5;
            int nPage = 0;

            //----------------------------------------NOTICIAS -------------------
            string NomeBase = Resources.insigte.language.defPDFDossierImp;

            string nomeOutFile = NomeBase + "_" + timestamp + "_0.pdf";
            string AuxNomeOutFile = "";

            string varOutputFile = "G:\\pdf\\" + nomeOutFile;
            string AuxVarOutputFile = "";

            string varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

            //var docNoticias = new Document();
            //FileStream fsNot = new FileStream(varOutputFile, FileMode.Create);
            //PdfWriter pdfwNews = PdfWriter.GetInstance(docNoticias, fsNot);

            //docNoticias.Open();
            //docNoticias.NewPage();

            //PlaceText(pdfwNews.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
            //PlaceText(pdfwNews.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);

            //docNoticias.Close();
            //docNoticias.Dispose();
            //fsNot.Close();

            int xCounter = 0;
            int PagNum = 3;

            float NumPagIndice = News.Rows.Count / 42;
            PagNum = PagNum + Convert.ToInt32(NumPagIndice);
            Boolean firstPage = true;

            foreach (DataRow x in News.Rows)
            {
                //ADICIONA AO INDICE
                String Icon = "";

                switch (x["tipo"].ToString())
                {
                    case "Online": Icon = "globe_1_icon_24.png";
                        break;
                    case "Imprensa": Icon = "doc_export_icon_24.png";
                        break;
                    case "Rádio": Icon = "headphones_icon_16.png";
                        break;
                    case "Televisão": Icon = "movie_icon_24.png";
                        break;
                    default: Icon = "doc_export_icon_24.png";
                        break;
                }

                if (nPage > 42)
                {
                    docIndice.NewPage();
                    PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 790, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);
                    vSpace = -60;
                    nPage = 0;
                }

                vSpace += 15;

                //String Titulo = x["TITLE"].ToString();
                String Titulo = "";
                if (x["TITLE"].ToString().Length > 85)
                    Titulo = x["TITLE"].ToString().Substring(0, 85) + "...";
                else
                    Titulo = x["TITLE"].ToString();

                String Editor = x["EDITOR"].ToString();
                String Data = Convert.ToDateTime(x["DATE"].ToString()).ToShortDateString();
                String Ficheiro = x["filepath"].ToString();

                Ficheiro = Ficheiro.Replace(" ", "%20");

                if (Titulo.Length > 100)
                {


                    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    newsIcon.ScaleAbsolute(6f, 6f);
                    docIndice.Add(newsIcon);


                    if (Icon == "globe_1_icon_24.png")
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, x["page_url"].ToString());
                    else
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, "http://insigte.com/ficheiros/" + Ficheiro);


                    //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, 90) + "...");
                    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    ////Comentado por trazer muitos problemas de compatibilidade com os browsers.

                    ////Tenta separar o titulo por espaço
                    //try
                    //{
                    //    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    //    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    //    newsIcon.ScaleAbsolute(6f, 6f);
                    //    docIndice.Add(newsIcon);

                    //    //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 435, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, Titulo.IndexOf(' ', 100)));
                    //    //PlaceText(pdfw.DirectContent, helveXSmall, 435, 660 - vSpace, 535, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    //    //PlaceText(pdfw.DirectContent, helveXSmall, 535, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());

                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, Titulo.IndexOf(' ', 100)));
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    //    vSpace += 15;

                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(Titulo.IndexOf(' ', 100), Titulo.Length - (Titulo.IndexOf(' ', 100))));
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, " ");
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, " ");
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    //    nPage++;
                    //}
                    //catch (Exception ex) //Se não conseguir, corta o final do titulo.
                    //{
                    //    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    //    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    //    newsIcon.ScaleAbsolute(6f, 6f);
                    //    docIndice.Add(newsIcon);

                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0,90) + "..." );
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());
                    //}
                }
                else
                {
                    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    newsIcon.ScaleAbsolute(6f, 6f);
                    docIndice.Add(newsIcon);


                    if (Icon == "globe_1_icon_24.png")
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, x["page_url"].ToString());
                    else
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, "http://insigte.com/ficheiros/" + Ficheiro);


                    //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo);
                    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());
                }

                nPage++;

                //TRATA NOTICIAS

                xCounter++;
                varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

                String Texto = x["text"].ToString();

                if (x["filepath"].ToString().Length > 0 && Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" && !chk_InText.Checked && x["tipo"].ToString() == "Imprensa")
                {
                    IDictionary<string ,string>  ipath = Global.InsigteManager[Session.SessionID].pathfile(varSolutionDir, x["filepath"].ToString());
                    varSolutionDir = ipath["directoria"].ToString();
                    String NewsPDF = varSolutionDir + ipath["ficheiro"].ToString();


                    //String NewsPDF = varSolutionDir + x["filepath"].ToString();

                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(varSolutionDirCostum + x["filepath"].ToString()))
                            NewsPDF = varSolutionDirCostum + x["filepath"].ToString();
                        else
                        {

                            String DesFileRoot = @"G:\cfiles";
                            String DesFilePath = DesFileRoot + @"\" + Global.InsigteManager[Session.SessionID].IdClient;
                            String OriFich = varSolutionDir + x["filepath"].ToString().Replace("/", "\\");
                            String DesFich = varSolutionDirCostum + x["filepath"].ToString().Replace("/", "\\");
                            String ChKDiRecT = x["filepath"].ToString().Replace("/", "\\");

                            string[] Directorias = ChKDiRecT.Split('\\');
                            String Dir = "";
                            foreach (string str in Directorias)
                            {
                                if (!str.EndsWith(".pdf"))
                                {
                                    Dir += "\\" + str;
                                    if (!Directory.Exists(DesFilePath + Dir))
                                        Directory.CreateDirectory(DesFilePath + Dir);
                                }
                            }

                            PdfReader readerPs = new PdfReader("G:\\pdf\\size.pdf");
                            int nPs = readerPs.NumberOfPages;
                            Rectangle pageSize = readerPs.GetPageSizeWithRotation(1);
                            Document document = new Document(pageSize);
                            FileStream fs = new FileStream(DesFich, FileMode.Create);
                            PdfWriter writer = PdfWriter.GetInstance(document, fs);
                            writer.CloseStream = false;
                            document.Open();
                            PdfContentByte cb = writer.DirectContent;
                            PdfImportedPage page;
                            int rotation;
                            PdfReader InsLogo = new PdfReader(OriFich);
                            int n = InsLogo.NumberOfPages;
                            int i = 0;
                            while (i < n)
                            {
                                i++;
                                document.SetPageSize(InsLogo.GetPageSizeWithRotation(i));
                                document.NewPage();
                                page = writer.GetImportedPage(InsLogo, i);
                                rotation = InsLogo.GetPageRotation(i);
                                if (rotation == 90 || rotation == 270)
                                {
                                    cb.AddTemplate(page, 0, -1f, 1f, 0, 0, InsLogo.GetPageSizeWithRotation(i).Height);
                                }
                                else
                                {
                                    cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                                }


                                iTextSharp.text.Image ClearLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_clear.png");
                                ClearLogoImage.SetAbsolutePosition(518f, 775f);
                                ClearLogoImage.ScalePercent(15f);
                                writer.DirectContent.AddImage(ClearLogoImage);

                                iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient + ".png");
                                ClienteLogoImage.SetAbsolutePosition(526f, 775f);
                                ClienteLogoImage.ScalePercent(10f);
                                writer.DirectContent.AddImage(ClienteLogoImage);

                                PlaceText(writer.DirectContent, helveXSmall, 528, 15, 605, 55, 14, Element.ALIGN_LEFT, "insigte ™ | 2015");

                                //document.Add(ClienteLogoImage);
                            }
                            document.Close();
                            document.Dispose();
                            fs.Close();
                            fs.Dispose();
                            readerPs.Close();
                            readerPs.Dispose();

                            NewsPDF = DesFich;
                        }
                    }

                    //String NewsPDF = varSolutionDir + x["filepath"].ToString();

                    string pdfss = varOutputFile + "," + NewsPDF;
                    string[] pdfs = pdfss.Split(',');

                    if (!File.Exists(NewsPDF))
                    {
                        // System.Diagnostics.Debug.WriteLine("Ficheiro: " + NewsPDF);
                        filetranfer(NewsPDF);
                    }

                    AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                    AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                    PagNum += PdfMerge.CountPageNo(NewsPDF);

                    if (firstPage)
                    {
                        //http://insigte.com/ficheiros/2013/04/29/Exame_01052013_p68.pdf
                        if (NewsPDF.Contains("http://"))
                        {
                            using (WebClient wc = new WebClient())
                           
                                wc.DownloadFile(NewsPDF, AuxVarOutputFile);
                            
                        }
                        else
                        {
                            File.Copy(NewsPDF, AuxVarOutputFile);
                        }

                        firstPage = false;
                    }
                    else
                    {
                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);
                        File.Delete(varOutputFile);
                    }

                    //PdfMerge.MergeFiles(AuxVarOutputFile, pdfs);

                    nomeOutFile = AuxNomeOutFile;
                    varOutputFile = AuxVarOutputFile;

                }
                else
                {

                    //Cria PDF temporario para depois fazer o merge.
                    var docAdd = new Document();
                    FileStream fs = new FileStream(varOutputTxtFile, FileMode.Create);

                    PdfWriter pdfwAdd = PdfWriter.GetInstance(docAdd, fs);
                    docAdd.Open();
                    docAdd.NewPage();



                    //numero pagina
                    PlaceText(pdfwAdd.DirectContent, helveXSmall, 558, 20, 635, 50, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        iTextSharp.text.Image ClearLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_clear.png");
                        ClearLogoImage.SetAbsolutePosition(518f, 775f);
                        ClearLogoImage.ScalePercent(15f);
                        pdfwAdd.DirectContent.AddImage(ClearLogoImage);


                        iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
                        ClienteLogoImage.SetAbsolutePosition(526f, 775f);
                        ClienteLogoImage.ScalePercent(10f);
                        pdfwAdd.DirectContent.AddImage(ClienteLogoImage);

                        PlaceText(pdfwAdd.DirectContent, helveXSmall, 528, 20 - vSpace, 605, 15 - vSpace, 14, Element.ALIGN_LEFT, " media='print'5");
                    }
                    else
                    {
                        //Logo Insigte
                        docAdd.Add(myImage);
                    }

                    PdfContentByte cb = pdfwAdd.DirectContent;
                    ColumnText ct = new ColumnText(cb);
                    ct.Alignment = Element.ALIGN_JUSTIFIED;
                    ct.YLine = 1f;
                    ct.SetLeading(0, 1.2f);

                    Paragraph heading = new Paragraph(Titulo, helveVSmallBlue);
                    Paragraph editor = new Paragraph(Editor + " - " + Data, helveVVSmallBlue);
                    heading.Leading = 40f;
                    docAdd.Add(heading);
                    docAdd.Add(editor);

                    Phrase phr = new Phrase(Texto, ArialSmall);
                    phr.Leading = 0;
                    ct.AddText(phr);


                    float gutter = 15f;
                    float colwidth = (docAdd.Right - docAdd.Left - gutter) / 2;
                    //float[] left = { docAdd.Left + 90f, docAdd.Top - 80f, docAdd.Left + 90f, docAdd.Top - 170f, docAdd.Left, docAdd.Top - 170f, docAdd.Left, docAdd.Bottom };

                    float[] left = { docAdd.Left, docAdd.Top - 80f, docAdd.Left, docAdd.Bottom };
                    float[] right = { docAdd.Left + colwidth, docAdd.Top - 80f, docAdd.Left + colwidth, docAdd.Bottom };
                    float[] left2 = { docAdd.Right - colwidth, docAdd.Top - 80f, docAdd.Right - colwidth, docAdd.Bottom };
                    float[] right2 = { docAdd.Right, docAdd.Top - 80f, docAdd.Right, docAdd.Bottom };

                    int status = 0;
                    int i = 0;
                    bool nextPage = false;

                    while (ColumnText.HasMoreText(status))
                    {
                        if (nextPage)
                        {
                            i = 0;
                            docAdd.NewPage();
                            PagNum++;
                            nextPage = false;
                        }

                        if (Texto.Length > 5000)

                            if (i == 0)
                            {
                                //Writing the first column
                                ct.SetColumns(left, right);
                                i++;
                            }
                            else
                            {
                                //write the second column
                                ct.SetColumns(left2, right2);
                                nextPage = true;
                            }

                        else
                        {
                            ct.SetColumns(left, right2);
                            nextPage = true;
                        }

                        //Needs to be here to prevent app from hanging
                        // NUNCA MEXER NESTA MERDA PKP LIXOU O IIS
                        ct.YLine = docAdd.Top - 80f;
                        //Commit the content of the ColumnText to the document
                        //ColumnText.Go() returns NO_MORE_TEXT (1) and/or NO_MORE_COLUMN (2)
                        //In other words, it fills the column until it has either run out of column, or text, or both
                        status = ct.Go();

                    }
                    if (x["page_url"].ToString().Length > 0)
                        PlaceTextLink(pdfwAdd.DirectContent, helveVSmall, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, MakeTinyUrl(x["page_url"].ToString()), MakeTinyUrl(x["page_url"].ToString()));

                    if (x["filepath"].ToString().Length > 0)
                        PlaceTextLink(pdfwAdd.DirectContent, helveVSmall, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, MakeTinyUrl("http://insigte.com/ficheiros/" + x["filepath"].ToString()), MakeTinyUrl("http://insigte.com/ficheiros/" + x["filepath"].ToString()));


                    //if (x["page_url"].ToString().Length > 0)
                    //    PlaceText(pdfwAdd.DirectContent, helveXSmall, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, x["page_url"].ToString());

                    //if (x["filepath"].ToString().Length > 0)
                    //    PlaceText(pdfwAdd.DirectContent, helveXSmall, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, "http://insigte.com/ficheiros/" + x["filepath"].ToString());


                    docAdd.Close();
                    docAdd.Dispose();
                    fs.Close();

                    string pdfss = varOutputFile + "," + varOutputTxtFile;
                    string[] pdfs = pdfss.Split(',');

                    AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                    AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                    if (firstPage)
                    {
                        File.Copy(varOutputTxtFile, AuxVarOutputFile);
                        firstPage = false;
                    }
                    else
                    {
                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);
                        //PdfMerge.MergeFiles(AuxVarOutputFile, pdfs);

                        File.Delete(varOutputFile);
                        File.Delete(varOutputTxtFile);
                    }
                    nomeOutFile = AuxNomeOutFile;
                    varOutputFile = AuxVarOutputFile;

                    PagNum++;
                }
            }

            docIndice.Close();

            //Finaliza o PDF

            String Capa = "G:\\pdf\\template_first.pdf";
            String PDFFinal = "G:\\pdf\\" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";

            if (File.Exists("G:\\pdf\\template_first_" + Global.InsigteManager[Session.SessionID].IdClient + ".pdf"))
                Capa = "G:\\pdf\\template_first_" + Global.InsigteManager[Session.SessionID].IdClient + ".pdf";


            string pdfsFinals = Capa + "," + varIndice + "," + varOutputFile;
            string[] pdfsFim = pdfsFinals.Split(',');

            PdfMerge.MergePdfs(PDFFinal, pdfsFim);
            //PdfMerge.MergeFiles(PDFFinal, pdfsFim);

            File.Delete(varIndice);
            File.Delete(varOutputFile);

            // Limpa as Cookies

            //string ValCookie = string.Empty;
            //string DelCookie = string.Empty;
            //string PDFCookie = string.Empty;

            //ValCookie = cookie.Values["ID"];
            //PDFCookie = "";
            //DelCookie = cookie.Values["DELNOT"];

            //cookie.Expires = DateTime.Now.AddDays(-1d);
            //Response.Cookies.Add(cookie);

            //HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            //newCookie["Version"] = "JUL2012";
            //newCookie["ID"] = ValCookie;
            //newCookie["PDF"] = "";
            //newCookie["DELNOT"] = DelCookie;
            //newCookie.Expires = DateTime.Now.AddDays(7);
            //Response.Cookies.Add(newCookie);


            try
            {

                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf" + "\"");
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.WriteFile(PDFFinal);
                HttpContext.Current.Response.End();

            }
            catch (Exception teste)
            {
                lblDataToday.Visible = true;
                //lblDataToday.Text = "Morangos : " + teste.Message.ToString();
            }

        }


        protected void filetranfer(string ficheironovo)
        {
            // Criar a ligação FTP.

            String IDCLIE = Global.InsigteManager[Session.SessionID].IdClient;
            String localf = "G:\\files";
            if (IDCLIE == ("1047"))
            {
                localf = "G:\\cfiles\\1047";
            }

            String fiche = ficheironovo.ToString().Replace(localf, "ftp://insigtedata.dyndns.org/insigtedata").Replace("\\", "/");
            //String fiche = ficheironovo.ToString().Replace("G:\\files", "ftp://insigtedata.dyndns.org/insigtedata").Replace("\\", "/");
            try
            {

                FtpWebRequest requestftp = (FtpWebRequest)WebRequest.Create(fiche);
                requestftp.Method = WebRequestMethods.Ftp.DownloadFile;
                requestftp.Credentials = new NetworkCredential("insigteftp", "media!INTELL");
                requestftp.UsePassive = true;
                requestftp.UseBinary = true;
                requestftp.KeepAlive = true;

                //criar o objeto FtpWebResponse
                FtpWebResponse responseftp = (FtpWebResponse)requestftp.GetResponse();
                //Criar a Stream para ler o ficheiro
                Stream responseStream = responseftp.GetResponseStream();

                byte[] buffer = new byte[2048];

                //Definir o local onde o ficheiro será criado.
                Directory.CreateDirectory(Path.GetDirectoryName(ficheironovo));
                FileStream newFile = new FileStream(ficheironovo, FileMode.Create);
                //Ler o ficheiro de origem
                int readCount = responseStream.Read(buffer, 0, buffer.Length);
                while (readCount > 0)
                {
                    //Escrever o ficheiro
                    newFile.Write(buffer, 0, readCount);
                    readCount = responseStream.Read(buffer, 0, buffer.Length);
                }
                newFile.Close();
                responseStream.Close();
                responseftp.Close();
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                //lblDataToday.Text = "Morangos : " + teste.Message.ToString();
            }
        }



        protected static void PlaceText(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text)
        {
            ColumnText ct = new ColumnText(pdfContentByte);
            ct.SetSimpleColumn(new Phrase(text, font), lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }

        protected static void PlaceTextLink(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text, string link)
        {
            ColumnText ct = new ColumnText(pdfContentByte);

            Anchor lnk = new Anchor(text, font);
            lnk.Reference = link;

            Chunk cnk = new Chunk(text, font);
            cnk.SetAnchor(link);


            //var c = new Chunk("Google");
            //c.SetAnchor("https://www.google.com");
            //memberCell.AddElement(c);

            Phrase phr = new Phrase(cnk);


            ct.SetSimpleColumn(phr, lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }

        public static void ErrorHandler(string strMethod, object rtnCode)
        {
            System.Diagnostics.Debug.WriteLine(strMethod + " error:  " + rtnCode.ToString());
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists( @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/","\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
            }
        }

        protected Boolean chkIsVisable()
        {
            if (Global.InsigteManager[Session.SessionID].inUser.HasPastas.ToString() == "1")
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        protected Boolean checkDossier(String idArtigo)
        {
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && idArtigo != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(idArtigo))
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        if (idVal == idArtigo)
                        {
                            return true;
                        }
                        else
                        {
                            return false;
                        }
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }


        }

        protected void chkSelAll_changed(object sender, EventArgs e)
        {
             CheckBox chk2 = (CheckBox)sender;
            
            
            //CheckBox chk1 = (CheckBox)dgPasta.FindControl("chkSelAll");
            
            foreach (DataGridItem drv in dgPasta.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");

                chk.Checked = chk2.Checked;
            }
        }

        protected void chkRow_Changed(object sender, EventArgs e)
        {
            CheckBox chk2 = (CheckBox)sender;
            int checks = 0;
            foreach (DataGridItem drv in dgPasta.Items)
            {
                CheckBox chk = (CheckBox)drv.FindControl("chka_AddPDF");
                if(chk.Checked)
                    checks++;

                //if(checks > 0 )
                
                }  
        }
    }
}