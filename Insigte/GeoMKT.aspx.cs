﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using INinsigteManager;
using System.Data;
using System.Drawing;
using System.Web.UI.DataVisualization.Charting;

namespace Insigte
{
    public partial class GeoMKT : BasePage
    {
        private DataTable countNews = new DataTable();
        private DataTable ConcorrenciaCount = new DataTable();
        private DataTable sumCliCon = new DataTable();

        //private DataTable countNewsTipoGeral = new DataTable();
        //private DataTable countNewsTipoMkt = new DataTable();
        //private string mktPT = "1014";
        //private string mktAO = "1084";
        //private string mktMZ = "1085";

        protected void Page_Load(object sender, EventArgs e)
        {
            
            //this.chCountryCli.Click += new ImageMapEventHandler(this.chCountryGeral_Click);
            //this.chCountryGeral.Click += new ImageMapEventHandler(this.chCountryCli_Click);
            if (!Page.IsPostBack)
            {
                if (!Global.InsigteManager[Session.SessionID].IsLogged)
                    return;
                //lblDataToday.Visible = true;
                carregaDataTables();
                //chartMktWorldCli();
                //chartMktWorld();

            }
            else
            {
                //divChart2.Visible = false;
                //divChart1.Visible = false;
            }   
        }

        protected void chart1test()
        {
            //if (!Page.IsPostBack) return;

            //divChart1.Visible=true;

            //try
            //{
            //    if (countNewsTipoMkt.Rows.Count > 0)
            //    {
            //        Int32[] yValues = new Int32[countNewsTipoMkt.Rows.Count];
            //        string[] xValues = new string[countNewsTipoMkt.Rows.Count];
            //        string[] zValues = new string[countNewsTipoMkt.Rows.Count];

            //        int counter = 0;
            //        foreach (DataRow row in countNewsTipoMkt.Rows)
            //        {
            //            yValues[counter] = Convert.ToInt32(row["Total"].ToString());
            //            xValues[counter] = row["Country"].ToString();
            //            zValues[counter] = row["tipo"].ToString();
            //            counter++;
            //        }

            //        string AuxNew = "";
            //        int i = 0;
            //        foreach (string c in zValues)
            //        {
            //            if (!AuxNew.Contains(c))
            //            {
            //                Chart1.Series.Add(c);
            //                //por cor
            //                Chart1.Legends.Add(c);
            //                Chart1.Legends[c].ItemColumnSpacing = 2;
            //                AuxNew += c + " , ";
            //                i++;
            //            }

            //        }

            //        counter = 0;
            //        foreach (string c in zValues)
            //        {
            //            Chart1.Series[c].Points.AddXY(xValues[counter], yValues[counter]);
            //            counter++;
            //        }
            //        AuxNew = "";
            //        foreach (string c in zValues)
            //        {
            //            if (c != AuxNew)
            //            {
            //                // Set series chart type
            //                Chart1.Series[c].ChartType = SeriesChartType.Column;
            //                // Set series point width
            //                Chart1.Series[c]["PointWidth"] = "0.5";
            //                // Show data points labels
            //                Chart1.Series[c].IsValueShownAsLabel = true;
            //                // Set data points label style
            //                Chart1.Series[c]["BarLabelStyle"] = "Center";
            //                // Show as 3D
            //                //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
            //                // Draw as 3D Cylinder
            //                Chart1.Series[c]["DrawingStyle"] = "Default";
            //                Chart1.Series[c].ToolTip = "#VALX [#VALY]";
            //                Chart1.Series[c].IsXValueIndexed = false;
            //                Chart1.Series[c].IsVisibleInLegend = true;
            //            }
            //            AuxNew = c;
            //        }
            //        Chart1.Titles["ttChart1"].Text = "Teste";
            //        Chart1.Titles["ttChart1"].Alignment = ContentAlignment.TopLeft;

            //        Chart1.ChartAreas["caChart1"].AxisX.LabelStyle.Enabled = true;
            //        Chart1.ChartAreas["caChart1"].AxisX.TextOrientation = TextOrientation.Rotated270;
            //        Chart1.ChartAreas["caChart1"].AxisX.Interval = 1;
            //        Chart1.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //        Chart1.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //    }


            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Text = "<br>" + e.Message.ToString();
            //}

            

            
        }

        protected void chCountryGeral_Click(object sender, System.Web.UI.WebControls.ImageMapEventArgs e)
        {
            //string[] strSplit = e.PostBackValue.Split('-');

            //preencheChartTipoPaisGeral(strSplit[0], strSplit[1], Chart1);

            ////lblDataToday.Visible = true;

            ////lblDataToday.Text = e.PostBackValue.ToString();
            ////}
        }

        protected void chCountryCli_Click(object sender, System.Web.UI.WebControls.ImageMapEventArgs e)
        {
            //string[] strSplit = e.PostBackValue.Split('-');

            //preencheChartTipoPaisCli(strSplit[0], strSplit[1], Chart2);
              
            //    //lblDataToday.Visible = true;

            //    //lblDataToday.Text += "<br>" + e.PostBackValue.ToString();
            
        }

        private void preencheChartTipoPaisCli(String _meio, String Country, Chart _chartNome)
        {
            //if (!Page.IsPostBack) return;
            
            //divChart2.Visible = true;

            //string query = "";
            //try
            //{
                
            //    query += " select s.name as Mercado,count(1) as Total ";
            //    query += " from contents c ";
            //    query += " inner join metadata m ";
            //    query += " on c.id = m.id ";
            //    query += " and m.date > (getdate()-90) ";
            //    query += " and m.tipo <> 'Insigte' ";
            //    query += " and m.tipo = '" + _meio + "' ";
            //    query += " inner join clientarticles ca ";
            //    query += " on m.id = ca.id ";
            //    query += " inner join subject s ";
            //    query += " on ca.clientid = s.clienteid ";
            //    query += " and ca.subjectid = s.subjectid ";


            //    if (Country == "Mozambique")
            //    {
            //        query += " and ca.clientid = " + mktMZ;
            //        query += " where m.id in (select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //        query += " and (CONTAINS (text, '\"Moçambique\"') ";
            //        query += " or CONTAINS (text, '\"Mozambique\"') ";
            //        query += " or CONTAINS (text, '\"Tete\"') ";
            //        query += " or CONTAINS (text, '\"Maputo\"') ";
            //        query += " or CONTAINS (text, '\"Manica\"') ";
            //        query += " or CONTAINS (text, '\"Inhambane\"') ";
            //        query += " or CONTAINS (text, '\"Gaza\"') ";
            //        query += " or CONTAINS (text, '\"Sofala\"') ";
            //        query += " or CONTAINS (text, '\"Zambezia\"') ";
            //        query += " or CONTAINS (text, '\"Nampula\"') ";
            //        query += " or CONTAINS (text, '\"Niassa\"') ";
            //        query += " or CONTAINS (text, '\"Cabo Delgado\"')) ";
            //    }

            //    if (Country == "Angola")
            //    {
            //        query += " and ca.clientid = " + mktAO;
            //        query += " where m.id in (select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //        query += " and (CONTAINS (text, '\"Angola\"') ";
            //        query += " or CONTAINS (text, '\"Luanda\"') ";
            //        query += " or CONTAINS (text, '\"Huila\"') ";
            //        query += " or CONTAINS (text, '\"Huambo\"') ";
            //        query += " or CONTAINS (text, '\"Benguela\"') ";
            //        query += " or CONTAINS (text, '\"Namibe\"') ";
            //        query += " or CONTAINS (text, '\"Gunene\"') ";
            //        query += " or CONTAINS (text, '\"Cuando Cubango\"') ";
            //        query += " or CONTAINS (text, '\"Moxico\"') ";
            //        query += " or CONTAINS (text, '\"Lunda Sul\"') ";
            //        query += " or CONTAINS (text, '\"Lunda Norte\"') ";
            //        query += " or CONTAINS (text, '\"Malange\"') ";
            //        query += " or CONTAINS (text, '\"Kwanza Sul\"') ";
            //        query += " or CONTAINS (text, '\"Kwanza Norte\"') ";
            //        query += " or CONTAINS (text, '\"Uíge\"') ";
            //        query += " or CONTAINS (text, '\"Bemgo\"') ";
            //        query += " or CONTAINS (text, '\"Zaier\"') ";
            //        query += " or CONTAINS (text, '\"Cabinda\"') ";
            //        query += " or CONTAINS (text, '\"Bie\"')) ";
            //    }

            //    if (Country == "Portugal")
            //    {
            //        query += " and ca.clientid = " + mktPT;
            //        query += " where m.id in (select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //        query += " and (CONTAINS (text, '\"Portugal\"') ";
            //        query += " or CONTAINS (text, '\"Viana do Castelo\"') ";
            //        query += " or CONTAINS (text, '\"Bragança\"') ";
            //        query += " or CONTAINS (text, '\"Braga\"') ";
            //        query += " or CONTAINS (text, '\"Vila Real\"') ";
            //        query += " or CONTAINS (text, '\"Porto\"') ";
            //        query += " or CONTAINS (text, '\"Aveiro\"') ";
            //        query += " or CONTAINS (text, '\"Viseu\"') ";
            //        query += " or CONTAINS (text, '\"Guarda\"') ";
            //        query += " or CONTAINS (text, '\"Coimbra\"') ";
            //        query += " or CONTAINS (text, '\"Castelo Branco\"') ";
            //        query += " or CONTAINS (text, '\"Santarém\"') ";
            //        query += " or CONTAINS (text, '\"Portalegre\"') ";
            //        query += " or CONTAINS (text, '\"Setúbal\"') ";
            //        query += " or CONTAINS (text, '\"Évora\"') ";
            //        query += " or CONTAINS (text, '\"Beja\"') ";
            //        query += " or CONTAINS (text, '\"Faro\"') ";
            //        query += " or CONTAINS (text, '\"Madeira\"') ";
            //        query += " or CONTAINS (text, '\"Açores\"') ";
            //        query += " or CONTAINS (text, '\"Lisboa\"')) ";
            //    }

            //    query += " group by s.name ";

            //    DataTable countNewsPorMkt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "getcountNewsPorMkt");
            //    string nome = _chartNome.ID.ToString();

            //    string _chartSeries = "sr" + nome;
            //    string _chartAreas = "ca" + nome;
            //    string _chartTittle = "tt" + nome;

            //    if (countNewsPorMkt.Rows.Count > 0)
            //    {
            //        _chartNome.Titles[_chartTittle].Text = _meio + " | " + Country + " | " + Global.InsigteManager[Session.SessionID].NomCliente;

            //        Int32[] yValues = new Int32[countNewsPorMkt.Rows.Count];
            //        string[] xValues = new string[countNewsPorMkt.Rows.Count];

            //        int counter = 0;
            //        foreach (DataRow r in countNewsPorMkt.Rows)
            //        {
            //            yValues[counter] = Convert.ToInt32(r["Total"].ToString());
            //            xValues[counter] = r["Mercado"].ToString();

            //            counter++;
            //        }
            //        _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

            //        counter = 0;
            //        foreach (DataRow r in countNewsPorMkt.Rows)
            //        {
            //            _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#00add9");
            //            //_chartNome.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=" + r["CLID"].ToString() + "&tema=" + r["SBID"].ToString() + "&rg=" + nome + "&ym=" + r["ym"].ToString() + "&fon=0&dd=180";
            //            counter++;
            //        }
            //        _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
            //        _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
            //        _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
            //        _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
            //        _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
            //        _chartNome.Series[_chartSeries]["PieLabelStyle"] = "Outside";
            //        _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
            //        _chartNome.Series[_chartSeries].ToolTip = "#VALY [#VALX]";
            //        _chartNome.Series[_chartSeries].IsXValueIndexed = false;
            //        _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
            //        _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
            //        _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
            //        _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //        _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //    }
            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Visible = true;
            //    lblDataToday.Text = e.Message.ToString();
            //    lblDataToday.Text += "<br>" +query;

            //}
        }

        private void preencheChartTipoPaisGeral(String _meio,String Country, Chart _chartNome)
        {
            //if (!Page.IsPostBack) return;

            //divChart1.Visible = true;

            //string query = "";
            //query += " select s.name as Mercado,count(1) as Total ";
            //query += " from contents c ";
            //query += " inner join metadata m ";
            //query += " on c.id = m.id ";
            //query +=  " and m.date > (getdate()-90) ";
            //query +=  " and m.tipo <> 'Insigte' ";
            //query +=  " and m.tipo = '" +_meio +"' ";
            //query +=  " inner join clientarticles ca ";
            //query +=  " on m.id = ca.id ";
            //query +=  " inner join subject s ";
            //query +=  " on ca.clientid = s.clienteid ";
            //query +=  " and ca.subjectid = s.subjectid ";
			
            //if (Country == "Mozambique")
            //{
            //    query += " and ca.clientid = " + mktMZ  ;
            //    query += " where (CONTAINS (text, '\"Moçambique\"') ";
            //    query += " or CONTAINS (text, '\"Mozambique\"') ";
            //    query += " or CONTAINS (text, '\"Tete\"') ";
            //    query += " or CONTAINS (text, '\"Maputo\"') ";
            //    query += " or CONTAINS (text, '\"Manica\"') ";
            //    query += " or CONTAINS (text, '\"Inhambane\"') ";
            //    query += " or CONTAINS (text, '\"Gaza\"') ";
            //    query += " or CONTAINS (text, '\"Sofala\"') ";
            //    query += " or CONTAINS (text, '\"Zambezia\"') ";
            //    query += " or CONTAINS (text, '\"Nampula\"') ";
            //    query += " or CONTAINS (text, '\"Niassa\"') ";
            //    query += " or CONTAINS (text, '\"Cabo Delgado\"')) ";
            //}

            //if (Country == "Angola")
            //{
            //    query += " and ca.clientid = " + mktAO;
            //    query += " where (CONTAINS (text, '\"Angola\"') ";
            //    query += " or CONTAINS (text, '\"Luanda\"') ";
            //    query += " or CONTAINS (text, '\"Huila\"') ";
            //    query += " or CONTAINS (text, '\"Huambo\"') ";
            //    query += " or CONTAINS (text, '\"Benguela\"') ";
            //    query += " or CONTAINS (text, '\"Namibe\"') ";
            //    query += " or CONTAINS (text, '\"Gunene\"') ";
            //    query += " or CONTAINS (text, '\"Cuando Cubango\"') ";
            //    query += " or CONTAINS (text, '\"Moxico\"') ";
            //    query += " or CONTAINS (text, '\"Lunda Sul\"') ";
            //    query += " or CONTAINS (text, '\"Lunda Norte\"') ";
            //    query += " or CONTAINS (text, '\"Malange\"') ";
            //    query += " or CONTAINS (text, '\"Kwanza Sul\"') ";
            //    query += " or CONTAINS (text, '\"Kwanza Norte\"') ";
            //    query += " or CONTAINS (text, '\"Uíge\"') ";
            //    query += " or CONTAINS (text, '\"Bemgo\"') ";
            //    query += " or CONTAINS (text, '\"Zaier\"') ";
            //    query += " or CONTAINS (text, '\"Cabinda\"') ";
            //    query += " or CONTAINS (text, '\"Bie\"')) ";
            //}

            //if (Country == "Portugal")
            //{
            //    query += " and ca.clientid = " + mktPT;
            //    query += " where (CONTAINS (text, '\"Portugal\"') ";
            //    query += " or CONTAINS (text, '\"Viana do Castelo\"') ";
            //    query += " or CONTAINS (text, '\"Bragança\"') ";
            //    query += " or CONTAINS (text, '\"Braga\"') ";
            //    query += " or CONTAINS (text, '\"Vila Real\"') ";
            //    query += " or CONTAINS (text, '\"Porto\"') ";
            //    query += " or CONTAINS (text, '\"Aveiro\"') ";
            //    query += " or CONTAINS (text, '\"Viseu\"') ";
            //    query += " or CONTAINS (text, '\"Guarda\"') ";
            //    query += " or CONTAINS (text, '\"Coimbra\"') ";
            //    query += " or CONTAINS (text, '\"Castelo Branco\"') ";
            //    query += " or CONTAINS (text, '\"Santarém\"') ";
            //    query += " or CONTAINS (text, '\"Portalegre\"') ";
            //    query += " or CONTAINS (text, '\"Setúbal\"') ";
            //    query += " or CONTAINS (text, '\"Évora\"') ";
            //    query += " or CONTAINS (text, '\"Beja\"') ";
            //    query += " or CONTAINS (text, '\"Faro\"') ";
            //    query += " or CONTAINS (text, '\"Madeira\"') ";
            //    query += " or CONTAINS (text, '\"Açores\"') ";
            //    query += " or CONTAINS (text, '\"Lisboa\"')) ";
            //}

            //query +=  " group by s.name ";

            //DataTable countNewsPorMkt = Global.InsigteManager[Session.SessionID].getTableQuery(query, "getcountNewsPorMkt");
            //string nome = _chartNome.ID.ToString();

            //string _chartSeries = "sr" + nome;
            //string _chartAreas = "ca" + nome;
            //string _chartTittle = "tt" + nome;

            //if (countNewsPorMkt.Rows.Count > 0)
            //{
            //    _chartNome.Titles[_chartTittle].Text = _meio + " | " + Country + " | Geral";

            //    Int32[] yValues = new Int32[countNewsPorMkt.Rows.Count];
            //    string[] xValues = new string[countNewsPorMkt.Rows.Count];

            //    int counter = 0;
            //    foreach (DataRow r in countNewsPorMkt.Rows)
            //    {
            //        yValues[counter] = Convert.ToInt32(r["Total"].ToString());
            //        xValues[counter] = r["Mercado"].ToString();

            //        counter++;
            //    }
            //    _chartNome.Series[_chartSeries].Points.DataBindXY(xValues, yValues);

            //    counter = 0;
            //    foreach (DataRow r in countNewsPorMkt.Rows)
            //    {
            //        _chartNome.Series[_chartSeries].Points[counter].Color = ColorTranslator.FromHtml("#00add9");
            //        //_chartNome.Series[_chartSeries].Points[counter].Url = "~/Subject.aspx?clid=" + r["CLID"].ToString() + "&tema=" + r["SBID"].ToString() + "&rg=" + nome + "&ym=" + r["ym"].ToString() + "&fon=0&dd=180";
            //        counter++;
            //    }
            //    _chartNome.Series[_chartSeries].ChartType = SeriesChartType.Column;
            //    _chartNome.Titles[_chartTittle].Alignment = ContentAlignment.TopLeft;
            //    _chartNome.Series[_chartSeries]["PointWidth"] = "0.5";
            //    _chartNome.Series[_chartSeries].IsValueShownAsLabel = true;
            //    _chartNome.Series[_chartSeries]["BarLabelStyle"] = "Center";
            //    _chartNome.Series[_chartSeries]["PieLabelStyle"] = "Outside";
            //    _chartNome.Series[_chartSeries]["DrawingStyle"] = "Default";
            //    _chartNome.Series[_chartSeries].ToolTip = "#VALY [#VALX]";
            //    _chartNome.Series[_chartSeries].IsXValueIndexed = false;
            //    _chartNome.Series[_chartSeries].IsVisibleInLegend = true;
            //    _chartNome.ChartAreas[_chartAreas].AxisX.LabelStyle.Enabled = true;
            //    _chartNome.ChartAreas[_chartAreas].AxisX.Interval = 1;
            //    _chartNome.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //    _chartNome.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //}

        }

        protected void chartMktWorld()
        {
            //try
            //{
            //    if (countNewsTipoMkt.Rows.Count > 0)
            //    {
            //        Int32[] yValues = new Int32[countNewsTipoMkt.Rows.Count];
            //        string[] xValues = new string[countNewsTipoMkt.Rows.Count];
            //        string[] zValues = new string[countNewsTipoMkt.Rows.Count];

            //        int counter = 0;
            //        foreach (DataRow row in countNewsTipoMkt.Rows)
            //        {
            //            yValues[counter] = Convert.ToInt32(row["Total"].ToString());
            //            xValues[counter] = row["Country"].ToString();
            //            zValues[counter] = row["tipo"].ToString();
            //            counter++;
            //        }

            //        string AuxNew = "";
            //        int i = 0;
            //        foreach (string c in zValues)
            //        {
            //            if (!AuxNew.Contains(c))
            //            {
            //                chCountryCli.Series.Add(c);
            //                //por cor
            //                chCountryCli.Legends.Add(c);
            //                chCountryCli.Legends[c].ItemColumnSpacing = 2;
            //                AuxNew += c + " , ";
            //                i++;
            //            }
            //        }

            //        AuxNew = "";
            //        counter = 0;
            //        foreach (string c in zValues)
            //        {
            //            chCountryCli.Series[c].Points.AddXY(xValues[counter], yValues[counter]);
            //            //chCountryCli.Series[c].PostBackValue = c.ToString() + "-" + xValues[counter];
            //            counter++;
            //        }

            //        AuxNew = "";
            //        foreach (string c in zValues)
            //        {
            //            if (c != AuxNew)
            //            {

            //                //foreach (DataPoint pt in chCountryCli.Series[c].Points)
            //                //{
            //                //    pt.PostBackValue = c.ToString() + "-" + "#VALX";
            //                //}
            //                chCountryCli.Series[c].PostBackValue = c.ToString() + "-" + "#VALX";
            //                // Set series chart type
            //                chCountryCli.Series[c].ChartType = SeriesChartType.Column;
            //                // Set series point width
            //                chCountryCli.Series[c]["PointWidth"] = "0.5";
            //                // Show data points labels
            //                chCountryCli.Series[c].IsValueShownAsLabel = true;
            //                // Set data points label style
            //                chCountryCli.Series[c]["BarLabelStyle"] = "Center";
            //                // Show as 3D
            //                //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
            //                // Draw as 3D Cylinder
            //                chCountryCli.Series[c]["DrawingStyle"] = "Default";
            //                chCountryCli.Series[c].ToolTip = "#VALX [#VALY]";
            //                chCountryCli.Series[c].IsXValueIndexed = false;
            //                chCountryCli.Series[c].IsVisibleInLegend = true;
            //            }
            //            AuxNew = c;
            //        }
            //        chCountryCli.Titles["ttCountryCli"].Text = "Mercados | 90 Dias | Geral" ;
            //        chCountryCli.Titles["ttCountryCli"].Alignment = ContentAlignment.TopLeft;

            //        chCountryCli.ChartAreas["caCountryCli"].AxisX.LabelStyle.Enabled = true;
            //        chCountryCli.ChartAreas["caCountryCli"].AxisX.Interval = 1;
            //        chCountryCli.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //        chCountryCli.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //    }


            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Text = "<br>" + e.Message.ToString();
            //}


        }

        protected void chartMktWorldCli()
        {
            
            //try
            //{

            //    if (countNewsTipoGeral.Rows.Count > 0)
            //    {
            //        Int32[] yValues = new Int32[countNewsTipoGeral.Rows.Count];
            //        string[] xValues = new string[countNewsTipoGeral.Rows.Count];
            //        string[] zValues = new string[countNewsTipoGeral.Rows.Count];

            //        //foreach (DataRow row in countNewsTipoGeral.Rows)
            //        //{
            //        //     For each Row add a new series
            //        //    string seriesName = row["Country"].ToString();



            //        //    if (chCountryGeral.Series.IndexOf(seriesName) == -1)
            //        //    {
            //        //        chCountryGeral.Series.Add(seriesName);
            //        //        chCountryGeral.Series[seriesName].ChartType = SeriesChartType.Column;
            //        //        chCountryGeral.Series[seriesName].BorderWidth = 2;
            //        //    }

            //        //    for (int colIndex = 1; colIndex < countNewsTipoGeral.Columns.Count; colIndex++)
            //        //    {
            //        //         For each column (column 1 and onward) add the value as a point
            //        //        string columnName = countNewsTipoGeral.Columns[colIndex].ColumnName;
            //        //       int YVal = (int)row[columnName];

            //        //        chCountryGeral.Series[seriesName].Points.AddXY(row["tipo"].ToString(), row["Total"].ToString());
            //        //    }
            //        //}

            //        int counter = 0;
            //        foreach (DataRow row in countNewsTipoGeral.Rows)
            //        {
            //            yValues[counter] = Convert.ToInt32(row["Total"].ToString());
            //            xValues[counter] = row["Country"].ToString();
            //            zValues[counter] = row["tipo"].ToString();
            //            counter++;
            //        }

            //        string AuxNew = "";
            //        int i = 0;
            //        foreach (string c in zValues)
            //        {
            //            if (!AuxNew.Contains(c))
            //            {
            //                chCountryGeral.Series.Add(c);
            //                //por cor
            //                chCountryGeral.Legends.Add(c);
            //                chCountryGeral.Legends[c].ItemColumnSpacing = 2;
            //                AuxNew += c + " , ";
            //                i++;
            //            }
            //        }

            //        counter = 0;
            //        foreach (string c in zValues)
            //        {

            //            chCountryGeral.Series[c].Points.AddXY(xValues[counter], yValues[counter]);
            //            counter++;
            //        }
            //        AuxNew = "";
            //        foreach (string c in zValues)
            //        {
            //            if (c != AuxNew)
            //            {
            //                chCountryGeral.Series[c].PostBackValue = c.ToString() + "-" + "#VALX";
            //                // Set series chart type
            //                chCountryGeral.Series[c].ChartType = SeriesChartType.Column;
            //                // Set series point width
            //                chCountryGeral.Series[c]["PointWidth"] = "0.5";
            //                // Show data points labels
            //                chCountryGeral.Series[c].IsValueShownAsLabel = true;
            //                // Set data points label style
            //                chCountryGeral.Series[c]["BarLabelStyle"] = "Center";
            //                // Show as 3D
            //                //Chart1.ChartAreas[_chartSeries].Area3DStyle.Enable3D = false;
            //                // Draw as 3D Cylinder
            //                chCountryGeral.Series[c]["DrawingStyle"] = "Default";
            //                chCountryGeral.Series[c].ToolTip = "#VALX [#VALY]";
            //                chCountryGeral.Series[c].IsXValueIndexed = false;
            //                chCountryGeral.Series[c].IsVisibleInLegend = true;
            //            }
            //            AuxNew = c;
            //        }
            //        chCountryGeral.Titles["ttCountryGeral"].Text = "Mercados | 90 Dias | " + Global.InsigteManager[Session.SessionID].NomCliente.ToString();
            //        chCountryGeral.Titles["ttCountryGeral"].Alignment = ContentAlignment.TopLeft;

            //        chCountryGeral.ChartAreas["caCountryGeral"].AxisX.LabelStyle.Enabled = true;
            //        chCountryGeral.ChartAreas["caCountryGeral"].AxisX.Interval = 1;
            //        chCountryGeral.ChartAreas[0].AxisX.MajorGrid.LineWidth = 0;
            //        chCountryGeral.ChartAreas[0].AxisY.MajorGrid.LineWidth = 0;
            //    }
            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Text = "<br>" + e.Message.ToString();
            //}
        }

        protected void carregaDataTables()
        {

            //string query = "";
            //string queryCon = "";
            ////string querycountNewsTipoGeral = "";
            ////string querycountNewsTipoMkt = "";

            //try
            //{

            //    queryCon += " select COD_SUBTEMAS from CL10H_CLIENT_SUBJECT where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient;
            //    queryCon += "and DES_TAB = 'Concorrência' and ID_CLIENT_USER = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;

            //    ConcorrenciaCount = Global.InsigteManager[Session.SessionID].getTableQuery(queryCon, "getConcorrenciaCount");

            //    String Filtro = "";
            //    if (ConcorrenciaCount.Rows.Count == 1)
            //    {

            //        foreach (DataRow xxx in ConcorrenciaCount.Rows)
            //        {
            //            Filtro = xxx["COD_SUBTEMAS"].ToString().Replace(";", "','");
            //        }
            string CliCon = "";
            CliCon += " select COD_VALOR as Country, VAL_VALOR as Total from GM10f_TEMA_COUNTRY with(nolock) ";
            CliCon += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and COD_QUERY = 'MKTCONTOT' ";

            sumCliCon = Global.InsigteManager[Session.SessionID].getTableQuery(CliCon, "Conta");
            //    }

            //    //get noticias do cliente
            //    query += " select 'Mozambique' as Country, count(1) as Total ";
            //    query += " from contents c  ";
            //    query += " inner join metadata m on c.id = m.id and m.date > (getdate()-90) and m.tipo <> 'Insigte'   ";
            //    query += " inner join clientarticles ca  on m.id = ca.id   ";
            //    //query += " where m.id in(select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //    //query += " where (CONTAINS (text, '\"Moçambique\"')   or CONTAINS (text, '\"Mozambique\"')  or CONTAINS (text, '\"Tete\"')   or CONTAINS (text, '\"Maputo\"')   ";
            //    query += " where (CONTAINS (text, '\"Tete\"')   or CONTAINS (text, '\"Maputo\"')   ";
            //    query += " or CONTAINS (text, '\"Manica\"')   or CONTAINS (text, '\"inhambane\"')  or CONTAINS (text, '\"Gaza\"')   or CONTAINS (text, '\"Sofala\"')    ";
            //    query += " or CONTAINS (text, '\"Zambezia\"')   or CONTAINS (text, '\"Nampula\"')  or CONTAINS (text, '\"Niassa\"') 	  or CONTAINS (text, '\"Cabo Delgado\"') ) ";
            //    query += " and cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //    query += " union all ";
            //    query += " select 'Angola' as Country, count(1) as Total   ";
            //    query += " from contents c  ";
            //    query += " inner join metadata m on c.id = m.id and m.date > (getdate()-90) and m.tipo <> 'Insigte'   ";
            //    query += " inner join clientarticles ca  on m.id = ca.id   ";
            //   //query += " where m.id in(select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //    //query += " where (CONTAINS (text, '\"Angola\"')   or CONTAINS (text, '\"Luanda\"')   or CONTAINS (text, '\"Huila\"')  or CONTAINS (text, '\"Huambo\"')    ";
            //    query += " where (CONTAINS (text, '\"Luanda\"')   or CONTAINS (text, '\"Huíla\"')  or CONTAINS (text, '\"Huambo\"')    ";
            //    query += " or CONTAINS (text, '\"Benguela\"')  or CONTAINS (text, '\"Namibe\"')   or CONTAINS (text, '\"Gunene\"')   or CONTAINS (text, '\"Cuando Cubango\"')    ";
            //    query += " or CONTAINS (text, '\"Moxico\"')  or CONTAINS (text, '\"Lunda-Sul\"')  or CONTAINS (text, '\"Lunda-Norte\"')  or CONTAINS (text, '\"Malange\"')   ";
            //    query += " or CONTAINS (text, '\"Kwanza-Sul\"')  or CONTAINS (text, '\"Kwanza-Norte\"')  or CONTAINS (text, '\"Uíge\"')  or CONTAINS (text, '\"Bengo\"')   ";
            //    query += " or CONTAINS (text, '\"Zaier\"')  or CONTAINS (text, '\"Cabinda\"')  or CONTAINS (text, '\"Bie\"') ) ";
            //    query += " and cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //    query += " union all  ";
            //    query += " select 'Portugal' as Country, count(1) as Total   ";
            //    query += " from contents c  ";
            //    query += " inner join metadata m on c.id = m.id and m.date > (getdate()-90) and m.tipo <> 'Insigte'   ";
            //    query += " inner join clientarticles ca  on m.id = ca.id   ";
            //    //query += " where m.id in(select id from clientarticles where cast(clientid as varchar) + cast(subjectid as varchar) = '" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";
            //    //query += " where (CONTAINS (text, '\"Portugal\"')  or CONTAINS (text, '\"Viana do Castelo\"')  or CONTAINS (text, '\"Bragança\"')  or CONTAINS (text, '\"Braga\"')   ";
            //    query += " where (CONTAINS (text, '\"Viana do Castelo\"')  or CONTAINS (text, '\"Bragança\"')  or CONTAINS (text, '\"Braga\"')   ";
            //    query += " or CONTAINS (text, '\"Vila Real\"')  or CONTAINS (text, '\"Porto\"')  or CONTAINS (text, '\"Aveiro\"')  or CONTAINS (text, '\"Viseu\"')   ";
            //    query += " or CONTAINS (text, '\"Guarda\"')  or CONTAINS (text, '\"Coimbra\"')  or CONTAINS (text, '\"Castelo Branco\"')  or CONTAINS (text, '\"Santarém\"')   ";
            //    query += " or CONTAINS (text, '\"Portalegre\"')  or CONTAINS (text, '\"Setúbal\"')  or CONTAINS (text, '\"Évora\"')  or CONTAINS (text, '\"Beja\"')   ";
            //    query += " or CONTAINS (text, '\"Faro\"')  or CONTAINS (text, '\"Madeira\"')  or CONTAINS (text, '\"Açores\"')  or CONTAINS (text, '\"Lisboa\"') )";
            //    query += " and cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + Global.InsigteManager[Session.SessionID].TemaCliente + "')";

            string query = " select COD_VALOR as Country, VAL_VALOR as Total from GM10f_TEMA_COUNTRY with(nolock) ";
            query += " where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + " and COD_QUERY = 'MKTCLITOT' ";

            countNews = Global.InsigteManager[Session.SessionID].getTableQuery(query, "getContNews");

            //}
            //catch (Exception e)
            //{
            //    lblDataToday.Text += "<br>" + e.Message.ToString();
            //    lblDataToday.Text += "<br>" + query;
            //}
        }

        protected string getPaths()
        {
            int concorrenciaTotalPT = 0;
            int concorrenciaTotalAO = 0;
            int concorrenciaTotalMZ = 0;

            if (sumCliCon.Rows.Count > 0)
            {
                try
                {
                    if (sumCliCon.Rows[0]["Total"] != null)
                        if (sumCliCon.Rows[0]["Total"].ToString() != "")
                            concorrenciaTotalMZ += Convert.ToInt32(sumCliCon.Rows[0]["Total"].ToString());
                }
                catch (Exception)
                {
                }
                try
                {
                    if (sumCliCon.Rows[1]["Total"] != null)
                        if (sumCliCon.Rows[1]["Total"].ToString() != "")
                            concorrenciaTotalAO += Convert.ToInt32(sumCliCon.Rows[1]["Total"].ToString());
                }
                catch (Exception)
                {
                }
                try
                {
                    if (sumCliCon.Rows[2]["Total"] != null)
                        if (sumCliCon.Rows[2]["Total"].ToString() != "")
                            concorrenciaTotalPT += Convert.ToInt32(sumCliCon.Rows[2]["Total"].ToString());
                }
                catch (Exception)
                {
                }
            }

            string path = "";
            try
            {
                try
                {
                    if (countNews.Rows[1]["Total"].ToString() != "0")
                        path += " <path id=\"AO\" concorrencia=\"" + concorrenciaTotalAO + "\" value=\"" + (Convert.ToInt32(countNews.Rows[1]["Total"].ToString()) + concorrenciaTotalAO) + "\" nome=\"Angola\" onclick=\"mouseClick(this);\" onmousemove=\"SetValues(this);\" onmouseover=\"mouseOver(this);\" onmouseout=\"mouseOut(this);\" d=\"M 599,398L 599,401L 600,401L 600,403L 601,403L 601,404L 602,404L 602,406L 603,406L 603,409L 602,409L 602,414L 603,414L 603,417L 604,417L 604,420L 605,420L 605,423L 604,423L 604,425L 603,425L 603,427L 602,427L 602,429L 601,429L 601,431L 600,431L 600,432L 599,432L 599,436L 598,436L 598,440L 597,440L 597,442L 596,442L 596,448L 599,448L 599,447L 604,447L 604,448L 609,448L 609,449L 616,449L 616,448L 623,448L 623,449L 627,449L 627,450L 631,450L 631,451L 637,451L 637,450L 642,450L 642,449L 641,449L 641,447L 640,447L 640,446L 639,446L 639,445L 638,445L 638,443L 637,443L 637,432L 638,432L 638,430L 639,430L 639,429L 645,429L 645,428L 646,428L 646,422L 647,422L 647,421L 646,421L 646,420L 643,420L 643,421L 640,421L 640,420L 639,420L 639,418L 638,418L 638,414L 637,414L 637,404L 636,404L 636,403L 632,403L 632,402L 629,402L 629,404L 628,404L 628,406L 627,406L 627,407L 624,407L 624,408L 621,408L 621,407L 620,407L 620,406L 619,406L 619,405L 618,405L 618,403L 617,403L 617,400L 616,400L 616,398L 599,398 Z \"/> ";
                }
                catch (Exception)
                {
                }
                try
                {
                    if (countNews.Rows[0]["Total"].ToString() != "0")
                        path += " <path id=\"MZ\" concorrencia=\"" + concorrenciaTotalMZ + "\" value=\"" + (Convert.ToInt32(countNews.Rows[0]["Total"].ToString()) + concorrenciaTotalMZ) + "\" nome=\"Mozambique\" onclick=\"mouseClick(this);\" onmousemove=\"SetValues(this);\" onmouseover=\"mouseOver(this);\" onmouseout=\"mouseOut(this);\" d=\"M 679,492L 678,492L 678,482L 677,482L 677,478L 676,478L 676,474L 677,474L 677,472L 676,472L 676,471L 678,471L 678,470L 679,470L 679,468L 680,468L 680,466L 681,466L 681,464L 682,464L 682,461L 683,461L 683,457L 684,457L 684,453L 685,453L 685,451L 684,451L 684,449L 683,449L 683,447L 682,447L 682,446L 681,446L 681,445L 679,445L 679,444L 677,444L 677,443L 674,443L 674,442L 670,442L 670,441L 671,441L 671,440L 672,440L 672,439L 674,439L 674,438L 676,438L 676,437L 678,437L 678,436L 679,436L 679,435L 681,435L 681,434L 683,434L 683,435L 685,435L 685,436L 687,436L 687,437L 688,437L 688,438L 689,438L 689,441L 688,441L 688,444L 689,444L 689,445L 691,445L 691,446L 692,446L 692,445L 693,445L 693,444L 694,444L 694,443L 695,443L 695,442L 696,442L 696,438L 695,438L 695,434L 694,434L 694,433L 693,433L 693,432L 692,432L 692,431L 691,431L 691,430L 690,430L 690,426L 691,426L 691,423L 697,423L 697,424L 703,424L 703,423L 706,423L 706,422L 709,422L 709,421L 712,421L 712,420L 714,420L 714,419L 715,419L 715,422L 714,422L 714,428L 715,428L 715,433L 716,433L 716,436L 715,436L 715,438L 714,438L 714,440L 713,440L 713,442L 712,442L 712,443L 711,443L 711,444L 710,444L 710,445L 709,445L 709,446L 707,446L 707,447L 705,447L 705,448L 703,448L 703,449L 701,449L 701,450L 699,450L 699,451L 698,451L 698,453L 697,453L 697,455L 696,455L 696,456L 695,456L 695,457L 694,457L 694,458L 693,458L 693,459L 692,459L 692,460L 691,460L 691,461L 690,461L 690,463L 689,463L 689,466L 690,466L 690,468L 691,468L 691,470L 692,470L 692,472L 693,472L 693,476L 692,476L 692,479L 691,479L 691,480L 690,480L 690,481L 689,481L 689,482L 687,482L 687,483L 685,483L 685,484L 683,484L 683,485L 681,485L 681,487L 680,487L 680,490L 679,490L 679,492 Z \"/> ";
                }
                catch (Exception)
                {
                }
                try
                {
                    if (countNews.Rows[2]["Total"].ToString() != "0")
                        path += " <path id=\"PT\" concorrencia=\"" + concorrenciaTotalPT + "\" value=\"" + (Convert.ToInt32(countNews.Rows[2]["Total"].ToString()) + concorrenciaTotalPT) + "\" nome=\"Portugal\" onclick=\"mouseClick(this);\" onmousemove=\"SetValues(this);\" onmouseover=\"mouseOver(this);\" onmouseout=\"mouseOut(this);\" d=\"M 518,180L 520,180L 520,181L 522,181L 522,182L 526,182L 526,183L 525,183L 525,185L 524,185L 524,187L 523,187L 523,190L 522,190L 522,194L 523,194L 523,196L 522,196L 522,198L 521,198L 521,200L 520,200L 520,202L 516,202L 516,201L 515,201L 515,200L 516,200L 516,197L 515,197L 515,192L 516,192L 516,188L 517,188L 517,183L 518,183L 518,180 Z \"/> ";
                }
                catch (Exception)
                {
                }
            }
            catch (Exception e)
            {
                lblDataToday.Text += "<br>" + e.Message.ToString();
                lblDataToday.Text = "<br>" + path;
            }//
            return path;
        }
    }
}