﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class SubscribeCP : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            dgPasta.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgPasta.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            if (!Page.IsPostBack)
            {
                //getPastasContent(getPastas());
                getPastasContent(null);
            }
        }

        protected void tvTemas_SelectedIndexChanged(object sender, EventArgs e)
        {
            //getPastasContent(tvTemas.SelectedItem.Value.ToString());
        }

        public string getPastaID()
        {
            //return tvTemas.SelectedItem.Value;
            return null;
        }

        public string getPastas()
        {
            //tvTemas.Visible = true;

            //tvTemas.Items.Clear();

            //foreach (var a in Global.InsigteManager[Session.SessionID].inUser.Temas)
            //{
            //    tvTemas.Items.Add(new ListItem(a.Value.Name,a.Value.IdTema));
            //}

            //if (tvTemas.SelectedIndex < 0)
            //    return string.Empty;
            //else
            //    return tvTemas.SelectedItem.Value;

            return null;
        }

        public void getPastasContent(string Tema)
        {
            
            dgPasta.Visible = true;

            String Query = "";

            //foreach (var a in Global.InsigteManager[Session.SessionID].inUser.Temas)
            //{
            //    if (a.Value.IdTema == Tema)
            //    {
            //        if (Tema.Substring(4, 3) == "000")
            //        {
            //            if (a.Value.SubTemas.Substring(4, 3) == "000")
            //            {
            //                Query = " select m.TITLE, m.EDITOR, m.DATE, m.ID as IDMETA, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo ";
            //                Query += "   from metadata m ";
            //                Query += " 	   inner join clientarticles ca ";
            //                Query += " 			   on m.id = ca.id ";
            //                Query += " 	   inner join contents c ";
            //                Query += " 			   on m.id = c.id ";
            //                Query += "  where convert(varchar,m.date,112) = convert(varchar,getdate(),112) ";
            //                Query += "    and m.id not in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV where id_client = "+ Global.InsigteManager[Session.SessionID].IdClient +") ";
            //                Query += "    and cast(ca.clientid as varchar) = '" + a.Value.IdTema.Substring(0,4) + "'";
            //            }
            //            else
            //            {

            //                Query = " select m.TITLE, m.EDITOR, m.DATE, m.ID as IDMETA, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo ";
            //                Query += "   from metadata m ";
            //                Query += " 	   inner join clientarticles ca ";
            //                Query += " 			   on m.id = ca.id ";
            //                Query += " 	   inner join contents c ";
            //                Query += " 			   on m.id = c.id ";
            //                Query += "  where convert(varchar,m.date,112) = convert(varchar,getdate(),112) ";
            //                Query += "    and m.id not in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + ") ";
            //                Query += "    and cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + a.Value.SubTemas.Replace(";","','") + "') ";
            //            }
            //        }
            //        else
            //        {
            //            Query = " select m.TITLE, m.EDITOR, m.DATE, m.ID as IDMETA, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo ";
            //            Query += "   from metadata m ";
            //            Query += " 	   inner join clientarticles ca ";
            //            Query += " 			   on m.id = ca.id ";
            //            Query += " 	   inner join contents c ";
            //            Query += " 			   on m.id = c.id ";
            //            Query += "  where convert(varchar,m.date,112) = convert(varchar,getdate(),112) ";
            //            Query += "    and m.id not in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + ") ";
            //            Query += "    and cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('" + Tema + "') ";
            //        }
            //    }


            //    lblDataToday.Text = Query;
            //    lblDataToday.Visible = true;
            //}
            string query = " SELECT COD_USER_VISABILITY FROM [iadvisers].[dbo].[CL10H_CLIENT_USERS] with(nolock) where id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString();
            DataTable NewsLetterChoice = Global.InsigteManager[Session.SessionID].getTableQuery(query, "GetUserVisability");
            
            if ((NewsLetterChoice.Rows[0]["COD_USER_VISABILITY"].ToString().Length - 1 >= 13))
            {
                if (!NewsLetterChoice.Rows[0]["COD_USER_VISABILITY"].ToString().ElementAt(13).ToString().Equals("1"))
                {
                    Query = " select distinct m.TITLE, m.EDITOR, m.DATE, m.ID as IDMETA, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo  ";
                    Query += "   from metadata m with(nolock) ";
                    Query += " 	   inner join clientarticles ca with(nolock) ";
                    Query += " 			   on m.id = ca.id ";
                    Query += " 	   inner join contents c with(nolock) ";
                    Query += " 			   on m.id = c.id ";
                    Query += "  where convert(varchar,m.date,112) = convert(varchar,getdate(),112) ";
                    Query += "    and m.id not in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + ") ";
                    Query += "    and (cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ( '1014102','1014103','1014106','1014111','1014110','1084102','1084103','1084106','1084111','1084110','1085102','1085103','1085106','1085111','1085110') ";
                    Query += "		Or m.id in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112))) ";
                }
                else
                {
                    Query = " select distinct m.ID as IDMETA, m.TITLE, m.EDITOR, m.DATE, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo ";
                    Query += " from metadata m with(nolock) ";
                    Query += " inner join clientarticles ca with(nolock) ";
                    Query += " on m.id = ca.id ";
                    Query += " inner join contents c with(nolock) ";
                    Query += " on m.id = c.id ";
                    Query += " where m.id in (select id_news from ML10F_CLIENT_NEWSLETTER_CHOICE with(nolock) where id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " )";
                }

            }
            else
            {
                Query = " select distinct m.TITLE, m.EDITOR, m.DATE, m.ID as IDMETA, m.filepath, m.page_url, replace(substring(c.text,1,500),'\"','') + '...' as text, m.tipo  ";
                Query += "   from metadata m with(nolock) ";
                Query += " 	   inner join clientarticles ca with(nolock) ";
                Query += " 			   on m.id = ca.id ";
                Query += " 	   inner join contents c with(nolock) ";
                Query += " 			   on m.id = c.id ";
                Query += "  where convert(varchar,m.date,112) = convert(varchar,getdate(),112) ";
                Query += "    and m.id not in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ") ";
                Query += "    and (cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) in ('1014102','1014103','1014106','1014111','1014110','1084102','1084103','1084106','1084111','1084110','1085102','1085103','1085106','1085111','1085110') ";
                Query += "		Or m.id in (select ID_NEWS from ML10F_CLIENT_USER_CONTROL_COMPOSED_ADD with(nolock) where convert(varchar,DAT_CREATE,112)=convert(varchar,getdate(),112))) ";
            }
            //Query += "";

            //lblDataToday.Visible = true;
            //lblDataToday.Text = Query;

            DataTable NewsLetterCP = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "NewsLetterCP");
            dgPasta.DataSource = NewsLetterCP;
            dgPasta.DataBind();
            
        }

        protected void bt_Delete_Command(Object sender, CommandEventArgs e)
        {
            String Query = "insert into ML10F_CLIENT_USER_CONTROL_COMPOSED_RMV ";
                   Query += "(ID_CLIENT, COD_NEWSLETTER_COMPOSE, ID_NEWS, DAT_CREATE) ";
                   Query += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", 1, " + e.CommandArgument.ToString() + ", getdate() ";

            DataTable del = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "DELNewsLetterCP");
            getPastasContent(getPastas());
            
        }

        protected void btnSendEmail_Click(object sender, EventArgs e)
        {
            //lblDataToday.Visible = false;
            //lblDataToday.Text = Global.InsigteManager[Session.SessionID].executeSP("SPcomposedEmails", Global.InsigteManager[Session.SessionID].IdClient);

            DataTable envia = Global.InsigteManager[Session.SessionID].getTableQuery("update CL10H_CLIENT_COMPOSED set IND_RUN = 1 where ID_CLIENT = " + Global.InsigteManager[Session.SessionID].IdClient, "SetEnvia");
            lblDataToday.Visible = true;
            lblDataToday.Text = "A processar o email! Esta operação pode levar até 5 minutos a executar.";
            

            getPastasContent(null);
            
        }

        protected string sReturnLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;
            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('RemNews.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/folderplus32_rem.png' width='16px' height='20px' alt='Remover do Arquivo' title='Remover do Arquivo' style='border-width:0px;'/></a>";
            return sLink;
        }
        
        protected void dgPasta_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgPasta.CurrentPageIndex = e.NewPageIndex;
            getPastasContent(getPastas());
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0, 4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4, 3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;

            }
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }
    }
}