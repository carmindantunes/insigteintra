﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class ListMod : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }
            if (!Page.IsPostBack)
            {

                string qsIDPasta = Request.QueryString["PID"];
                getListInfo(qsIDPasta);
            }
        }

        public void getListInfo(string pasta)
        {
            DataTable lists = Global.InsigteManager[Session.SessionID].getTableQuery("select * from CL10D_LISTS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and ID_LIST = " + pasta, "getList");

            foreach (DataRow dr in lists.Rows)
            {
                NomePasta.Text = dr["NOM_LIST"].ToString();
                tbNota.Text = dr["DES_LIST"].ToString();
            }
            
        }

        protected void btMod_Click(object sender, EventArgs e)
        {
            string qsIDPasta = Request.QueryString["PID"];

            String Query = " update CL10D_LISTS set NOM_LIST = '" + NomePasta.Text + "', DES_LIST = '" + tbNota.Text + "' ";
                   Query+= " where id_client = '" + Global.InsigteManager[Session.SessionID].IdClient + "' and id_client_user = '" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + "' and ID_LIST = " + qsIDPasta;

            DataTable lists = Global.InsigteManager[Session.SessionID].getTableQuery(Query, "UpdList");

            Response.Redirect("listscfg.aspx");

            //Response.Write("<script language='javascript'> { self.close() }</script>");

        }

        protected void cancelar_click(object sender, EventArgs e)
        {
            Response.Redirect("listscfg.aspx");
        }
    }
}