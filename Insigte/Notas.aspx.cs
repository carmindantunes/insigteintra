﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;

namespace Insigte
{
    public partial class Notas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Page.IsPostBack)
            {
                string qsIDArtigo = Request.QueryString["ID"];
                string qsIDPasta = Request.QueryString["PID"];

                //lbinfo.Text = qsIDArtigo + ":" + qsIDPasta;

                string cmdQueryPastas = "select nota from dbo.CL10H_PASTAS_ARTICLES with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid=" + qsIDPasta + " and articleid=" + qsIDArtigo;
                SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
                try
                {
                    conn.Open();

                    SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                    cmd.Connection = conn;
                    cmd.CommandType = CommandType.Text;

                    SqlDataReader dr = cmd.ExecuteReader();

                    if (dr.HasRows == true)
                    {
                        while (dr.Read())
                            tbNota.Text = dr["nota"].ToString();
                    }

                    dr.Close();

                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                }
                finally
                {
                    conn.Dispose();
                    conn.Close();
                }
            }
        }



        protected void btSalvarNota_Click(object sender, EventArgs e)
        {
            string qsIDArtigo = Request.QueryString["ID"];
            string qsIDPasta = Request.QueryString["PID"];

            //lbinfo.Text = qsIDArtigo + ":" + qsIDPasta;

            string cmdQueryPastas = "update dbo.CL10H_PASTAS_ARTICLES set nota='" + tbNota.Text.ToString() + "' where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " and pastaid=" + qsIDPasta + " and articleid=" + qsIDArtigo;
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                //lbinfo.Text += " - " + cmdQueryPastas;

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

            Response.Write("<script language='javascript'> { self.close() }</script>");

        }

    }
}