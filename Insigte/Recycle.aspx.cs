﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Globalization;

namespace Insigte
{
    public partial class Recycle : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblDataToday.Text = "Lisboa, " + DateTime.Now.ToString("dd") + " de " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("pt-PT")) + " de " + DateTime.Now.ToString("yyyy");
            dgNewsRecycleACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsRecycleACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());
            

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null)
            {
                if (!Page.IsPostBack)
                {
                    getNewsArchive();
                }
            }
            else
            {
                lblWarning.Text = lblWarning.Text = Resources.insigte.language.defNaoTemRecicladas;
                lblWarning.Visible = true;
            } 
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public void getNewsArchive()
        {
            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] != null)
            {
                if (cookie.Values["DELNOT"].ToString().Length > 0)
                {
                    dgNewsRecycleACN.Visible = true;

                    if (cookie.Values["DELNOT"].ToString().Length <= 0)
                    {
                        btnClearRecycleList.Visible = false;
                    }

                    string[] words = cookie.Values["DELNOT"].ToString().Split('|');
                    string idsArt = string.Empty;


                    foreach (string value in words)
                    {
                        idsArt += "'" + value + "',";
                    }

                    idsArt = idsArt.Substring(0, idsArt.LastIndexOf(','));

                    String Title = "TITLE";
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    }

                    string cmdQuery = "SELECT " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath,page_url ";
                    cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
                    cmdQuery += " where iadvisers.dbo.metadata.ID in (" + idsArt + ") ";
                    cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";

                    SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                    try
                    {
                        conn.Open();

                        SqlCommand cmd = new SqlCommand(cmdQuery);
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        DataSet tempSet;
                        SqlDataAdapter iadvisers_ACN_NEWS;

                        iadvisers_ACN_NEWS = new SqlDataAdapter(cmd);

                        tempSet = new DataSet("tempSet");
                        iadvisers_ACN_NEWS.Fill(tempSet);

                        dgNewsRecycleACN.DataSource = tempSet;
                        dgNewsRecycleACN.DataBind();
                        dgNewsRecycleACN.Visible = true;
                    }
                    catch (Exception exp)
                    {
                        exp.Message.ToString();
                    }
                }
                else
                {
                    lblWarning.Text = Resources.insigte.language.defNaoTemRecicladas;
                    btnClearRecycleList.Visible = false;
                    lblWarning.Visible = true;
                }
            }
            else
            {
                lblWarning.Text = Resources.insigte.language.defNaoTemRecicladas;
                btnClearRecycleList.Visible = false;
                lblWarning.Visible = true;
            }
        }

        protected string sReturnLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;
            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('RemRec.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/icons/black/png/playback_reload_icon_16.png' width='16px' height='16px' alt='" + Resources.insigte.language.defColAccoesReciclar + "' title='" + Resources.insigte.language.defColAccoesReciclar + "' style='border-width:0px;'/></a>";
            return sLink;
        }


        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;
            sLink = strValue;
            return sLink;
        }

        protected string sReturnLinkPDF(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["PDF"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["PDF"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "#" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpapercheck32_color.png' width='16px' height='16px' alt='Notícia Adicionada' title='Notícia Adicionada' style='border-width:0px;'/></a>";
                        }
                        else
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "#" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpapercheck32_color.png' width='16px' height='16px' alt='Notícia Adicionada' title='Notícia Adicionada' style='border-width:0px;'/></a>";
                        }
                        else
                        {
                            sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
                        }
                    }
                }
            }
            else
            {
                sLink = "<a target=" + "\"" + "_self" + "\"" + " href=" + "\"" + "javascript:window.open('AddPDF.aspx?ID=" + strValue + "','CustomPopUp', 'width=400, height=200, menubar=no, resizeble=no, location=no'); void(0)" + "\"" + " style='border-width:0px;text-decoration:none;' >" + "<img src='Imgs/linedpaperplus32.png' width='16px' height='16px' alt='Compilar Notícia' title='Compilar Notícia' style='border-width:0px;'/></a>";
            }
            return sLink;
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0,4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4,3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;
            }
        }

        protected void btnClearRecycleList_Click(object sender, EventArgs e)
        {
            string ValCookie = string.Empty;
            string DelCookie = string.Empty;
            string PDFCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            ValCookie = cookie.Values["ID"];
            PDFCookie = cookie.Values["PDF"];
            DelCookie = "";

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(7);
            Response.Cookies.Add(newCookie);

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        private void RemArt(string qsIDArtigo)
        {
            try
            {
                string ValCookie = string.Empty;
                string DelCookie = string.Empty;
                string PDFCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];


                //LINQ
                if (cookie.Values["DELNOT"].ToString().Contains('|'))
                {
                    string[] oldDelCookie = cookie.Values["DELNOT"].ToString().Split('|');
                    string numToRemove = qsIDArtigo;
                    oldDelCookie = oldDelCookie.Where(val => val != numToRemove).ToArray();

                    foreach (string value in oldDelCookie)
                    {
                        DelCookie += value + "|";
                    }
                }

                if (DelCookie != string.Empty)
                {
                    DelCookie = DelCookie.Substring(0, DelCookie.LastIndexOf('|'));
                }
                else
                {
                    DelCookie = "";
                }

                ValCookie = cookie.Values["ID"];
                PDFCookie = cookie.Values["PDF"];

                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

            }
            catch (Exception exp)
            {
                //lblArtigoRem.Text = exp.Message.ToString() + " - " + exp.StackTrace.ToString() + " - " + exp.Data.ToString();
            }

            Response.Redirect("Recycle.aspx");
        }

        protected void bt_Recycle_Command(Object sender, CommandEventArgs e)
        {
            RemArt(e.CommandArgument.ToString());
            //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            //ImageButton b = sender as ImageButton;
            //b.ImageUrl = "Imgs/folderplus32_add.png";

            dgNewsRecycleACN.Items[Convert.ToInt32(e.CommandName)].Visible = false;

        }
    }
}