﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="NewsMap.aspx.cs" Inherits="Insigte.NewsMap" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 5px auto;
        background-color: #323232;
        color: #FFFFFF;
        bottom:
    } 
    
    .mapMz
    {
       float:left;
       /*border:1px solid black;*/
       margin:15px 5px 10px 15px;
       width:360px;
       height:556px;
    }

    .info
    {
        /*border:1px solid black;*/
        display:block; 
        position:absolute;               
        border-radius:5px; 
        font-family:Arial;
        font-size:12px;
    }

    .infoNome
    {    
        color:white;        
        background-color:#323232;
        padding:2px;
        border-top-left-radius:5px;
        border-top-right-radius:5px; 
                
    }
    
    .infoDesc
    {
        padding:2px;
        background-color:#f0f0f0; 
        border-bottom-left-radius:5px;
        border-bottom-right-radius:5px;            
    }
    
    .regionInfo
    {
        float:right;
        border:0px solid black;
        margin:15px 15px 10px 0px;
       /* height:556px;*/
        width:400px;
        border-top-left-radius:5px;
        border-top-right-radius:5px;     
    } 
   
    .divChart1
    {
        float:right;
        border:0px solid black;
        margin:15px 15px 10px 0px;
       /* height:556px;*/
        width:400px;
        border-top-left-radius:5px;
        border-top-right-radius:5px;     
    }  
   

  
</style>

<script type="text/javascript">
      // This function is called when the lake is clicked.
      var posx=0; var posy=0;
      var clicked;

        var divPos = {};
        $(document).mousemove(function(e){
        var $div = $("#mapMz");
       
        posx= e.pageX - $div.offset().left,
        posy= e.pageY - $div.offset().top
        
        });

      
      function getMouse(e){
          posx=0;posy=0;
          var ev=(!e)?window.event:e;//IE:Moz
          if (ev.pageX){//Moz
              posx=ev.pageX+window.pageXOffset;
              posy=ev.pageY+window.pageYOffset;
          }
          else if(ev.clientX){//IE
              posx=ev.clientX+document.body.scrollLeft;
              posy=ev.clientY+document.body.scrollTop;
          }
          else{return false}//old browsers
          document.getElementById('myspan').firstChild.data='X='+posx+' Y='+posy;
      }

    function hideCharts6meses()
    {                       
        grMaputo.setAttribute("style", "display:none;");
        grCaboDelgado.setAttribute("style", "display:none;");
        grTete.setAttribute("style", "display:none;");
        grManica.setAttribute("style", "display:none;");
        grInhambane.setAttribute("style", "display:none;");
        grGaza.setAttribute("style", "display:none;");
        grSofala.setAttribute("style", "display:none;");
        grZambezia.setAttribute("style", "display:none;");
        grNampula.setAttribute("style", "display:none;");
        grNiassa.setAttribute("style", "display:none;");

        grbMaputo.setAttribute("style", "display:none;");
        grbCaboDelgado.setAttribute("style", "display:none;");
        grbTete.setAttribute("style", "display:none;");
        grbManica.setAttribute("style", "display:none;");
        grbInhambane.setAttribute("style", "display:none;");
        grbGaza.setAttribute("style", "display:none;");
        grbSofala.setAttribute("style", "display:none;");
        grbZambezia.setAttribute("style", "display:none;");
        grbNampula.setAttribute("style", "display:none;");
        grbNiassa.setAttribute("style", "display:none;");

        grcMaputo.setAttribute("style", "display:none;");
        grcCaboDelgado.setAttribute("style", "display:none;");
        grcTete.setAttribute("style", "display:none;");
        grcManica.setAttribute("style", "display:none;");
        grcInhambane.setAttribute("style", "display:none;");
        grcGaza.setAttribute("style", "display:none;");
        grcSofala.setAttribute("style", "display:none;");
        grcZambezia.setAttribute("style", "display:none;");
        grcNampula.setAttribute("style", "display:none;");
        grcNiassa.setAttribute("style", "display:none;");

         divCaboDelgado.setAttribute("style", "display:none;");
              divTete.setAttribute("style", "display:none;");
                 divMaputo.setAttribute("style", "display:none;");
                divManica.setAttribute("style", "display:none;");
                divInhambane.setAttribute("style", "display:none;");
                 divGaza.setAttribute("style", "display:none;");
                divSofala.setAttribute("style", "display:none;");
                 divZambezia.setAttribute("style", "display:none;");
                 divNampula.setAttribute("style", "display:none;");
                divNiassa.setAttribute("style", "display:none;");
    }
   
    function MouseClick(var1) {
          
        var territorio = document.getElementById(var1);
        clicked = territorio;
          
        var nome = territorio.getAttribute('id');
        
        territorio.setAttribute("fill", "#00add9");
        territorio.setAttribute("fill-opacity", "0.60");

        var regStatus = document.getElementById('regionInfo');
        regStatus.setAttribute("style", "display:none;");

        var _divChart1 = document.getElementById('Div1');
        _divChart1.setAttribute("style", "display:block;");

        var _DivChartCon6M = document.getElementById('DivChartCon6M');
        _DivChartCon6M.setAttribute("style", "display:block;");

        hideCharts6meses();

        var nomeDivChart = "gr" + nome;
        var divChart = document.getElementById(nomeDivChart);
        divChart.setAttribute("style", "display:block;");
        
        var nomeDivChartb = "grb" + nome;
        var divChartb = document.getElementById(nomeDivChartb);
        divChartb.setAttribute("style", "display:block;");
                
        var nomeDivChartc = "grc" + nome;
        var divChartc = document.getElementById(nomeDivChartc);
        divChartc.setAttribute("style", "display:block;");

        var divnome = "div" + nome ;
        var _divprov = document.getElementById(divnome);
        _divprov.setAttribute("style", "display:block;");
        trataFill();
    }

    function mouseOver(var1) {          
        var territorio = document.getElementById(var1);
        territorio.setAttribute("fill", "#00add9");
        territorio.setAttribute("fill-opacity", "0.20");
    }

    function mouseOut(var1) {
        var territorio = document.getElementById(var1);
        territorio.setAttribute("fill-opacity", "0.01");
        var t = document.getElementById("info");
        t.setAttribute("style", "display:none;");
        trataFill();
    }

    function SetValues(name) {
        //document.onmousemove = getMouse;
        //alert("x:"+ posx+ "y:"+ posy);
        var _info = document.getElementById('info');
        _info.setAttribute("style", " top: " + (posy + 270) + "px; left: " + (posx+25)  + "px; ");
        var _infoNome = document.getElementById('infoNome');

          
        if (name.getAttribute("id") == "CaboDelgado")
            _infoNome.textContent = "Cabo Delgado";
        else{          
            _infoNome.textContent = name.getAttribute('id');
        }

        _infoNome.setAttribute("class", "infoNome");
                   
        var _infoText = document.getElementById('infoDesc');
         
        _infoText.innerHTML = "Existe "+ name.getAttribute('value').toString() + " Noticia(s)";
        _infoText.setAttribute("class", "infoDesc");
    }

    function trataFill() {    
        <%= getTrataFill() %>
    }

    function restartMap(t){
        clicked = null;
        var regStatus = document.getElementById('regionInfo');
        regStatus.setAttribute("style", "display:block;");
        var divChart1 = document.getElementById('Div1');
        divChart1.setAttribute("style", "display:none;");
        var _DivChartCon6M = document.getElementById('DivChartCon6M');
        _DivChartCon6M.setAttribute("style", "display:none;");

        trataFill();
    }



   </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%" > 
        <div style="margin-top:12px">
        
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
              <a href="#"><div ID="lblTitleAdvSearch" style="font-family:Arial; float:left; font-size:12px; color:#FFFFFF;" onclick=' restartMap(this); ' >&nbsp;Geo-MKT | Beta 1.0</div></a>
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px; display: none;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>    
        <div id="mapMz" class="mapMz">
            <div style="text-align:left; color:#00add9; font-size:12px; font-family:Arial;">Moçambique</div>
            <svg  width="362" height="556" viewBox="0 0 181.00 278.00" enable-background="new 0 0 181.00 278.00" xml:space="preserve" xmlns="http://www.w3.org/2000/svg">	          
                <%--<image x="0" y="0" width="362" style="display:none;" height="556" transform="matrix(1.00,0.00,0.00,1.00,0.00,0.00)" xlink:href="mozambiquer_files/image2.png"/>--%>	      
	            <%= getRegions() %>
            </svg>
            <div id="info" class="info" style="display:none;">
	            <div id="infoNome" class="infoNome"></div>
                <div id="infoDesc" class="infoDesc"></div>
            </div> 
        </div> 
         
        <div id="regionInfo" class="regionInfo" style="display:block;">
            <div>
                <asp:Chart ID="chEvolReg" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttEvolReg" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srEvolReg">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caEvolReg">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div>
               <asp:Chart ID="chEvolRegCli" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttEvolRegCli"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srEvolRegCli">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caEvolRegCli">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
        </div>
        
        <div id="Div1" class="divChart1" style="display:none;">
             <div style="text-align:left; color:black; font-size:14px; font-family:Arial; margin-left: 13px; font-weight:bold;">
                 <div id="divCaboDelgado" style="display:none;">Cabo Delgado</div>
                 <div id="divTete" style="display:none;">Tete</div>
                 <div id="divMaputo" style="display:none;">Maputo</div>
                 <div id="divManica" style="display:none;">Manica</div>
                 <div id="divInhambane" style="display:none;">Inhambane</div>
                 <div id="divGaza" style="display:none;">Gaza</div>
                 <div id="divSofala" style="display:none;">Sofala</div>
                 <div id="divZambezia" style="display:none;">Zambezia</div>
                 <div id="divNampula" style="display:none;">Nampula</div>
                 <div id="divNiassa" style="display:none;">Niassa</div>             
             </div> 

            <div id="grCaboDelgado" style="display:none">
                <asp:Chart ID="caCaboDelgado" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttCaboDelgado" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srCaboDelgado">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caCaboDelgado">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grTete" style="display:none">
                <asp:Chart ID="caTete" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttTete">
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srTete">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caTete">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grMaputo" style="display:none">
                <asp:Chart ID="caMaputo" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttMaputo" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srMaputo">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caMaputo">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div> 
            <div id="grManica" style="display:none">
                <asp:Chart ID="caManica" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttManica" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srManica">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caManica">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grInhambane" style="display:none">
                <asp:Chart ID="caInhambane" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttInhambane" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srInhambane">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caInhambane">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grGaza" style="display:none">
                <asp:Chart ID="caGaza" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttGaza" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srGaza">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caGaza">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grSofala" style="display:none">
                <asp:Chart ID="caSofala" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttSofala" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srSofala">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caSofala">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grZambezia" style="display:none">
                <asp:Chart ID="caZambezia" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttZambezia" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srZambezia">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caZambezia">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grNampula" style="display:none">
                <asp:Chart ID="caNampula" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNampula" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNampula">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNampula">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grNiassa" style="display:none">
                <asp:Chart ID="caNiassa" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNiassa" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNiassa">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNiassa">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>

            <div id="grbCaboDelgado" style="display:none">
                <asp:Chart ID="cabCaboDelgado" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttCaboDelgado" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srCaboDelgado">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caCaboDelgado">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbTete" style="display:none">
                <asp:Chart ID="cabTete" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttTete">
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srTete">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caTete">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbMaputo" style="display:none">
                <asp:Chart ID="cabMaputo" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttMaputo" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srMaputo">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caMaputo">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div> 
            <div id="grbManica" style="display:none">
                <asp:Chart ID="cabManica" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttManica" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srManica">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caManica">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbInhambane" style="display:none">
                <asp:Chart ID="cabInhambane" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttInhambane" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srInhambane">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caInhambane">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbGaza" style="display:none">
                <asp:Chart ID="cabGaza" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttGaza" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srGaza">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caGaza">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbSofala" style="display:none">
                <asp:Chart ID="cabSofala" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttSofala" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srSofala">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caSofala">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbZambezia" style="display:none">
                <asp:Chart ID="cabZambezia" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttZambezia" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srZambezia">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caZambezia">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbNampula" style="display:none">
                <asp:Chart ID="cabNampula" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNampula" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNampula">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNampula">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grbNiassa" style="display:none">
                <asp:Chart ID="cabNiassa" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNiassa" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNiassa">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNiassa">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>




         <div>
                
             <%--  <asp:Chart ID="Chart2" runat="server" Width="400px" Height="300px" Palette="Chocolate">
                    <%--<Titles>
                    <asp:Title Name="ttEvolRegCli"></asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srEvolRegCli">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caEvolRegCli">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>--%>
            </div>
        </div>  
        <div id="DivChartCon6M" style="float:left; border:1px solid black; margin:5px; width:789px; height:277px; display:none;">
            <div id="grcCaboDelgado" style="display:none">
                <asp:Chart ID="cacCaboDelgado" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttCaboDelgado" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srCaboDelgado">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caCaboDelgado">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcTete" style="display:none">
                <asp:Chart ID="cacTete" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttTete">
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srTete">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caTete">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcMaputo" style="display:none">
                <asp:Chart ID="cacMaputo" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttMaputo" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srMaputo">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caMaputo">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div> 
            <div id="grcManica" style="display:none">
                <asp:Chart ID="cacManica" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttManica" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srManica">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caManica">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcInhambane" style="display:none">
                <asp:Chart ID="cacInhambane" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttInhambane" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srInhambane">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caInhambane">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcGaza" style="display:none">
                <asp:Chart ID="cacGaza" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttGaza" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srGaza">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caGaza">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcSofala" style="display:none">
                <asp:Chart ID="cacSofala" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttSofala" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srSofala">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caSofala">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcZambezia" style="display:none">
                <asp:Chart ID="cacZambezia" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttZambezia" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srZambezia">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caZambezia">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcNampula" style="display:none">
                <asp:Chart ID="cacNampula" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNampula" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNampula">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNampula">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div>
            <div id="grcNiassa" style="display:none">
                <asp:Chart ID="cacNiassa" runat="server" Width="789px" Height="277px" Palette="Chocolate">
                    <Titles>
                    <asp:Title Name="ttNiassa" >
                    </asp:Title>
                    </Titles>
                    <Series>
                        <asp:Series Name="srNiassa">
                        </asp:Series>
                    </Series>
                    <ChartAreas>
                        <asp:ChartArea Name="caNiassa">
                        </asp:ChartArea>
                    </ChartAreas>
                </asp:Chart>
            </div> 
        </div>
    </div> 
</div>

</asp:Content>
