﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;
using APToolkitNET;

using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;

namespace Insigte
{
    public partial class ListPDF : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //lblDataToday.Text = "Lisboa, " + DateTime.Now.ToString("dd") + " de " + DateTime.Now.ToString("MMMM", CultureInfo.CreateSpecificCulture("pt-PT")) + " de " + DateTime.Now.ToString("yyyy");

            dgNewsToPDFACN.PagerStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[0].ToString());
            dgNewsToPDFACN.HeaderStyle.BackColor = System.Drawing.Color.FromName(Global.InsigteManager[Session.SessionID].inUser.Cores[1].ToString());

            if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend != null)
            {
                if (Global.InsigteManager[Session.SessionID].inUser.EmailShareSend == "1")
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email enviado.";
                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
                else
                {
                    lblDataToday.Visible = true;
                    lblDataToday.Text = "Email não enviado.";

                    Global.InsigteManager[Session.SessionID].inUser.EmailShareSend = null;
                }
            }
            
            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null)
            {
                if (!Page.IsPostBack)
                {
                    getNewsArchive();
                }
            }
            else
            {
                lblWarning.Text = Resources.insigte.language.defColAccoesArtigosGuardados;
                lblWarning.Visible = true;
            }

        }

        protected void dgNewsToPDFACN_PageIndexChanged(object source, DataGridPageChangedEventArgs e)
        {
            dgNewsToPDFACN.CurrentPageIndex = e.NewPageIndex;
            getNewsArchive();
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        public void getNewsArchive()
        {

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];


            if (HttpContext.Current.Request.Cookies["CookieinsigteSys"] != null)
            {
                if (cookie.Values["PDF"].ToString().Length > 0)
                {

                    dgNewsToPDFACN.Visible = true;
                    //HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

                    if (cookie.Values["PDF"].ToString().Length <= 0)
                    {
                        btnBuildPDF.Visible = false;
                        btnClearPDFList.Visible = false;
                        chk_InText.Visible = false;
                    }

                    string[] words = cookie.Values["PDF"].ToString().Split('|');
                    string idsArt = string.Empty;

                    foreach (string value in words)
                    {
                        idsArt += "'" + value + "',";
                    }

                    idsArt = idsArt.Substring(0, idsArt.LastIndexOf(','));

                    String Title = "TITLE";
                    if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
                    {
                        Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                    }

                    string cmdQuery = "SELECT " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath, page_url, tipo ";
                    cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
                    cmdQuery += " where iadvisers.dbo.metadata.ID in (" + idsArt + ") ";
                    cmdQuery += " order by iadvisers.dbo.metadata.date desc, IDMETA desc ";


                    SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");

                    try
                    {
                        conn.Open();

                        SqlCommand cmd = new SqlCommand(cmdQuery);
                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;

                        DataSet tempSet;
                        SqlDataAdapter iadvisers_ACN_NEWS;

                        iadvisers_ACN_NEWS = new SqlDataAdapter(cmd);

                        tempSet = new DataSet("tempSet");
                        iadvisers_ACN_NEWS.Fill(tempSet);

                        dgNewsToPDFACN.DataSource = tempSet;
                        dgNewsToPDFACN.DataBind();
                        dgNewsToPDFACN.Visible = true;

                        cmd.Connection.Close();
                        cmd.Connection.Dispose();
                    }
                    catch (Exception exp)
                    {
                        exp.Message.ToString();
                    }
                }
                else
                {
                    lblWarning.Text = Resources.insigte.language.defColCompilingNews;
                    btnBuildPDF.Visible = false;
                    btnClearPDFList.Visible = false;
                    chk_InText.Visible = false;

                    lblWarning.Visible = true;
                }
            }
            else
            {
                lblWarning.Text = Resources.insigte.language.defColCompilingNews;
                lblWarning.Visible = true;
            }
        }

        protected void btnBuildPDF_Click(object sender, EventArgs e)
        {
            
            btnBuildPDF.Enabled = false;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            string[] words = cookie.Values["PDF"].ToString().Split('|');
            string idsArt = string.Empty;

            foreach (string value in words)
            {
                idsArt += "'" + value + "',";
            }

            idsArt = idsArt.Substring(0, idsArt.LastIndexOf(','));

            //DIFINIR AS FONTES DO DOCUMENTO

            BaseFont bfTimes = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, false);
            BaseFont bfHelve = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1252, false);
            BaseFont bfArial = BaseFont.CreateFont("C:\\Windows\\Fonts\\arial.ttf", BaseFont.CP1252, false);

            Font times = new Font(bfTimes, 12, Font.ITALIC, BaseColor.BLACK);
            Font helve = new Font(bfHelve, 18f, Font.NORMAL, BaseColor.BLACK);
            Font ArialSmall = new Font(bfArial, 8f, Font.NORMAL, BaseColor.BLACK);
            Font helveSmall = new Font(bfHelve, 16f, Font.NORMAL, BaseColor.BLACK);
            Font helveVSmall = new Font(bfHelve, 10f, Font.NORMAL, BaseColor.BLACK);

            Font helveVSmallBlue = new Font(bfHelve, 12f, Font.NORMAL, BaseColor.BLUE);
            helveVSmallBlue.SetColor(0, 173, 217);
            Font helveVVSmallBlue = new Font(bfHelve, 8f, Font.NORMAL, BaseColor.BLUE);
            helveVVSmallBlue.SetColor(0, 173, 217);

            Font helveXSmall = new Font(bfHelve, 6f, Font.NORMAL, BaseColor.BLACK);
            Font helveXSmall8 = new Font(bfHelve, 9f, Font.NORMAL, BaseColor.BLACK);

            //ABRE O INDICE E PREPARA OD DOCUMENTOS

            string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");
            string varSolutionDir = "G:\\files\\";
            string varSolutionDirCostum = "G:\\cfiles\\" + Global.InsigteManager[Session.SessionID].IdClient + "\\";

            string varIndice = "G:\\pdf\\findiceMISC" + timestamp + ".pdf";
            string varCapa = "G:\\pdf\\template_first.pdf";

            var docIndice = new Document();
            string path = Server.MapPath("pdf");
            PdfWriter pdfw = PdfWriter.GetInstance(docIndice, new FileStream(varIndice, FileMode.Create));

            docIndice.Open();
            docIndice.NewPage();

            //INICIALIZA O INDICE

            PlaceText(pdfw.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");

            string idcliente = Global.InsigteManager[Session.SessionID].IdClient;
            if (idcliente == "3003")
            {
                PlaceText(pdfw.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpEA);
            }
            else
            {
                PlaceText(pdfw.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);
            }

            PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 710, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);

            iTextSharp.text.Image myImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_insigte_trans.png");

            if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                myImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
                
            myImage.SetAbsolutePosition(500f, 760f);
            //myImage.ScalePercent(36f); 100%
            myImage.ScalePercent(10f);
            //myImage.ScaleToFit(16, 16);
            docIndice.Add(myImage);

            String Title = "TITLE";
            String texto = "TEXT as text";

            //TEMPORARIO PARA O LUIS TIRAR OS PDFS NA LIGUA ORIGINAL!!!//
            Title = " CASE WHEN idioma = 'pt' then title else title_en end as TITLE ";
            texto = " CASE WHEN idioma = 'pt' then text else text_en end as TEXT ";
            /////////////////////////////////////////////////////////////

            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                texto = "TEXT_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as text";
            }

            string cmdQuery = "SELECT " + texto + ", " + Title + ", EDITOR, DATE, iadvisers.dbo.metadata.ID as IDMETA, filepath, page_url, tipo ";
            cmdQuery += " FROM iadvisers.dbo.metadata with(nolock) ";
            cmdQuery += " INNER JOIN iadvisers.dbo.contents with(nolock) ";
            cmdQuery += " ON iadvisers.dbo.contents.id = iadvisers.dbo.metadata.ID ";
            cmdQuery += " WHERE iadvisers.dbo.metadata.ID in (" + idsArt + ") ";
            cmdQuery += " order by case when iadvisers.dbo.metadata.ID = 524806 then 1 else 2 end, iadvisers.dbo.metadata.date desc, IDMETA desc ";

            DataTable News = Global.InsigteManager[Session.SessionID].getTableQuery(cmdQuery, "getDadosPdf");

            int vSpace = 5;
            int nPage = 0;
            
            //----------------------------------------NOTICIAS -------------------

            string NomeBase;
            if (idcliente == "3003")
            {
                NomeBase = Resources.insigte.language.defPDFDossierImpEA;
            }
            else
            {
                NomeBase = Resources.insigte.language.defPDFDossierImp;
            }
            string nomeOutFile = NomeBase + "_" + timestamp + "_0.pdf";
            string AuxNomeOutFile = "";

            string varOutputFile = "G:\\pdf\\" + nomeOutFile;
            string AuxVarOutputFile = "";

            string varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

            //var docNoticias = new Document();
            //FileStream fsNot = new FileStream(varOutputFile, FileMode.Create);
            //PdfWriter pdfwNews = PdfWriter.GetInstance(docNoticias, fsNot);

            //docNoticias.Open();
            //docNoticias.NewPage();

            //PlaceText(pdfwNews.DirectContent, helve, 30, 750, 335, 790, 14, Element.ALIGN_LEFT, "media intelligence");
            //PlaceText(pdfwNews.DirectContent, helveSmall, 30, 710, 335, 750, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImp);

            //docNoticias.Close();
            //docNoticias.Dispose();
            //fsNot.Close();

            int xCounter = 0;
            int PagNum = 3;

            float NumPagIndice = News.Rows.Count / 42;
            PagNum = PagNum + Convert.ToInt32(NumPagIndice);
            Boolean firstPage = true;

            foreach (DataRow x in News.Rows)
            {
                //ADICIONA AO INDICE
                String Icon = "";

                switch (x["tipo"].ToString())
                {
                    case "Online": Icon = "globe_1_icon_24.png";
                        break;
                    case "Imprensa": Icon = "doc_export_icon_24.png";
                        break;
                    case "Rádio": Icon = "headphones_icon_16.png";
                        break;
                    case "Televisão": Icon = "movie_icon_24.png";
                        break;
                    default: Icon = "doc_export_icon_24.png";
                        break;
                }

                if (nPage > 42)
                {
                    docIndice.NewPage();
                    PlaceText(pdfw.DirectContent, helveVSmall, 30, 680, 335, 790, 14, Element.ALIGN_LEFT, Resources.insigte.language.defPDFDossierImpIndice);
                    vSpace = -60;
                    nPage = 0;
                }

                vSpace += 15;

                String Titulo = x["TITLE"].ToString();
                String Editor = x["EDITOR"].ToString();
                String Data = Convert.ToDateTime(x["DATE"].ToString()).ToShortDateString();
                String Ficheiro = x["filepath"].ToString();

                Ficheiro = Ficheiro.Replace(" ", "%20");

                if (Titulo.Length > 100)
                {

                    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    newsIcon.ScaleAbsolute(6f, 6f);
                    docIndice.Add(newsIcon);

                    //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, 90) + "...");

                    if (Icon == "globe_1_icon_24.png")
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, 90) + "...", x["page_url"].ToString());
                    else
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo.Substring(0, 90) + "...", "http://insigte.com/ficheiros/" + Ficheiro);
                    

                    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    //vSpace += 8;
                    //if (Icon == "globe_1_icon_24.png")
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, x["page_url"].ToString());
                    //else
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, "http://insigte.com/ficheiros/" + Ficheiro);

                }
                else
                {
                    iTextSharp.text.Image newsIcon = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\icons\black\png\" + Icon);
                    newsIcon.SetAbsolutePosition(30f, 695f - vSpace);
                    newsIcon.ScaleAbsolute(6f, 6f);
                    docIndice.Add(newsIcon);

                    //PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo);

                    if (Icon == "globe_1_icon_24.png")
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, x["page_url"].ToString());
                    else
                        PlaceTextLink(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, Titulo, "http://insigte.com/ficheiros/" + Ficheiro);

                    PlaceText(pdfw.DirectContent, helveXSmall, 358, 660 - vSpace, 458, 710 - vSpace, 14, Element.ALIGN_LEFT, Editor);
                    PlaceText(pdfw.DirectContent, helveXSmall, 458, 660 - vSpace, 558, 710 - vSpace, 14, Element.ALIGN_LEFT, Data.ToString());
                    PlaceText(pdfw.DirectContent, helveXSmall, 558, 660 - vSpace, 635, 710 - vSpace, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    //vSpace += 8;
                    //if (Icon == "globe_1_icon_24.png")
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, x["page_url"].ToString());
                    //else
                    //    PlaceText(pdfw.DirectContent, helveXSmall, 40, 660 - vSpace, 358, 710 - vSpace, 14, Element.ALIGN_LEFT, "http://insigte.com/ficheiros/" + Ficheiro);
                }

                nPage++;

                //TRATA NOTICIAS

                xCounter++;
                varOutputTxtFile = "G:\\pdf\\" + "tmp_" + nomeOutFile;

                String Texto = x["text"].ToString();

                if (x["filepath"].ToString().Length > 0 && Global.InsigteManager[Session.SessionID].inUser.CodLanguage == "pt" && !chk_InText.Checked && x["tipo"].ToString() == "Imprensa")
                {
                    String NewsPDF = varSolutionDir + x["filepath"].ToString();

                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(varSolutionDirCostum + x["filepath"].ToString()))
                            NewsPDF = varSolutionDirCostum + x["filepath"].ToString();

                        else
                        {

                            String DesFileRoot = @"G:\cfiles";
                            String DesFilePath = DesFileRoot + @"\" + Global.InsigteManager[Session.SessionID].IdClient;
                            String OriFich = varSolutionDir + x["filepath"].ToString().Replace("/", "\\");
                            String DesFich = varSolutionDirCostum + x["filepath"].ToString().Replace("/", "\\");
                            String ChKDiRecT = x["filepath"].ToString().Replace("/", "\\");

                            string[] Directorias = ChKDiRecT.Split('\\');
                            String Dir = "";
                            foreach (string str in Directorias)
                            {
                                if (!str.EndsWith(".pdf"))
                                {
                                    Dir += "\\" + str;
                                    if (!Directory.Exists(DesFilePath + Dir))
                                        Directory.CreateDirectory(DesFilePath + Dir);
                                }
                            }

                            PdfReader readerPs = new PdfReader("G:\\pdf\\size.pdf");

                            int nPs = readerPs.NumberOfPages;
                            Rectangle pageSize = readerPs.GetPageSizeWithRotation(1);
                            Document document = new Document(pageSize);
                            FileStream fs = new FileStream(DesFich, FileMode.Create);
                            PdfWriter writer = PdfWriter.GetInstance(document, fs);
                            writer.CloseStream = false;
                            document.Open();
                            PdfContentByte cb = writer.DirectContent;
                            PdfImportedPage page;
                            int rotation;
                            PdfReader InsLogo = new PdfReader(OriFich);
                            int n = InsLogo.NumberOfPages;
                            int i = 0;
                            while (i < n)
                            {
                                i++;
                                document.SetPageSize(InsLogo.GetPageSizeWithRotation(i));
                                document.NewPage();
                                page = writer.GetImportedPage(InsLogo, i);
                                rotation = InsLogo.GetPageRotation(i);
                                if (rotation == 90 || rotation == 270)
                                {
                                    cb.AddTemplate(page, 0, -1f, 1f, 0, 0, InsLogo.GetPageSizeWithRotation(i).Height);
                                }
                                else
                                {
                                    cb.AddTemplate(page, 1f, 0, 0, 1f, 0, 0);
                                }


                                iTextSharp.text.Image ClearLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_clear.png");
                                ClearLogoImage.SetAbsolutePosition(518f, 775f);
                                ClearLogoImage.ScalePercent(15f);
                                writer.DirectContent.AddImage(ClearLogoImage);

                                iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient + ".png");
                                ClienteLogoImage.SetAbsolutePosition(526f, 775f);
                                ClienteLogoImage.ScalePercent(10f);
                                writer.DirectContent.AddImage(ClienteLogoImage);

                                //PlaceText(writer.DirectContent, helveXSmall, 528, 15, 605, 55, 14, Element.ALIGN_LEFT, "insigte ™ | 2014");

                                //document.Add(ClienteLogoImage);
                            }
                            document.Close();
                            document.Dispose();
                            fs.Close();
                            fs.Dispose();
                            readerPs.Close();
                            readerPs.Dispose();

                            NewsPDF = DesFich;

                        }
                    } 

                    //String NewsPDF = varSolutionDir + x["filepath"].ToString();

                    string pdfss = varOutputFile + "," + NewsPDF;
                    string[] pdfs = pdfss.Split(',');

                    if (!File.Exists(NewsPDF))
                    {
                        // System.Diagnostics.Debug.WriteLine("Ficheiro: " + NewsPDF);
                        filetranfer(NewsPDF);
                    }

                    AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                    AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                    PagNum += PdfMerge.CountPageNo(NewsPDF);

                    if (firstPage)
                    {
                        File.Copy(NewsPDF, AuxVarOutputFile);
                        firstPage = false;
                    }
                    else
                    {
                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);
                        File.Delete(varOutputFile);
                    }

                    //PdfMerge.MergeFiles(AuxVarOutputFile, pdfs);

                    nomeOutFile = AuxNomeOutFile;
                    varOutputFile = AuxVarOutputFile;

                }
                else
                {

                    //Cria PDF temporario para depois fazer o merge.
                    var docAdd = new Document();
                    FileStream fs = new FileStream(varOutputTxtFile, FileMode.Create);

                    PdfWriter pdfwAdd = PdfWriter.GetInstance(docAdd, fs);
                    docAdd.Open();
                    docAdd.NewPage();

                    //numero pagina
                    PlaceText(pdfwAdd.DirectContent, helveXSmall, 558, 20, 635, 50, 14, Element.ALIGN_LEFT, PagNum.ToString());

                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        iTextSharp.text.Image ClearLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_clear.png");
                        ClearLogoImage.SetAbsolutePosition(518f, 775f);
                        ClearLogoImage.ScalePercent(15f);
                        pdfwAdd.DirectContent.AddImage(ClearLogoImage);


                        iTextSharp.text.Image ClienteLogoImage = iTextSharp.text.Image.GetInstance(@"G:\pdf\Imgs\logo_" + Global.InsigteManager[Session.SessionID].IdClient.ToString() + ".png");
                        ClienteLogoImage.SetAbsolutePosition(526f, 775f);
                        ClienteLogoImage.ScalePercent(10f);
                        pdfwAdd.DirectContent.AddImage(ClienteLogoImage);

                        //PlaceText(pdfwAdd.DirectContent, helveXSmall, 528, 20 - vSpace, 605, 60 - vSpace, 14, Element.ALIGN_LEFT, "insigte ™ | 2014");
                    }
                    else
                    {
                        //Logo Insigte
                        docAdd.Add(myImage);
                    }

                    PdfContentByte cb = pdfwAdd.DirectContent;
                    ColumnText ct = new ColumnText(cb);
                    ct.Alignment = Element.ALIGN_JUSTIFIED;
                    ct.YLine = 1f;
                    ct.SetLeading(0, 1.2f);

                    Paragraph heading = new Paragraph(Titulo, helveVSmallBlue);
                    Paragraph editor = new Paragraph(Editor + " - " + Data, helveVVSmallBlue);
                    heading.Leading = 40f;
                    docAdd.Add(heading);
                    docAdd.Add(editor);

                    Phrase phr = new Phrase(Texto, ArialSmall);
                    phr.Leading = 0;
                    ct.AddText(phr);

                    
                    float gutter = 15f;
                    float colwidth = (docAdd.Right - docAdd.Left - gutter) / 2;
                    //float[] left = { docAdd.Left + 90f, docAdd.Top - 80f, docAdd.Left + 90f, docAdd.Top - 170f, docAdd.Left, docAdd.Top - 170f, docAdd.Left, docAdd.Bottom };

                    float[] left = { docAdd.Left, docAdd.Top - 80f, docAdd.Left, docAdd.Bottom };
                    float[] right = { docAdd.Left + colwidth, docAdd.Top - 80f, docAdd.Left + colwidth, docAdd.Bottom };
                    float[] left2 = { docAdd.Right - colwidth, docAdd.Top - 80f, docAdd.Right - colwidth, docAdd.Bottom };
                    float[] right2 = { docAdd.Right, docAdd.Top - 80f, docAdd.Right, docAdd.Bottom };

                    int status = 0;
                    int i = 0;
                    bool nextPage = false;

                    while (ColumnText.HasMoreText(status))
                    {
                        if (nextPage)
                        {
                            i = 0;
                            docAdd.NewPage();
                            PagNum++;
                            nextPage = false;
                        }

                        if (Texto.Length > 5000)

                            if (i == 0)
                            {
                                //Writing the first column
                                ct.SetColumns(left, right);
                                i++;
                            }
                            else
                            {
                                //write the second column
                                ct.SetColumns(left2, right2);
                                nextPage = true;
                            }

                        else
                        {
                            ct.SetColumns(left, right2);
                            nextPage = true;
                        }

                        //Needs to be here to prevent app from hanging
                        // NUNCA MEXER NESTA MERDA PKP LIXOU O IIS
                        ct.YLine = docAdd.Top - 80f;
                        //Commit the content of the ColumnText to the document
                        //ColumnText.Go() returns NO_MORE_TEXT (1) and/or NO_MORE_COLUMN (2)
                        //In other words, it fills the column until it has either run out of column, or text, or both
                        status = ct.Go();
                        
                    }

                    //-----------------------------------------------------------------------------------------------------------------------------
                    try
                    {


                        if (x["page_url"].ToString().Length > 0)
                            PlaceTextLink(pdfwAdd.DirectContent, helveXSmall8, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, MakeTinyUrl(x["page_url"].ToString()), MakeTinyUrl(x["page_url"].ToString()));

                        if (x["filepath"].ToString().Length > 0)
                            PlaceTextLink(pdfwAdd.DirectContent, helveXSmall8, 40, 20, 635, 50, 14, Element.ALIGN_LEFT, MakeTinyUrl("http://insigte.com/ficheiros/" + x["filepath"].ToString()), MakeTinyUrl("http://insigte.com/ficheiros/" + x["filepath"].ToString()));
                    }
                    catch (Exception er)
                    {
                        er.Message.ToString();

                    }


                    docAdd.Close();
                    docAdd.Dispose();
                    fs.Close();

                    string pdfss = varOutputFile + "," + varOutputTxtFile;
                    string[] pdfs = pdfss.Split(',');

                    AuxNomeOutFile = NomeBase + "_" + timestamp + "_" + xCounter.ToString() + ".pdf";
                    AuxVarOutputFile = "G:\\pdf\\" + AuxNomeOutFile;

                    if (firstPage)
                    {
                        File.Copy(varOutputTxtFile, AuxVarOutputFile);
                        firstPage = false;
                    }
                    else
                    {
                        PdfMerge.MergePdfs(AuxVarOutputFile, pdfs);
                        //PdfMerge.MergeFiles(AuxVarOutputFile, pdfs);

                        File.Delete(varOutputFile);
                        File.Delete(varOutputTxtFile);
                    }
                    nomeOutFile = AuxNomeOutFile;
                    varOutputFile = AuxVarOutputFile;

                    PagNum++;
                }
            }

            docIndice.Close();

            //Finaliza o PDF

            String Capa = "G:\\pdf\\template_first.pdf";
            String PDFFinal = "G:\\pdf\\" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";

            if (File.Exists("G:\\pdf\\template_first_"+ Global.InsigteManager[Session.SessionID].IdClient +".pdf"))
                Capa = "G:\\pdf\\template_first_" + Global.InsigteManager[Session.SessionID].IdClient + ".pdf";


            string pdfsFinals = Capa + "," + varIndice + "," + varOutputFile;
            string[] pdfsFim = pdfsFinals.Split(',');

            PdfMerge.MergePdfs(PDFFinal, pdfsFim);
            //PdfMerge.MergeFiles(PDFFinal, pdfsFim);

            File.Delete(varIndice);
            File.Delete(varOutputFile);

            // Limpa as Cookies

            //string ValCookie = string.Empty;
            //string DelCookie = string.Empty;
            //string PDFCookie = string.Empty;

            //ValCookie = cookie.Values["ID"];
            //PDFCookie = "";
            //DelCookie = cookie.Values["DELNOT"];

            //cookie.Expires = DateTime.Now.AddDays(-1d);
            //Response.Cookies.Add(cookie);

            //HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            //newCookie["Version"] = "JUL2012";
            //newCookie["ID"] = ValCookie;
            //newCookie["PDF"] = "";
            //newCookie["DELNOT"] = DelCookie;
            //newCookie.Expires = DateTime.Now.AddDays(7);
            //Response.Cookies.Add(newCookie);

            try
            {
                
                HttpContext.Current.Response.ContentType = "application/pdf";
                HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=\"" + NomeBase + "_" + timestamp + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf" + "\"");
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.WriteFile(PDFFinal);
                //HttpContext.Current.Response.End();

                addToDocuments(PDFFinal);
                //HttpContext.Current.Response.Redirect("ListPDF.aspx", true);
               
            }
            catch (Exception teste)
            {
                lblDataToday.Visible = true;
                //lblDataToday.Text = "Morangos : " + teste.Message.ToString();
            }

            //Response.Redirect("ListPDF.aspx");
            btnBuildPDF.Enabled = true;

        }

        protected void filetranfer(string ficheironovo)
        {

                // Criar a ligação FTP.
                String IDCLIE = Global.InsigteManager[Session.SessionID].IdClient;
                String localf = "G:\\files";
                if (IDCLIE == ("1047"))
                    {
                        localf = "G:\\cfiles\\1047";
                    }

                String fiche = ficheironovo.ToString().Replace(localf, "ftp://insigtedata.dyndns.org/insigtedata").Replace("\\", "/");

                try
                {
                    FtpWebRequest requestftp = (FtpWebRequest)WebRequest.Create(fiche);
                    requestftp.Method = WebRequestMethods.Ftp.DownloadFile;
                    requestftp.Credentials = new NetworkCredential("insigteftp", "media!INTELL");
                    requestftp.UsePassive = true;
                    requestftp.UseBinary = true;
                    requestftp.KeepAlive = true;

                    //criar o objeto FtpWebResponse
                    FtpWebResponse responseftp = (FtpWebResponse)requestftp.GetResponse();
                    //Criar a Stream para ler o ficheiro
                    Stream responseStream = responseftp.GetResponseStream();

                    byte[] buffer = new byte[2048];

                    //Definir o local onde o ficheiro será criado.
                    Directory.CreateDirectory(Path.GetDirectoryName(ficheironovo));
                    FileStream newFile = new FileStream(ficheironovo, FileMode.Create);
                    //Ler o ficheiro de origem
                    int readCount = responseStream.Read(buffer, 0, buffer.Length);
                    while (readCount > 0)
                    {
                        //Escrever o ficheiro
                        newFile.Write(buffer, 0, readCount);
                        readCount = responseStream.Read(buffer, 0, buffer.Length);
                    }
                    newFile.Close();
                    responseStream.Close();
                    responseftp.Close();
                }
                catch (Exception exp)
                {
                    exp.Message.ToString();
                    //lblDataToday.Text = "Morangos : " + teste.Message.ToString();
                }
        }

        protected string ToTinyURLS(string txt)
        {
            Regex regx = new Regex("http://([\\w+?\\.\\w+])+([a-zA-Z0-9\\~\\!\\@\\#\\$\\%\\^\\&amp;\\*\\(\\)_\\-\\=\\+\\\\\\/\\?\\.\\:\\;\\'\\,]*)?", RegexOptions.IgnoreCase);

            MatchCollection mactches = regx.Matches(txt);

            foreach (Match match in mactches)
            {
                string tURL = MakeTinyUrl(match.Value);
                txt = txt.Replace(match.Value, tURL);
            }

            return txt;
        }

        public static string MakeTinyUrl(string Url)
        {
            try
            {
                if (Url.Length <= 12)
                {
                    return Url;
                }
                if (!Url.ToLower().StartsWith("http") && !Url.ToLower().StartsWith("ftp"))
                {
                    Url = "http://" + Url;
                }
                var request = WebRequest.Create("http://tinyurl.com/api-create.php?url=" + Url);
                var res = request.GetResponse();
                string text;
                using (var reader = new StreamReader(res.GetResponseStream()))
                {
                    text = reader.ReadToEnd();
                }
                return text;
            }
            catch (Exception)
            {
                return Url;
            }
        }

        public void addToDocuments(string PDFFinal)
        {
            #region AddPdf A documentos

            try
            {



                string confirmValue = @Request.Form["confirm_value"];

                if (confirmValue.EndsWith("Yes"))
                {
                    string prompText = @Request.Form["pdfName_value"].Contains(',') ? @Request.Form["pdfName_value"].Split(',').Last() : @Request.Form["pdfName_value"];


                    if (!prompText.Equals(null) && !prompText.EndsWith("null") && !prompText.Equals("null"))
                    {

                        string folderYear = DateTime.Now.ToString("yyyy");
                        string folderMonth = DateTime.Now.ToString("MM");
                        string folderDay = DateTime.Now.ToString("dd");
                        string folderCli = Global.InsigteManager[Session.SessionID].IdClient.ToString();
                        string timestamp = DateTime.Now.ToString("yyyyMMddHHmmss");

                        if (!Directory.Exists(@"G:\Documents\\" + folderCli))
                            Directory.CreateDirectory(@"G:\Documents\\" + folderCli);

                        if (!Directory.Exists(@"G:\Documents\\" + folderCli + "\\" + folderYear))
                            Directory.CreateDirectory(@"G:\Documents\\" + folderCli + "\\" + folderYear);

                        if (!Directory.Exists(@"G:\Documents\\" + folderCli + "\\" + folderYear + "\\" + folderMonth))
                            Directory.CreateDirectory(@"G:\Documents\\" + folderCli + "\\" + folderYear + "\\" + folderMonth);

                        if (!Directory.Exists(@"G:\Documents\\" + folderCli + "\\" + folderYear + "\\" + folderMonth + "\\" + folderDay))
                            Directory.CreateDirectory(@"G:\Documents\\" + folderCli + "\\" + folderYear + "\\" + folderMonth + "\\" + folderDay);


                        //string file1 = @"G:\Documents\\" + folderCli + "\\" + folderYear + "\\" + folderMonth + "\\" + folderDay + "\\" + prompText + "_" + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";

                        //if (File.Exists(file1))
                        //{
                        //    lblDataToday.Visible = true;
                        //    lblDataToday.Text = "Ficheiro já existe!!";
                        //    //this.Page.ClientScript.RegisterStartupScript(this.GetType(), "alert", "alert('File Já Existe')", true);
                        //}
                        //else
                        //{
                        try
                        {
                            lblDataToday.Visible = true;
                            string caminhoFileRoot = @"G:\Documents\"+@folderCli + "\\";  
                            string caminhoFile = @folderYear + "\\" + @folderMonth + "\\" + @folderDay + "\\";
                            string fileName = @prompText + "_" + @timestamp + "_" + @Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ".pdf";
                           
                            string ficheiro = caminhoFileRoot + caminhoFile + fileName;

                            File.Copy(PDFFinal, ficheiro);

                            #region Adicionar ficheiro há tabela documents

                            string query = "";

                            query += " insert into CL10H_CLIENT_DOCUMENTS ";
                            query += " (ID_CLIENT, NOM_DOCUMENT, NOM_DOCUMENT_EN, DES_DOCUMENT, DES_DOCUMENT_EN, COD_DOCUMENT_TYPE, FILE_PATH, DAT_CREATE, ID_USER_CREATE, DAT_MODIFY, ID_USER_MODIFY, IND_ACTIVE, VAL_DOCUMENT_ORDER, ID_CLIENT_USER) ";
                            query += " values ";
                            query += " ( " + @Global.InsigteManager[Session.SessionID].IdClient.ToString();
                            query += " , '" + @fileName + "'";
                            query += " , '" + @fileName + "','" + @fileName + "','" + @fileName + "', 4,'" + @caminhoFile.Replace("\\", "/") + @fileName + "', getdate(),0,getdate(),0,1,0, " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser.ToString() + " ) ";

                            try
                            {
                                DataTable dtIns = Global.InsigteManager[Session.SessionID].getTableQuery(query, "InsertDocuments");
                            }
                            catch (Exception)
                            {
                                
                            }
                            
                            


                            #endregion




                            lblDataToday.Text = "Criado com sucesso";

                            //string message = "Criado com sucesso.";
                            //System.Text.StringBuilder sb = new System.Text.StringBuilder();
                            //sb.Append("<script type = 'text/javascript'>");
                            //sb.Append("window.onload=function(){");
                            //sb.Append("alert('");
                            //sb.Append(message);
                            //sb.Append("')};");
                            //sb.Append("</script>");
                            //ClientScript.RegisterClientScriptBlock(this.GetType(), "alert", sb.ToString());

                        }
                        catch (Exception)
                        {
                            lblDataToday.Text = "Algum problema";

                        }
                        //}
                    }
                }
            }
            catch (Exception er)
            {


            }

            #endregion
        }

        public byte[] FileToByteArray(string fileName)
        {
            byte[] buff = null;
            FileStream fs = new FileStream(fileName,
                                           FileMode.Open,
                                           FileAccess.Read);
            BinaryReader br = new BinaryReader(fs);
            long numBytes = new FileInfo(fileName).Length;
            buff = br.ReadBytes((int)numBytes);
            return buff;
        }

        protected static void PlaceText(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text)
        {
            ColumnText ct = new ColumnText(pdfContentByte);
            ct.SetSimpleColumn(new Phrase(text, font), lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }

        protected static void PlaceTextLink(PdfContentByte pdfContentByte, iTextSharp.text.Font font, float lowerLeftx, float lowerLefty, float upperRightx, float upperRighty, float leading, int alignment, string text, string link)
        {
            ColumnText ct = new ColumnText(pdfContentByte);

            Anchor lnk = new Anchor(text, font);
            lnk.Reference = link;

            Chunk cnk = new Chunk(text, font);
            cnk.SetAnchor(link);


            //var c = new Chunk("Google");
            //c.SetAnchor("https://www.google.com");
            //memberCell.AddElement(c);

            Phrase phr = new Phrase(cnk);
            

            ct.SetSimpleColumn(phr, lowerLeftx, lowerLefty, upperRightx, upperRighty, leading, alignment);
            ct.Go();
        }

        protected string sReturnIconTipoPasta(string url, string tipo, string file)
        {
            //Imprensa
            //Insigte
            //Online
            //Rádio
            //Televisão
            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        string auxfile = @"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\");
                        bool urlexists = INinsigteManager.Utils.URLExists("http://insigte.com/cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file);

                        if (File.Exists(auxfile) || urlexists)
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    return string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    //return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    return string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    return string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"16px\" height=\"16px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }
            return null;
        }

        protected void btnClearPDFList_Click(object sender, EventArgs e)
        {
            string ValCookie = string.Empty;
            string DelCookie = string.Empty;
            string PDFCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            ValCookie = cookie.Values["ID"];
            PDFCookie = "";
            DelCookie = cookie.Values["DELNOT"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = "";
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(7);
            Response.Cookies.Add(newCookie);

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }

        public static void ErrorHandler(string strMethod, object rtnCode)
        {
            System.Diagnostics.Debug.WriteLine(strMethod + " error:  " + rtnCode.ToString());
        }

        protected string sReturnTitle(string idArticle, string sTitle)
        {
            string sLink = string.Empty;

            //select clientid, subjectid from clientarticles where id='164400'

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                string cmdQuery = "select clientid, subjectid from clientarticles with(nolock) where id='" + idArticle + "'";

                SqlCommand cmd = new SqlCommand(cmdQuery);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                SqlDataReader reader = cmd.ExecuteReader();

                bool isACNArticle = false;

                while (reader.Read())
                {
                    if (reader.GetValue(0).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(0,4) && reader.GetValue(1).ToString() == Global.InsigteManager[Session.SessionID].TemaCliente.Substring(4,3))
                    {
                        isACNArticle = true;
                    }
                }

                if (isACNArticle)
                {
                    sLink = "<b>" + sTitle + "</b>";
                }
                else
                {
                    sLink = sTitle;
                }

                cmd.Connection.Close();
                cmd.Connection.Dispose();

                return sLink;


            }
            catch (Exception exp)
            {
                exp.Message.ToString();
                return sTitle;

            }
        }

        protected void bt_AddArquivo_Command(Object sender, CommandEventArgs e)
        {
            addNews(e.CommandArgument.ToString());
            lblDataToday.Text = e.CommandArgument.ToString() + " " + Resources.insigte.language.defAdicionadoSucesso;

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/folderplus32_add.png";
        }

        protected void addNews(string qsIDArtigo)
        {
            string ValCookie = string.Empty;
            string PDFCookie = string.Empty;
            string DelCookie = string.Empty;

            HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

            if (cookie.Values["ID"] != "")
            {
                ValCookie = cookie.Values["ID"] + "|" + qsIDArtigo;
            }
            else
            {
                ValCookie = qsIDArtigo;
            }

            DelCookie = cookie.Values["DELNOT"];
            PDFCookie = cookie.Values["PDF"];

            cookie.Expires = DateTime.Now.AddDays(-1d);
            Response.Cookies.Add(cookie);

            HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
            newCookie["Version"] = "JUL2012";
            newCookie["ID"] = ValCookie;
            newCookie["PDF"] = PDFCookie;
            newCookie["DELNOT"] = DelCookie;
            newCookie.Expires = DateTime.Now.AddDays(360);
            Response.Cookies.Add(newCookie);

            //lblArtigoAdd.Text = "O artigo de ID: " + qsIDArtigo + ", foi adicionado com sucesso.";
        }

        protected string sReturnImgLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16_sel.png";
                        }
                        else
                        {
                            sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
                        }
                    }
                }
            }
            else
            {
                sLink = "Imgs/icons/black/png/clipboard_copy_icon_16.png";
            }
            return sLink;
        }

        protected string sReturnIdLinkDetalhe(string strValue)
        {
            string sLink = string.Empty;

            if (HttpContext.Current.Request.Cookies["CookieinsigteInfo"] != null && strValue != string.Empty)
            {
                if (HttpContext.Current.Request.Cookies["CookieinsigteSys"]["ID"] != null)
                {
                    HttpCookie cookie = Request.Cookies["CookieinsigteSys"];
                    string idVal = cookie.Values["ID"];

                    if (idVal.Contains('|'))
                    {
                        string[] words = idVal.Split('|');
                        if (words.Contains(strValue))
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                    else
                    {
                        if (idVal == strValue)
                        {
                            sLink = string.Empty;
                        }
                        else
                        {
                            sLink = strValue;
                        }
                    }
                }
            }
            else
            {
                sLink = strValue;
            }
            return sLink;
        }

        protected string sReturnImgPDF(string strValue)
        {
            string sLink = string.Empty;
            sLink = "Imgs/icons/black/png/doc_delete_icon_16.png";
            return sLink;
        }

        protected string sReturnIdPDF(string strValue)
        {
            string sLink = string.Empty;
            sLink = strValue;
            return sLink;
        }

        protected void bt_RemPDF_Command(Object sender, CommandEventArgs e)
        {
            RemPDF(e.CommandArgument.ToString());
            //lblDataToday.Text = e.CommandArgument.ToString() + " : Adicionado com sucesso!";

            ImageButton b = sender as ImageButton;
            b.ImageUrl = "Imgs/folderplus32_add.png";

            dgNewsToPDFACN.Items[Convert.ToInt32(e.CommandName)].Visible = false;

        }

        private void RemPDF(string qsIDArtigo)
        {
            try
            {
                string ValCookie = string.Empty;
                string DelCookie = string.Empty;
                string PDFCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];

                //LINQ
                if (cookie.Values["PDF"].ToString().Contains('|'))
                {
                    string[] oldPDFCookie = cookie.Values["PDF"].ToString().Split('|');
                    string numToRemove = qsIDArtigo;
                    oldPDFCookie = oldPDFCookie.Where(val => val != numToRemove).ToArray();

                    foreach (string value in oldPDFCookie)
                    {
                        PDFCookie += value + "|";
                    }
                }

                if (PDFCookie != string.Empty)
                {
                    PDFCookie = PDFCookie.Substring(0, PDFCookie.LastIndexOf('|'));
                }
                else
                {
                    PDFCookie = "";
                }


                DelCookie = cookie.Values["DELNOT"];
                ValCookie = cookie.Values["ID"];


                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

                //lblPDFRem.Text = "O artigo de ID: " + qsIDArtigo + ", foi removido com sucesso.";
            }
            catch (Exception exp)
            {
                //lblPDFRem.Text = exp.Message.ToString() + " - " + exp.StackTrace.ToString();
            }
        }

        protected void OnConfirm(object sender, EventArgs e)
        {
            string confirmValue = Request.Form["confirm_value"];
            if (confirmValue == "Yes")
            { }
        }
    }
}