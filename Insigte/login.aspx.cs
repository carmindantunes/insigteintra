﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace Insigte
{
    public partial class login : BasePage
    {
        string info = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            CultureInfo userInfo;

            try
            {

                userInfo = CultureInfo.CreateSpecificCulture(Request.UserLanguages[0]);

            }

            catch
            {

                userInfo = CultureInfo.CurrentCulture;

            }


            info = userInfo.TwoLetterISOLanguageName;
            if (info.Equals("pt"))
            {
                TrataSiteLangPT();
            }
            else
            {
                TrataSiteLangEN();
            }

            //endereco();

        }

        //protected string endereco()
        //{
        //    Uri intraclient = Request.UrlReferrer;
        //    try
        //    {
        //        string endereco = intraclient.ToString();

        //        //if (endereco.Substring(11, 7) == "insigte")
        //        //{
        //        //    Global.InsigteManager[Session.SessionID].Login("intranet@pwc", "intrapwc162");
        //        //    Response.Redirect("Default.aspx");
        //        //}

        //        if (endereco.Substring(8, 81) == "pwc-spark.com/groups/internal-portal-pwc-portugal/projects/external-research-news")
        //            {
        //            Global.InsigteManager[Session.SessionID].Login("intranet@pwc", "intrapwc162");
        //            Response.Redirect("Default.aspx");
        //            }

        //        return endereco;

        //    }
        //    catch 
        //    {
        //        return null;
        //    }
        //}
        
        private void TrataSiteLangPT()
        {
            FormTitle.InnerText = "Clientes";
            FormUser.InnerText = "utilizador";
            FormPW.InnerText = "código";
            //DescTitle1.InnerText = "media intelligence company";
            DescTitle1.InnerText = "Insigte is now Carma";
            //DescTitle2.InnerText = "collecting small pieces to deliver the Big Picture";
            DescTitle2.InnerText = "Delivering what matters";
            DescTitle3.InnerText = "Saiba mais";
            DescTitleTec.InnerText = "Nova tecnologia, novos INSIGTES";
            TecTitle1.InnerText = "REAL TIME";
            TecText1.InnerText = "Recepção de informação em tempo real de forma a permitir uma atitude proactiva, eficaz e assertiva";
            TecTitle2.InnerText = "EARLY WARNING";
            TecText2.InnerText = "Entender as tendências, antecipar as necessidades e monitorizar o ambiente competitivo";
            TecTitle3.InnerText = "PERFORMANCE";
            TecText3.InnerText = "Partilhar o retorno financeiro das acções de comunicação, as pessoas atingidas e o share mediático";
            TecTitle4.InnerText = "BENCHMARK";
            TecText4.InnerText = "Conhecer a performance mediática de cada concorrente e identificar as acções";
            TecTitle5.InnerText = "BRANDMINING";
            TecText5.InnerText = "Analisar o comportamento da marca, defender a reputação e realinhar o planeamento estratégico";
            TecTitle6.InnerText = "GEO-MKT";
            TecText6.InnerText = "Conhecer a performance geográfica da organização, em cada província e o respectivo benchmark";
            TecTitle7.InnerText = "MULTI-FORMAT";
            TecText7.InnerText = "Plataforma acessível através de qualquer dispositivo, seja o computador, o tablet ou o telefone";
            TecTitle8.InnerText = "SHARE";
            TecText8.InnerText = "Partilhar os factos relevantes por toda a organização de forma a gerar valor";
            DescTitleOut.InnerText = "OUTPUTS que geram INSIGTES";
            OutTitle1.InnerText = "MANAGEMENT";
            OutText1.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br />";
            OutTitle2.InnerText = "MKT & COMM";
            OutText2.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Relatório de media<br />Relatório BI<br />Newsletter & Alertas<br />Portal";
            OutTitle3.InnerText = "NEW BUSINESS";
            OutText3.InnerHtml = "Sumário Executivo<br / >Newsletter<br />Alertas | Prospects<br />";
            OutTitle4.InnerText = "CLIENT CARE";
            OutText4.InnerHtml = "Sumário Executivo<br />Newsletter<br />Alertas | Clientes";
            OutTitle5.InnerText = "RESEARCH";
            OutText5.InnerHtml = "Sumário Executivo<br />Diagnóstico reputacional<br />Newsletter<br />Relatório BI<br />Portal";
            OutTitle6.InnerText = "TECHNOLOGY";
            OutText6.InnerHtml = "Sumário Executivo<br />Newsletter | TECH";
            OutTitle7.InnerText = "PROJECTS";
            OutText7.InnerHtml = "Sumário Executivo<br />Newsletter<br />Alertas | Project";
            OutTitle8.InnerText = "GLOBAL";
            OutText8.InnerHtml = "Sumário Executivo<br />Newsletter diária<br />Alertas<br />Research";
            CountryName1.InnerText = "Angola";
            CountryName2.InnerText = "Cabo Verde";
            CountryName3.InnerText = "Moçambique";
            CountryName4.InnerText = "Portugal";
            CountryName5.InnerText = "África do Sul";
            CountryName6.InnerText = "Namíbia";
        }

        private void TrataSiteLangEN()
        {
            FormTitle.InnerText = "Login";
            FormUser.InnerText = "User";
            FormPW.InnerText = "Password";
            //DescTitle1.InnerText = "media intelligence company";
            DescTitle1.InnerText = "Insigte is now Carma";
            //DescTitle2.InnerText = "collecting small pieces to deliver the Big Picture";
            DescTitle2.InnerText = "Delivering what matters";
            DescTitle3.InnerText = "Know more";
            DescTitleTec.InnerText = "New technology, new INSIGTES";
            TecTitle1.InnerText = "REAL TIME";
            TecText1.InnerText = "Receiving information in real time so as to allow proactive, effective and assertive";
            TecTitle2.InnerText = "EARLY WARNING";
            TecText2.InnerText = "Understand the trends, anticipate needs and monitor the competitive environment";
            TecTitle3.InnerText = "PERFORMANCE";
            TecText3.InnerText = "Share the financial return of publicity measures, the affected people and the media share";
            TecTitle4.InnerText = "BENCHMARK";
            TecText4.InnerText = "Knowing the media performance of each competitor and identify actions";
            TecTitle5.InnerText = "BRANDMINING";
            TecText5.InnerText = "Analyze the behavior of the brand, defend the reputation and realign the strategic planning";
            TecTitle6.InnerText = "GEO-MKT";
            TecText6.InnerText = "Knowing the geographical performance of the organization in each province and its benchmark";
            TecTitle7.InnerText = "MULTI-FORMAT";
            TecText7.InnerText = "Accessible platform through any device, be it computer, tablet or phone";
            TecTitle8.InnerText = "SHARE";
            TecText8.InnerText = "Share the relevant facts throughout the organization in order to generate value";
            DescTitleOut.InnerText = "OUTPUTS generating INSIGTES";
            OutTitle1.InnerText = "MANAGEMENT";
            OutText1.InnerHtml = "Executive Summary<br />Reputational Diagnosis <br />Media report<br />BI Report<br />";
            OutTitle2.InnerText = "MKT & COMM";
            OutText2.InnerHtml = "Executive Summary<br />Reputational Diagnosis<br />Media report<br />BI Report <br />Newsletter & Alerts<br />Portal";
            OutTitle3.InnerText = "NEW BUSINESS";
            OutText3.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Prospects<br />";
            OutTitle4.InnerText = "CLIENT CARE";
            OutText4.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Customers<br />";
            OutTitle5.InnerText = "RESEARCH";
            OutText5.InnerHtml = "Executive Summary<br />reputational Diagnostic<br />Newsletter<br />BI Report<br />Portal";
            OutTitle6.InnerText = "TECHNOLOGY";
            OutText6.InnerHtml = "Executive Summary<br />Newsletter | TECH";
            OutTitle7.InnerText = "PROJECTS";
            OutText7.InnerHtml = "Executive Summary<br />Newsletter<br />Alerts | Project";
            OutTitle8.InnerText = "GLOBAL";
            OutText8.InnerHtml = "Executive Summary<br />daily Newsletter<br />Alerts<br />Research";
            CountryName1.InnerText = "Angola";
            CountryName2.InnerText = "Cape Verde";
            CountryName3.InnerText = "Mozambique";
            CountryName4.InnerText = "Portugal";
            CountryName5.InnerText = "South Africa";
            CountryName6.InnerText = "Namibia";

        }

        public static CultureInfo ResolveCulture()
        {

            string[] languages = HttpContext.Current.Request.UserLanguages;



            if (languages == null || languages.Length == 0)

                return null;



            try
            {

                string language = languages[0].ToLowerInvariant().Trim();

                return CultureInfo.CreateSpecificCulture(language);

            }

            catch (ArgumentException)
            {

                return null;

            }

        }

        protected void timerAO_Tick(object sender, EventArgs e)
        {
            //lb_tmAo.Text = AoTime();
            //lb_tmMz.Text = MzTime();
            //lb_tmPt.Text = PtTime();

            //lb_tmAS.Text = MzTime();
            //lb_tmNB.Text = MzTime();
        }

        protected String PtTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String MzTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(2).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String AoTime()
        {

            var remoteTimeZone = TimeZoneInfo.FindSystemTimeZoneById("GMT Standard Time");
            var remoteTime = TimeZoneInfo.ConvertTime(DateTime.Now, remoteTimeZone);

            string seg = remoteTime.Second.ToString().Length > 1 ? remoteTime.Second.ToString() : "0" + remoteTime.Second.ToString();

            return remoteTime.AddHours(1).ToShortTimeString() + ":" + seg;
            //return "Time in " + remoteTimeZone.Id + " is " + remoteTime.TimeOfDay.ToString();
        }

        protected String getTop5AO()
        {

            String topStr = "";
            String SQLquery = "";
            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1084' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE"+ ( info.Equals("pt") ? "" :"_EN" ) + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1084'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5NB()
        {

            String topStr = "";
            String SQLquery = "";
            //foreach (var pair in Global.InsigteManager)
            //{
            //    topStr += "Session : " + pair.Key.ToString() + "<br>";
            //    topStr += "ID Adr : " + pair.Value.inUser.IPAddress + "<br>";
            //    topStr += "ID Lan Adr : " + pair.Value.inUser.IPLanAddress + "<br>";
            //    topStr += "Nome : " + pair.Value.inUser.Name + "<br>";
            //    topStr += "Cod : " + pair.Value.CodUser + "<br>";
            //}


            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1087' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1087'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5AS()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1099' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1099'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }

            return topStr;
        }

        protected String getTop5MZ()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock) ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles with(nolock) ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1085' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1085'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";
            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5PT()
        {

            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata with(nolock)  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1014' with(nolock) and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1014'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5CV()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1089' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1089'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-3,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5BW()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1101' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1101'  and isnull(page_url,'') != '' ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-30,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";


            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected String getTop5ZB()
        {
            String topStr = "";
            String SQLquery = "";
            //String SQLquery = " SELECT TOP 5 TITLE, EDITOR, DATE, page_url FROM iadvisers.dbo.metadata  ";
            //SQLquery += " INNER JOIN iadvisers.dbo.clientarticles ON iadvisers.dbo.clientarticles.id = iadvisers.dbo.metadata.ID ";
            //SQLquery += " WHERE clientarticles.clientid='1100' and page_url <> '' order by iadvisers.dbo.metadata.date desc ";

            SQLquery += " SELECT TOP 5 m.TITLE" + (info.Equals("pt") ? "" : "_EN") + " as TITLE, m.EDITOR, m.DATE, m.page_url ";
            SQLquery += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLquery += " INNER JOIN iadvisers.dbo.clientarticles  ca with(nolock) ON ca.id = m.ID ";
            SQLquery += " WHERE ca.clientid='1100'  and isnull(page_url,'') != ''  ";
            SQLquery += " and m.cod_dia_insert >= convert(varchar(8),getdate()-7,112) ";
            SQLquery += " and isnull(m.title_en,'') <> '' ";
            SQLquery += " order by m.insert_date desc ";

            DataTable LastNewsMZ = new DataTable();
            LastNewsMZ = Global.InsigteManager[Session.SessionID].getTableQuery(SQLquery, "LastNewsMZ");

            int i = 0;
            String Color = "#FFFFFF";

            foreach (DataRow row in LastNewsMZ.Rows)
            {
                i++;
                if (i % 2 == 0)
                    Color = "#FFFFFF";
                else
                    Color = "#FFFFFF";

                topStr += "<a style='text-decoration:none' target='_blank' href='" + row["page_url"].ToString() + "'>";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 10pt'>";
                topStr += row["TITLE"].ToString() + "</font></a>&nbsp;";
                topStr += "<font face='Calibri' color='" + Color + "' style='font-size: 8pt'>";
                topStr += row["editor"].ToString();
                topStr += "</font><br>";
            }
            return topStr;
        }

        protected void Ib_login_Click(object sender, EventArgs e)
        {
            Global.InsigteManager[Session.SessionID].Login(txt_user.Value, txt_pass.Value);
            Response.Redirect("Default.aspx");
            //Response.Redirect("login.aspx");
        }

    }
}