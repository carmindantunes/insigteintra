﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Fontes.aspx.cs" Inherits="Insigte.Fontes" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">

<script type="text/javascript">

    function lbEditores_DoubleClick(value, vleID) {

        //alert(vleID);

        vleID = vleID.replace("LbEditorsAO", "ta_editores_codes");
        vleID = vleID.replace("LbEditorsMZ", "ta_editores_codes");
        vleID = vleID.replace("LbEditorsPT", "ta_editores_codes");
        vleID = vleID.replace("LbEditorsOU", "ta_editores_codes");
        vleID = vleID.replace("LbEditors", "ta_editores_codes");

        //alert(vleID);

        if ($('textarea[id^="' + vleID + '"]').tagExist(value)) {
            alert("Editor '" + value + "' já Existe");
        } else {
            if (value == "<%= getResource("geralTagTodos") %>") {
                $('textarea[id^="' + vleID + '"]').importTags('');
                $('textarea[id^="' + vleID + '"]').addTag(value);
            }
            else {
                if ($('textarea[id^="' + vleID + '"]').tagExist("<%= getResource("geralTagTodos") %>")) {
                    $('textarea[id^="' + vleID + '"]').importTags('');
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                } else {
                    $('textarea[id^="' + vleID + '"]').addTag(value);
                }
            }
        }
    };

    

    $(function () {
        $('div[id="tabs"]').tabs()
    });

    $(function () {
        $('.show_on_hover').hide();
        $('.sector').hover(function () {
            $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
        },
        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
    });

    $(function () {
        var name = $("#MainContent_name"),
		email = $("#email"),
		password = $("#password"),

		tips = $(".validateTips");
        function updateTips(t) {
            tips
			.text(t)
			.addClass("ui-state-highlight");
            setTimeout(function () {
                tips.removeClass("ui-state-highlight", 1500);
            }, 500);
        }


        $('textarea[id^="MainContent_repGroups_ta_editores_codes"]').tagsInput({
            width: 'auto',
            height: '212px',
            interactive: false
        });

        $("#dialog-form").dialog({
            autoOpen: false,
            height: 200,
            width: 350,
            modal: true,
            beforeClose: function (event, ui) {
                $("#MainContent_txt_aux_novo").val($("#MainContent_name").val());
            }
        });

        $("#create-user").button().click(function () {
            $("#dialog-form").dialog("open");
        });

        $("#criar-novo").click(function () {
            $("#dialog-form").dialog("open");
        });


    });
</script>
<style type="text/css">
        
    a.lbk
    {
        color: #000;
        text-decoration: none;
        font-weight:bold;
        font-size: 12px;
        font-family:Arial;
    }
    
    a.lbk:hover
    {
        color: #000;
        text-decoration: underline;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    #Data
    {
        float:left;
        width:100%;
    }
    #groups
    {
        float:left;
        width:45%;
         margin-right:10px;
    }
    
    #config
    {
        float:left;
        margin-left:10px;
        margin-right:10px;
        width:45%;
        
    }
    
    #dialog-form
    {
        height:auto;
        color: #000;
        text-decoration: none;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;
    }
    
    #dialog-form p.newDesc { color: #000; }
        
    .actionBtn
    {
        width: 24px;
        height: 24px;
        padding: 1px 2px 3px;
        border: none;
        background: transparent url(Imgs/controls.png);
    }    
    .actionBtn-snone {
      background-position: -144px 0;
    }

    .actionBtn-snone:hover {
      background-position: -168px 0;
    }

    .actionBtn-snone:active {
      background-position: -192px 0;
    }
    
    .actionBtn-sall {
      background-position: -72px 0;
    }

    .actionBtn-sall:hover {
      background-position: -96px 0;
    }

    .actionBtn-sall:active {
      background-position: -120px 0;
    }
    
    fieldset { padding:0; border:0; margin-top:25px; }
    
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 80px;
        min-width:300px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin: 0px 0px 5px 0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 0px auto;
        background-color: #323232;
        color: #FFFFFF;
    }
    
    .ui-tabs-nav a 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;
    }
        
    .ui-tabs-nav a:hover 
    {
        text-decoration: none;
        font-family: Arial;
        font-size: 12px;        
    }
    
</style>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    
    
    <asp:Label ID="lbInfo" Font-Names="Arial" Font-Size="12px" ForeColor="#000000"  runat="server" Visible="false" ></asp:Label>

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">

    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <div id="TopInfo">
                <asp:DropDownList ID="ItemSell"  Font-Names="Arial" Font-Size="12px" ForeColor="#000000" OnSelectedIndexChanged="getSelected" AutoPostBack="true" runat="server">
                    <asp:ListItem Value="Temas" Text="<%$Resources:insigte.language,fgTemas%>"></asp:ListItem>
                    <asp:ListItem Value="Newsletter" Text="<%$Resources:insigte.language,fgNewsletter%>"></asp:ListItem>
                    <asp:ListItem Value="Pesquisa" Text="<%$Resources:insigte.language,fgPesquisa%>"></asp:ListItem>
                    <asp:ListItem Value="Homepage" Text="<%$Resources:insigte.language,fgHomepage%>"></asp:ListItem>
                </asp:DropDownList>
                <asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#000000" runat="server" ID="lblDataToday" Visible="false" />
            </div>
        </div>
    </div>
    <div id="groups" style="float:left; width:100%;">
        <asp:Repeater id="repGroups" runat="server" OnItemDataBound='repGroups_OnItemDataBound'>
            <ItemTemplate>
                <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
                    <div id="Area" style="float:left;width:100%;">
                        <div id="sectionTitle" style="float:left;width:100%;" class="form_search headbg ui-corner-top">
                            <div id="titlel" style="float:left;width:100%;">
                                &nbsp;<asp:Label Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" ID="Label1" runat="server" text='<%# DataBinder.Eval(Container.DataItem, "ItemTitle") %>' ></asp:Label>
                            </div>
                        </div>
                        <div id="sectionBody" style="float:left; margin:1px; width:96%; margin:10px 10px 10px 10px;">
                            <div id="ListEditors" style="float:left; width:50%;" >
                                <div id="tabs">
                                    <ul>
                                        <%--<li><a href="#tabs-1">Todos</a></li>
                                        <li><a href="#tabs-2">Angola</a></li>
                                        <li><a href="#tabs-3">Moçambique</a></li>
                                        <li><a href="#tabs-4">Portugal</a></li>
                                        <li><a href="#tabs-5">Outros</a></li>--%>
                                        <li><a href="#tabs-1"><%= getResource("advSMainEditoresT1")%></a></li>
                                        <li><a href="#tabs-2"><%= getResource("advSMainEditoresT2")%></a></li>
                                        <li><a href="#tabs-3"><%= getResource("advSMainEditoresT3")%></a></li>
                                        <li><a href="#tabs-4"><%= getResource("advSMainEditoresT5")%></a></li>
                                        <li><a href="#tabs-5"><%= getResource("advSMainEditoresT4")%></a></li>
                                    </ul>
                                    <div id="tabs-1">
                                        <p>
                                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="130px" ID="LbEditors" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>'></asp:ListBox>
                                        </p>
                                    </div>
                                    <div id="tabs-2">
                                        <p>
                                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="130px" ID="LbEditorsAO" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>'></asp:ListBox>
                                        </p>
                                    </div>
                                    <div id="tabs-3">
                                        <p>
                                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="130px" ID="LbEditorsMZ" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>'></asp:ListBox>
                                        </p>
                                    </div>
                                    <div id="tabs-4">
                                        <p>
                                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="130px" ID="LbEditorsPT" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>'></asp:ListBox>
                                        </p>
                                    </div>
                                    <div id="tabs-5">
                                        <p>
                                            <asp:ListBox Font-Names="Arial" Font-Size="12px" runat="server" Width="100%" Height="130px" ID="LbEditorsOU" ondblclick="lbEditores_DoubleClick(this.value,this.id)" SelectionMode="Multiple" CommandName='<%# DataBinder.Eval(Container.DataItem,"ItemID").ToString() %>'></asp:ListBox>
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div id="SelectedEditors" style="float:right; width:48%; margin-left:10px;">
                                <textarea cols="50" rows="4" runat="server" ID="ta_editores_codes" style="width: 354px; height:100px;"></textarea>
                            </div>
                        </div>
                        <div id="actionSave" style="float:left; margin:1px; width:90%; margin:10px 10px 10px 10px;"><asp:LinkButton ID="LBSave" CssClass="btn-pdfs" runat="server" OnClick="Save_Click"><%= getResource("fgGuardar") %></asp:LinkButton></div>
                    </div>
                </div>
            </ItemTemplate>
        </asp:Repeater>
    </div>
</div>
</asp:Content>
