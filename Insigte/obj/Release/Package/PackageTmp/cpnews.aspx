﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="cpnews.aspx.cs" Inherits="Insigte.cpnews" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>

<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <asp:Label runat="server" ID="Lb_Tema" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Área" />
            <asp:DropDownList ID="ddTemas" runat="server" AutoPostBack="True" onselectedindexchanged="ddTemas_SelectedIndexChanged"></asp:DropDownList>
            <asp:Label runat="server" ID="LbSubTema" Font-Names="Arial" Font-Size="12px" Visible="false" Text="SubÁrea" />
            <asp:DropDownList ID="ddSubTema" runat="server" AutoPostBack="True" onselectedindexchanged="ddSubTemas_SelectedIndexChanged" Visible="false"></asp:DropDownList>
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgNewsACN" AutoGenerateColumns="false" AllowPaging="True" 
        GridLines="None" BorderWidth="0px" BorderStyle="None" CellPadding="0" CellSpacing="0"
        Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="40" AlternatingItemStyle-BackColor="#EEEFF0"
        PagerStyle-BackColor="#8D8D8D" PagerStyle-Font-Size="12px" OnPageIndexChanged="dgNewsACN_PageIndexChanged"
        PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle"
        PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <Columns>
            <asp:BoundColumn DataField="DES_CONCURSO_PUBLICO" HeaderText="&nbsp;Objecto" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" ItemStyle-Width="400px" />
            <asp:BoundColumn DataField="DES_LOCAL" HeaderText="Local" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="Editor" HeaderText="Fonte" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="nom_entidade" HeaderText="Entidade" HeaderStyle-HorizontalAlign="Left" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundColumn DataField="DAT_ANUNCIO" HeaderText="Data" DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center" ItemStyle-Width="60px" />
            <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="center" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="50px">
                <ItemTemplate>
                    <asp:Table ID="Table1" runat="server">
                        <asp:TableRow ID="TableRow1" runat="server" >
                            <asp:TableCell ID="TableCell3" runat="server">
                                <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "COD_URL").ToString(), DataBinder.Eval(Container.DataItem, "COD_FILEPATH").ToString())%>
                            </asp:TableCell>
                        </asp:TableRow>
                    </asp:Table>
                </ItemTemplate>
            </asp:TemplateColumn>
        </Columns>
        
    </asp:DataGrid>
    </div>
    <div style="float: left; padding: 5px,0px,5px,0px; width:98%">
        <div style="float:left; width:100%"> 
            <div style="float:left;width:50%; vertical-align:middle">
                <asp:ImageButton runat="server" ID="btnExportarXML" Text="<%$Resources:insigte.language,defExcel%>" OnClick="btnExportXML_Click" Enabled="true" Width="16px" Height="16px" ImageUrl="~/Imgs/pesquisa_button.png" />
                <asp:LinkButton runat="server" ID="lbExportarXML" OnClick="btnExportXML_Click" Text="<%$Resources:insigte.language,defExcel%>" Font-Names="Arial" Font-Size="12px" ForeColor="#000000" Font-Underline="false" />
            </div>
<%--            <div style="float:left; width:50%;vertical-align:middle">
                <div style="float:right;">
                    <asp:LinkButton runat="server" Visible="true" ID="btnDossier" CssClass="btn-pdfs" OnClick="btnDossier_Click" Text="<%$Resources:insigte.language,defColAccoesDossier%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                    &nbsp;
                    <asp:LinkButton runat="server" Visible="true" ID="btnSelAll" CssClass="btn-pdfs" OnClick="btnSelAll_Click" Text="<%$Resources:insigte.language,defColAccoesDossierSell%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                </div>
            </div>--%>
        </div>
    </div>
</div>
</asp:Content>
