﻿<%@ Control Language="C#" AutoEventWireup="true" CodeBehind="NewsRelated.ascx.cs"
    Inherits="Insigte.UserControls.NewsRelated" %>
<div style="margin-top: 10px; float: left; width: 100%;">
    <div style="<%= getStyle() %>">
        <asp:UpdatePanel ID="upcont1" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:ListView ID="lst_NewsRelated" runat="server">
                    <LayoutTemplate>
                        <div id="divglob" style="float: left; width: 100%;">
                            <asp:PlaceHolder ID="itemPlaceHolder" runat="server" />
                        </div>
                    </LayoutTemplate>
                    <ItemTemplate>
                        <div id="divItem" class='<%# getcolor1( Convert.ToString(Eval("id"))) %>' onmouseover="this.style.backgroundColor='<%= getcolor() %>';"
                            onmouseout="this.style.backgroundColor='<%# getcolor2( Convert.ToString(Eval("id"))) %>';">
                            <div style="margin: 10px;">
                                <a href='Article.aspx?ID=<%#Eval("id") %><%= newsNRP() %>' target="_self" title='<%#Eval("text") %>'>
                                    <span id="title">
                                        <%#Eval("title") %></span>
                                    <br />
                                    <span id="Span1" style="font-size: 12px; font-weight: normal;"><span>
                                        <%#Eval("editor") %></span> <span>| </span><span>
                                            <%#Eval("date") %></span> <span>| </span><span>
                                                <%#TransTipo(Eval("tipo").ToString()) %></span> </span></a>
                            </div>
                        </div>
                    </ItemTemplate>
                </asp:ListView>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>
</div>
