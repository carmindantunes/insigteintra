﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscribeAl.aspx.cs" Inherits="Insigte.SubscribeAl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" />
    <br />
    <br />
    <asp:Table runat="server" ID="tblAlertas" Visible="true">
        <asp:TableRow runat="server" ID="trAlertas1">
            <asp:TableCell runat="server" ID="tcAlertas1" HorizontalAlign="Right" VerticalAlign="Middle">
                <asp:Image runat="server" ID="imgArrowAlertas" ImageUrl="~/Images/red-arrow.gif" />
            </asp:TableCell>
            <asp:TableCell runat="server" ID="tcAlertas2">
                <asp:Label runat="server" ID="lblTitleAlertas" Font-Bold="true" Font-Names="Arial"
                    Font-Size="12px" ForeColor="#000000" Text="Caso deseje receber em real-time as notícias publicadas sobre a BCG, introduza o seu email:" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow runat="server" ID="trAlertas2">
            <asp:TableCell runat="server" ID="tcAlertas3">
            </asp:TableCell>
            <asp:TableCell runat="server" ID="tcAlertas4">
                <asp:TextBox runat="server" ID="tbAlertasEmail" Font-Names="Arial" Font-Size="12px" Width="300px" Text="utilizador@bcg.com" Font-Italic="true" VerticalAlign="Middle" Enabled="true" />
                <asp:RegularExpressionValidator ID="expEmail" runat="server" ControlToValidate="tbAlertasEmail" Font-Names="Arial" Font-Size="12px" ForeColor="Red" ErrorMessage="Endereço de email inválido" ValidationExpression="^[a-zA-Z0-9._%+-]+(@bcg\.com)$"></asp:RegularExpressionValidator>
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell></asp:TableCell>
            <asp:TableCell>
                <asp:Button runat="server" ID="btnInsertEmail" Text="Enviar" CssClass="btn-pdfs" OnClick="btnInsertEmail_Click" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:Label runat="server" ID="lblEmailList" Font-Names="Arial" Font-Size="12px"/>
</asp:Content>
