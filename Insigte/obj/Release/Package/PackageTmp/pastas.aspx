﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="pastas.aspx.cs" Inherits="Insigte.pastas" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" /> 
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <asp:Label runat="server" ID="Label1" Font-Names="Arial" Font-Size="12px" Visible="true" Text="<%$Resources:insigte.language,ucPastasTitulo%>" />
            <asp:DropDownList ID="tvPastas" runat="server" AutoPostBack="True" onselectedindexchanged="tvPastas_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgPasta" AutoGenerateColumns="false" 
        AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="0" 
        BorderWidth="0px" Visible="false"
        BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" 
        PageSize="100" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
        PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" 
        PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" 
        PagerStyle-Mode="NumericPages" 
        onpageindexchanged="dgPasta_PageIndexChanged">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
            <Columns>
                <asp:TemplateColumn>
                    <HeaderTemplate>
                        <div>
                            <asp:CheckBox ID="chkSelAll" runat="server" OnCheckedChanged="chkSelAll_changed" AutoPostBack="true" />
                        </div>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:CheckBox Checked='false' Font-Names="Arial" NewsId='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString() %>' Font-Size="12px" runat="server" ToolTip="" ID="chka_AddPDF" Visible="true" /> 
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>
                        <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';"><%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), DataBinder.Eval(Container.DataItem, "TITLE").ToString())%></a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="EDITOR" HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Justify" />
                <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="100px" />
                <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                    
                    <ItemTemplate>
                        <asp:Table ID="Table1" runat="server">
                            <asp:TableRow ID="TableRow1" runat="server">
                                <asp:TableCell ID="TableCell1" runat="server">
                                    <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" target="_self" style="color:#000000; text-decoration:none;">
                                        <img src="Imgs/icons/black/png/doc_lines_stright_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesTexto") %>" title="<%= getResource("defColAccoesTexto") %>" style="border-width:0px;"/>
                                     </a>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell2" runat="server">
                                    <%--<a href="javascript:window.open('Email.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "IDMETA")%>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="border-width:0px;text-decoration:none;">
                                        <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="Partilhar artigo" title="Partilhar artigo" style="border-width:0px;"/>
                                    </a>--%>
                                    <a id="share-news" onclick="shareTheNews(<%# "'" + DataBinder.Eval(Container.DataItem, "IDMETA").ToString() +  "','" +  DataBinder.Eval(Container.DataItem, "TITLE").ToString().Replace("'"," ").Replace("\""," ") + "'" %>)" title='<%# DataBinder.Eval(Container.DataItem, "TITLE").ToString() %>' style="color:#000000; text-decoration:none;" href="#">
                                        <img src="Imgs/icons/black/png/share_icon_16.png" width="16px" height="16px" alt="<%= getResource("defColAccoesShare") %>" title="<%= getResource("defColAccoesShare") %>" style="border-width:0px;"/>
                                    </a>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell3" runat="server">
                                    <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell4" runat="server" Visible="false">
                                    <%# sReturnLinkDetalhe(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell5" runat="server">
                                    <asp:ImageButton ID="bt_AddPDF" runat="server" 
                                        OnCommand="bt_AddPDF_Command" 
                                        CommandArgument='<%# sReturnIdPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString())%>'
                                        CausesValidation="True" 
                                        ImageUrl='<%# sReturnImgPDF(DataBinder.Eval(Container.DataItem, "IDMETA").ToString()) %>' width='16px' height='16px' alt='<%# getResource("defColAccoesDossier") %>' title='<%# getResource("defColAccoesDossier") %>' style='border-width:0px;' >
                                    </asp:ImageButton >
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell6" runat="server">
                                    <asp:ImageButton ID="bt_delete" runat="server" alt='Remover Artigo' CausesValidation="True"
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString()%>'
                                        Height='16px' ImageUrl='Imgs/icons/black/png/doc_delete_icon_16.png' 
                                        OnClientClick='return confirm("Tem a certeza que deseja apagar este artigo?");'
                                        OnCommand="bt_Delete_Command" Style='border-width: 0px;' title='Remover Artigo'
                                        Width='16px' />
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell7" runat="server">
                                    <a href="javascript:window.open('Notas.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "IDMETA")%>&PID=<%# getPastaID() %>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="border-width:0px;text-decoration:none;"><img src="Imgs/icons/black/png/message_attention_icon_16.png" width="16px" height="16px" alt="Notas" title="Notas" style="border-width:0px;"/></a>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell8" runat="server">
                                    <a href="javascript:window.open('MoverArtigo.aspx?ID=<%# DataBinder.Eval(Container.DataItem, "IDMETA")%>&PID=<%# getPastaID() %>','CustomPopUp', 'width=400, height=400, menubar=no, resizeble=no, location=no'); void(0)" style="border-width:0px;text-decoration:none;"><img src="Imgs/icons/black/png/folder_arrow_icon_16.png" width="16px" height="16px" alt="Mover Artigo" title="Mover Artigo" style="border-width:0px;"/></a>
                                </asp:TableCell>
                                
                            </asp:TableRow>
                        </asp:Table>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
    </asp:DataGrid>
    </div>

    <div style="float:right; padding-right:7px;">
        <asp:Table ID="Table2" runat="server">
            <asp:TableRow ID="TableRow2" runat="server">
                <asp:TableCell ID="TableCell5" runat="server">
                    <asp:CheckBox ID="chk_InText" CssClass="btn-pdfs" Height="17px" runat="server" Text ="<%$Resources:insigte.language,defOnlyText%>" Checked="false" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell9" runat="server">
                    <asp:Button runat="server" ID="btnBuildPDF" Text="<%$Resources:insigte.language,pastasCompilarPDF%>" CssClass="btn-pdfs" OnClick="btnBuildPDF_Click" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell10" runat="server">
                   <%--<video controls="controls" preload="none">
                      <source src="http://insigte.com/ficheiros/2013/05/10/TVI_09052013.mp4" type="video/mp4">
                    </video>--%>
                </asp:TableCell>
               <%-- <asp:TableCell ID="TableCell11" runat="server">
                    &nbsp;
                    <asp:LinkButton runat="server" Visible="true" ID="btnSelAll" CssClass="btn-pdfs" OnClick="btnSelAll_Click" Text="<%$Resources:insigte.language,defColAccoesDossierSell%>" Font-Names="Arial" Font-Size="12px" ForeColor="#ffffff" Font-Underline="false" />
                </asp:TableCell>--%>
               
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
</asp:Content>
