﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="SubscribeCP.aspx.cs" Inherits="Insigte.SubscribeCP" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    }    
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <%--<asp:Label runat="server" ID="Label1" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Temas: " />
            <asp:DropDownList ID="tvTemas" runat="server" AutoPostBack="True" onselectedindexchanged="tvTemas_SelectedIndexChanged"></asp:DropDownList>--%>
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" /> 
        </div>
    </div>
    <div class="form_caja">
    <asp:DataGrid runat="server" ID="dgPasta" AutoGenerateColumns="false" 
        AllowPaging="True" GridLines="None" CellPadding="0" CellSpacing="0" 
        BorderWidth="0px" Visible="false" BorderStyle="None" Width="800px" Font-Names="Arial" Font-Size="11px" 
        PageSize="100" AlternatingItemStyle-BackColor="#EEEFF0" PagerStyle-BackColor="#686C6E" 
        PagerStyle-Font-Size="12px" PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle" PagerStyle-HorizontalAlign="Center" 
        PagerStyle-Mode="NumericPages" onpageindexchanged="dgPasta_PageIndexChanged">
        <ItemStyle BackColor="White" Height="24px" />
        <AlternatingItemStyle  BackColor="#EEEFF0" />
        <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
        <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
            <Columns>
                <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>&nbsp;
                        <a href="<%# string.Format("Article.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "IDMETA")) %>" title="<%# string.Format("{0}", DataBinder.Eval(Container.DataItem, "text")) %>" target="_self" style="color:#000000; text-decoration:none;" onmouseout="this.style.textDecoration='none';" onmouseover="this.style.textDecoration='underline';"><%# sReturnTitle(DataBinder.Eval(Container.DataItem, "IDMETA").ToString(), DataBinder.Eval(Container.DataItem, "TITLE").ToString())%></a>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="EDITOR" HeaderText="<%$Resources:insigte.language,defColFonte%>" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Justify" />
                <asp:BoundColumn DataField="DATE" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:d}" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"  ItemStyle-Width="100px" />
                <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center" >
                    <ItemTemplate>
                        <asp:Table ID="Table1" runat="server">
                            <asp:TableRow ID="TableRow1" runat="server">
                                <asp:TableCell ID="TableCell3" runat="server">
                                    <%# sReturnIconTipoPasta(DataBinder.Eval(Container.DataItem, "page_url").ToString(), DataBinder.Eval(Container.DataItem, "tipo").ToString(), DataBinder.Eval(Container.DataItem, "filepath").ToString())%>
                                </asp:TableCell>
                                <asp:TableCell ID="TableCell6" runat="server">
                                    <asp:ImageButton ID="bt_delete" runat="server" alt='Remover Artigo' CausesValidation="True"
                                        CommandArgument='<%# DataBinder.Eval(Container.DataItem, "IDMETA").ToString()%>'
                                        Height='16px' ImageUrl='Imgs/icons/black/png/doc_delete_icon_16.png' 
                                        OnClientClick='return confirm("Tem a certeza que deseja apagar este artigo?");'
                                        OnCommand="bt_Delete_Command" Style='border-width: 0px;' title='Remover Artigo'
                                        Width='16px' />
                                </asp:TableCell>
                            </asp:TableRow>
                        </asp:Table>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
    </asp:DataGrid>
    </div>

    <div style="float:right; padding-right:7px;">
        <asp:Table ID="Table2" runat="server">
            <asp:TableRow ID="TableRow2" runat="server">
                <asp:TableCell ID="TableCell9" runat="server">
                    <asp:Button runat="server" ID="btnSendEmail" Text="Enviar Email" CssClass="btn-pdfs" OnClick="btnSendEmail_Click" />
                </asp:TableCell>
                <asp:TableCell ID="TableCell10" runat="server">
<%--                    <video controls="controls" preload="none">
                      <source src="http://insigte.com/ficheiros/2013/05/10/TVI_09052013.mp4" type="video/mp4">
                    </video>--%>
                </asp:TableCell>
            </asp:TableRow>
        </asp:Table>
    </div>
</div>
</asp:Content>