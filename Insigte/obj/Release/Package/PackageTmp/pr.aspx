﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="pr.aspx.cs" Inherits="Insigte.pr" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 300px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    
    .form_search
    {
        float:left;
        width: 100%;
        height:10px;
        padding-top: 4px;
        padding-bottom: 10px;
        margin: 0 auto 20px auto;
        background-color: #323232;
        color: #FFFFFF;
    } 
       
    .doc_link
    {
        float:left;
        width:100%;
        height:62px;
        color: #000000;
        line-height:1.5em;
    }
    
    .doc_link:hover
    {
        background-color: #f0f0f0;
    }
    
    .doc_link a
    {
        color: #000000;
    } 
</style>
<script type="text/javascript">
    $(function () {
        $('.show_on_hover').hide();
        $('.doc_link').hover(function () {
            $(this).find('.show_on_hover').stop(true, true).fadeIn(400);
        },
        function () {
            $(this).find('.show_on_hover').stop(true, true).fadeOut(400);
        }
        );
    });
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false" />
        </div>
    </div>
    <div id="section" class="sector ui-corner-top" style="float:left;width:100%;">
        <div id="titulo" class="form_search headbg ui-corner-top" style="float:left;width:100%;">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" PR" />
            </div>
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px;">
                    <img alt="Ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                </div>
            </div>
        </div>
        <div id="search_pr" style="float:left;width:100%;">
            <div style=" margin: 5px 10px 5px 10px;">
                <asp:Label Text="<%$Resources:insigte.language,advSMainSourceTitulo%>" runat="server" ID="lbl1" /><asp:DropDownList ID="DDfontes" runat="server"></asp:DropDownList> 
                <asp:Label Text="<%$Resources:insigte.language,advSMainPaisTitulo%>" runat="server" ID="lbl2" /><asp:DropDownList ID="DDPais" runat="server">
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem1%>" Value="-1" Selected="true"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem2%>" Value="Angola"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem3%>" Value="Moçambique"></asp:ListItem>
                        <asp:ListItem Text="<%$Resources:insigte.language,advSMainPaisItem4%>" Value="Portugal"></asp:ListItem>
                      </asp:DropDownList>
                <asp:Label Text="<%$Resources:insigte.language,advSMainSearchTitulo%>" runat="server" ID="lbl3" /> <input id="txt_pesquisa" runat="server" size="30" />
                <asp:LinkButton ID="LbPesquisa" CssClass="btn-pdfs" runat="server" OnClick="Pesquisar_Click"><%= getResource("advSMainBtnSearch")%></asp:LinkButton>
            </div>
        </div>
        <div id="pr" style="float:left;width:100%; margin-top:10px;">
            <div style="float:left;width:50%;">
                <div style=" margin: 5px 10px 5px 10px;"><asp:Label Text="<%$Resources:insigte.language,prRstAutores%>" runat="server" ID="lbl_ra"></asp:Label><br /><br />

                    <asp:Repeater id="repGroups_autores" runat="server" OnItemDataBound='repGroups_autores_OnItemDataBound'>
                        <ItemTemplate>
                            <div class="doc_link">
                                <div style="float:left;width:14%;height:80px;">
                                    <p>
                                        <img src='<%# "../ficheiros/autores/" + Eval("photo") %>' alt="" height="42px" width="42px" />
                                    </p>
                                </div>
                                <div style="float:left;width:76%; line-height:1.5em;">
                                    <p>
                                        <%# getEditorLink(Eval("ID").ToString(),"0", Eval("name").ToString()) %>
                                        (<small><%# DataBinder.Eval(Container.DataItem, "editor") %> \ total: <%# DataBinder.Eval(Container.DataItem, "total") %></small>)<br />
                                        <small><%# DataBinder.Eval(Container.DataItem, "email") %></small>
                                    </p>
                                </div>
                                <div class="show_on_hover" style="float:left;width:10%; line-height:1.5em;">
                                    <p>
                                        <%# getEditorLink(Eval("ID").ToString(), "1", Eval("name").ToString())%>
                                    </p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                
                </div>
            </div>
            <div style="float:left;width:50%;">
                <div style=" margin: 5px 10px 5px 10px;"><asp:Label Text="<%$Resources:insigte.language,prRstAutores%>" runat="server" ID="lbl_re"></asp:Label><br /><br />

                    <asp:Repeater id="repGroups_editores" runat="server" OnItemDataBound='repGroups_editores_OnItemDataBound'>
                        <ItemTemplate>
                            <div class="doc_link">
                                <div style="float:left;width:10%; height:50px;">
                                    <p>
                                        <img src="Imgs/icons/black/png/doc_lines_stright_icon_32.png" alt="" />
                                    </p>
                                </div>
                                <div style="float:left;width:90%; line-height:1.5em;">
                                    <p>
                                        <%# getPubLink(Eval("ID").ToString(),"0",Eval("name").ToString()) %>
                                        (<small>total: <%# DataBinder.Eval(Container.DataItem, "total") %></small>)<br />
                                        <small><%# DataBinder.Eval(Container.DataItem, "email") %></small>
                                    </p>
                                </div>
                            </div>
                        </ItemTemplate>
                    </asp:Repeater>
                </div>
            </div>
        </div>

    </div>
</div>

</asp:Content>
