﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrevMediaReport.aspx.cs"
    Inherits="Insigte.Media_Measurement.PrevMediaReport" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>

    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();

        $(document).ready(function () {

            if ($.browser.webkit) {
                $($(":hidden[id*='DatePickers']").val().split(",")).each(function (i, item) {
                    var h = $("table[id*='ParametersGrid'] span").filter(function (i) {
                        var v = "[" + $(this).text() + "]";
                        return (v != null && v.indexOf(item) >= 0);
                    }).parent("td").next("td").find("input").datepicker({
                        showOn: "button"
           , buttonImage: '/Reserved.ReportViewerWebControl.axd?OpType=Resource&Name=Microsoft.Reporting.WebForms.calendar.gif'
           , buttonImageOnly: true
           , dateFormat: 'dd/mm/yyyy'
           , changeMonth: true
           , changeYear: true
                    });
                });
            }

        });

    </script>
   
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div style=" background-color:#FFFFFF; width:100%; height:800px;">
            <asp:ScriptManager ID="ScriptManager1" runat="server">
            </asp:ScriptManager>
            <asp:HiddenField ID="DatePickers" runat="server" />
            <rsweb:ReportViewer ID="RV_MediaMeasurement"  runat="server" ProcessingMode="Remote" Font-Names="Verdana" Font-Size="8pt" 
                InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" Width="100%" Height="100%">
               <%-- <ServerReport ReportPath="/Report" />--%>
                
            </rsweb:ReportViewer>
        </div>
    </div>
    </form>
</body>
</html>