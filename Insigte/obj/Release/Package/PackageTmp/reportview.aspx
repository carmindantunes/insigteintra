﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="reportview.aspx.cs" Inherits="Insigte.reportview" %>

<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=10.0.0.0, Culture=neutral, PublicKeyToken=b03f5f7f11d50a3a"
    Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
    <script type="text/javascript">
        var _gaq = _gaq || [];

        _gaq.push(['_setAccount', 'UA-33483716-1']);

        _gaq.push(['_trackPageview']);

        (function () {

            var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;

            ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';

            var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);

        })();
    </script>
    <style type="text/css"> 
        #section.sector
        {
            float:left;
            border :1px solid Black;
            width: 100%;
            min-height: 300px;
            min-width:400px;
            color: #000;
            text-decoration: none;
            font-size: 12px;
            font-family:Arial;
            padding:0px;
            margin:0px;
        }
        .form_search
        {
            float:left;
            width: 100%;
            height:10px;
            padding-top: 4px;
            padding-bottom: 10px;
            margin: 0 auto 20px auto;
            background-color: #323232;
            color: #FFFFFF;
        }    
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <div id="Geral" style="float:left; padding:0px; margin:0px; height:100%;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:8px">
            <asp:Label runat="server" ID="Lb_Tema" Font-Names="Arial" Font-Size="12px" Visible="true" Text="Report Server beta 0.1" />
            <%--<asp:DropDownList ID="ddAdv" Font-Names="Arial" Font-Size="12px" runat="server" AutoPostBack="True" onselectedindexchanged="ddAdv_SelectedIndexChanged"></asp:DropDownList>--%>
        </div>
    </div>
    <div class="form_caja">
        <div style=" background-color:#FFFFFF; width:100%; height:800px;">
            <rsweb:ReportViewer ID="RV_test" runat="server" ProcessingMode="Remote"  Width="100%" Height="100%" Font-Names="Verdana" Font-Size="8pt" 
                InteractiveDeviceInfos="(Collection)" WaitMessageFont-Names="Verdana" WaitMessageFont-Size="14pt" OnSubmittingParameterValues="RV_test_SubmittingParameters">
                <ServerReport ReportPath="/Report" />

            </rsweb:ReportViewer>
        </div>
    </div>
</div>
</asp:Content>
