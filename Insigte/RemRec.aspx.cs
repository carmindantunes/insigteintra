﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Insigte
{
    public partial class RemRec : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string qsIDArtigo = Request.QueryString["ID"];
            RemArt(qsIDArtigo);
        }


        private void RemArt(string qsIDArtigo)
        {
            try
            {
                string ValCookie = string.Empty;
                string DelCookie = string.Empty;
                string PDFCookie = string.Empty;

                HttpCookie cookie = Request.Cookies["CookieinsigteSys"];


                //LINQ
                if (cookie.Values["DELNOT"].ToString().Contains('|'))
                {
                    string[] oldDelCookie = cookie.Values["DELNOT"].ToString().Split('|');
                    string numToRemove = qsIDArtigo;
                    oldDelCookie = oldDelCookie.Where(val => val != numToRemove).ToArray();

                    foreach (string value in oldDelCookie)
                    {
                        DelCookie += value + "|";
                    }
                }

                if (DelCookie != string.Empty)
                {
                    DelCookie = DelCookie.Substring(0, DelCookie.LastIndexOf('|'));
                }
                else
                {
                    DelCookie = "";
                }

                ValCookie = cookie.Values["ID"];
                PDFCookie = cookie.Values["PDF"];

                cookie.Expires = DateTime.Now.AddDays(-1d);
                Response.Cookies.Add(cookie);

                HttpCookie newCookie = new HttpCookie("CookieinsigteSys");
                newCookie["Version"] = "JUL2012";
                newCookie["ID"] = ValCookie;
                newCookie["PDF"] = PDFCookie;
                newCookie["DELNOT"] = DelCookie;
                newCookie.Expires = DateTime.Now.AddDays(360);
                Response.Cookies.Add(newCookie);

                lblArtigoRem.Text = "O artigo de ID: " + qsIDArtigo + ", foi removido com sucesso.";
            }
            catch (Exception exp)
            {
                lblArtigoRem.Text = exp.Message.ToString() + " - " + exp.StackTrace.ToString() + " - " + exp.Data.ToString();
            }
        }
    }
}