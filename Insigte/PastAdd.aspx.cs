﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using APToolkitNET;


namespace Insigte
{
    public partial class PastAdd : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            //if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            //{
            //    Response.Redirect("Default.aspx");
            //}
        }

        protected String getResource(String resx)
        {
            return Resources.insigte.language.ResourceManager.GetObject(resx).ToString();
        }

        protected void btAdicionar_Click(object sender, EventArgs e)
        {

            string cmdQueryPastas = " insert into dbo.CL10D_PASTAS (id_client, id_client_user, nome, des_pasta, dat_criacao, cod_dia_criacao) ";
            cmdQueryPastas += " select " + Global.InsigteManager[Session.SessionID].IdClient + ", " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + ",'" + NomePasta.Text + "', '" + tbNota.Text + "', getdate(), convert(varchar,getdate(),112) ";

            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                cmd.ExecuteNonQuery();

            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
            finally
            {
                conn.Dispose();
                conn.Close();
            }

            Response.Redirect("pastascfg.aspx");
            //Response.Write("<script language='javascript'> { self.close() }</script>");
        }

        protected void cancelar_click(object sender, EventArgs e)
        {
            Response.Redirect("pastascfg.aspx");
        }

    }
}