﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace Insigte
{
    public partial class MediaMeasurement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    txtLtsTemas.Attributes.Add("onclick", "txtTemaClick(this);");
                    //txtLtsTemas.Attributes.Add("onmouseover", "this.style='cursor:pointer;'");
                    //txtLtsTemas.Attributes.Add("onmouseout", "this.class='btnOver'");


                    INInsigteManager.INManager inmanager = Global.InsigteManager[Session.SessionID];

                    string query = "";

                    query += " select CAST(COD_TEMA_ACCESS as varchar(4)) as COD_TEMA_ACCESS,DES_TAB ";
                    query += " from [dbo].[CL10H_CLIENT_SUBJECT] with(nolock) where ID_CLIENT_USER = " + inmanager.inUser.IdClientUser.ToString();
                    query += " and COD_TEMA_ACCESS <> (select COD_CLIENT_TEMA from CL10D_CLIENT with(nolock) where ID_CLIENT = " + inmanager.IdClient.ToString() + "  ) ";
                    query += "order by des_tab ";


                    DataTable dtTabs = inmanager.getTableQuery(query, "TabsForDropDown");

                    preencheDDTemas(dtTabs);
                }
                else
                {
                    lblDataToday.Visible = false;
                }
            }
            catch (Exception er)
            {

                er.Message.ToString();
            }
        }

        private void preencheDDTemas(DataTable tabs)
        {
            try
            {
                if (tabs == null)
                    return;

                ddTab.Items.Add(new System.Web.UI.WebControls.ListItem("selecione uma tab", "-1"));

                foreach (DataRow row in tabs.Rows)
                {
                    ddTab.Items.Add(new System.Web.UI.WebControls.ListItem(row["DES_TAB"].ToString(), row["COD_TEMA_ACCESS"].ToString()));
                }

                ddTab.SelectedValue = "-1";
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }
        }

        protected void ddTab_SelectedIndexChanged(object sender, EventArgs e)
        {

            try
            {
                if (ddTab.SelectedValue != "-1")
                {
                    plTemas.Visible = true;

                    INInsigteManager.INManager inmanager = Global.InsigteManager[Session.SessionID];

                    string query = "";

                    query += " select cast(clienteid as varchar) + cast(subjectid as varchar) as TEMA,name ";
                    query += " from subject with(nolock) where cast(clienteid as varchar)  = cast( " + ddTab.SelectedValue.ToString() + " as varchar(4)) order by name ";

                    DataTable dtTemas = inmanager.getTableQuery(query, "TemasDaTab");

                    if (dtTemas == null)
                        return;

                    if (dtTemas.Rows.Count > 0)
                    {
                        chkList.Items.Clear();

                        foreach (DataRow row in dtTemas.Rows)
                        {
                            System.Web.UI.WebControls.ListItem item = new System.Web.UI.WebControls.ListItem();
                            item.Value = row["tema"].ToString();
                            item.Text = row["name"].ToString();
                            item.Attributes.Add("onclick", "changeColor(this);escreveTxt(this);");
                            chkList.Items.Add(item);
                        }
                    }

                }
            }
            catch (Exception er)
            {

                er.Message.ToString();
            }
        }

        protected void btnPrev_Click(object sender, EventArgs e)
        {
            //if (ddTemas.SelectedValue == "-2")
            //{
            //    lblDataToday.Text = "necessário escolher tema";
            //    lblDataToday.Visible = true;
            //    return;
            //}

        }

        protected void btnTemas_Click(object sender, EventArgs e)
        {
            if (txtLtsTemas.Text.Length == 0)
            {
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "AlertBox", "alert('Texto vazio');", true);
                return;
            }

            try
            {
                string temas = txtLtsTemas.Text.TrimEnd(',');
                string codlg = Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt" ? "_en" : "";
                string inner = "";
                int i = 0;
                if (temas.Contains(','))
                {
                    foreach (string str in txtLtsTemas.Text.TrimEnd(',').Split(','))
                    {
                        i++;
                        inner += trataInnerHtml(str, codlg, i);
                        inner += "<br />";
                    }
                }
                else
                {
                    i++;
                    inner += trataInnerHtml(temas.TrimEnd(',').Split(',')[0], codlg, i);
                }
                divtemas.InnerHtml = inner;
            }
            catch (Exception er)
            {
                er.Message.ToString();
            }

        }
       
        private string trataInnerHtml(string temas, string codlg, int i)
        {
            try
            {
                string query = "";
                query += " select distinct m.insert_date, m.id,left(convert(nvarchar,m.date,111),10)  as date,m.title" + codlg + " as title,m.editor,m.tipo,cast(c.text" + codlg + " as varchar(1000)) + ' ...' as text ";//, s.name" + codlg + " as name";
                query += " from metadata m with(nolock) ";
                query += " join contents c with (nolock) on c.id = m.id  ";
                query += " join clientarticles ca with(nolock) on ca.id = m.id and (cast(ca.clientid as varchar) + cast(ca.subjectid as varchar) ) in (  " + temas + " ) ";
                //query += " join subject s with(nolock) on s.clienteid = ca.clientid and s.subjectid = ca.clientid ";
                query += " and left(convert(varchar,m.date,112),6) >= cast(left(convert(varchar,getdate(),112),6) as int) -1 order by date desc ";

                DataTable dtnwestema = Global.InsigteManager[Session.SessionID].getTableQuery(query, "Newstema");

                string qtema = "select name from subject with(nolock) where cast(clienteid as varchar) + cast(subjectid as varchar) = " + temas;
                DataTable dttema = Global.InsigteManager[Session.SessionID].getTableQuery(qtema, "tema");
                string inner = "";

                inner += "<div class=\"divSpan\"><span style=\"margin-left:10px;\">" + dttema.Rows[0][0].ToString() + "</span></div>";
                inner += "<div id=\"divglob_" + i.ToString() + "\" style=\"float: left; width: 100%; height: 250px; overflow: auto; margin-bottom:10px; margin-top:5px;\">";
                
                int j = 0;

                if (dtnwestema.Rows.Count > 0)
                {
                    foreach (DataRow row in dtnwestema.Rows)
                    {
                        j++;
                        inner += "<div id=\"div_" + row["id"].ToString() + "_" + i.ToString() + "\" class=\"itemNormal\" onmouseover=\"divOnMouseOver(this.id);\" onmouseout=\"divOnMouseOut(this.id);\">";
                        inner += "  <div style=\"margin: 10px;\">";
                        inner += "      <a href='Article.aspx?ID=" + row["id"].ToString() + "' target=\"_self\" title='" + row["text"].ToString() + "'>";
                        inner += "          <span id=\"" + row["title"].ToString() + "\"> ";
                        inner += "             " + row["title"].ToString();
                        inner += "         <br />";
                        inner += "        <span id=\"Span_" + j.ToString() + "\" style=\"font-size: 12px; font-weight: normal;\">";
                        inner += "          <span>" + row["editor"].ToString() + "</span>";
                        inner += "          <span>| </span><span>" + row["date"].ToString() + "</span>";
                        inner += "          <span>| </span><span>" + trataTipo(row["tipo"].ToString()) + "</span>";
                        inner += "        </span>";
                        inner += "     </a>";
                        inner += "     <div>";
                        inner += "          <form> ";
                        inner += "               <input id=\"chk_" + row["id"].ToString() + "_" + i.ToString() + "\" type=\"radio\" name=\"myradio_" + j.ToString() + "\" value=\"1\" onclick=\"targetPos(this.id);\" >:-)";
                        inner += "               <input id=\"chk_" + row["id"].ToString() + "_" + i.ToString() + "\" type=\"radio\" name=\"myradio_" + j.ToString() + "\" value=\"-1\" onclick=\"targetNeg(this.id);\" >:-( ";
                        inner += "               <input id=\"chk_" + row["id"].ToString() + "_" + i.ToString() + "\" type=\"radio\" name=\"myradio_" + j.ToString() + "\" value=\"0\" checked onclick=\"targetNeutro(this.id);\" >none";
                        inner += "          </form> ";
                        inner += "     </div>";
                        inner += "  </div>";
                        inner += "</div>";
                    }
                }
                inner += "</div>";
                return inner;
            }
            catch (Exception er)
            {
                er.Message.ToString();
                return "";
            }
        }

        public string getcolor()
        {
            string cor = Global.InsigteManager[Session.SessionID].CorCliente != "" && Global.InsigteManager[Session.SessionID].CorCliente.ToString().Contains("#") ? Global.InsigteManager[Session.SessionID].CorCliente : "#00add9";
            return cor;
        }


        protected string TransTipo(string tipo)
        {
            return trataTipo(tipo);
        }

        private string trataTipo(string tipo)
        {
            string codlg = Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            if (codlg != "pt")
            {
                switch (tipo)
                {
                    case "Online":
                        return tipo;
                    case "Imprensa":
                        return "Press";
                    case "Rádio":
                        return "Radio";
                    case "Insigte":
                        return tipo;
                    case "Televisão":
                        return "Television";
                    default:
                        return tipo;
                }
            }
            else
                return tipo;
        }

        protected void radioSelectChange(object sender, EventArgs e)
        {

        }

    }
}