﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class sExecutivo : BasePage
    {
        private Boolean LinhaPar = true;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!Global.InsigteManager[Session.SessionID].IsLogged || Global.InsigteManager[Session.SessionID] == null)
            {
                Response.Redirect("login.aspx", true);
            }

            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(6, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            getPublicacoes();

            if (!Page.IsPostBack)
            {
                fillDropDowns();
                
            }

        }

        private void fillDropDowns()
        {
            String SQL = " select * from CL10D_CLIENT with(nolock) where ID_CLIENT in ( select distinct ID_CLIENTE from NW10D_TEMA_SUMARIO_EXECUTIVO with(nolock) ) ";

            DataTable Temas = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "Jesus");


            ddCliente.DataSource = Temas;
            ddCliente.DataBind();
            ddCliente.DataTextField = "NOM_CLIENT";
            ddCliente.DataValueField = "ID_CLIENT";
            ddCliente.DataBind();

        }

        private void getTemasCliente()
        {
            String SQL = " select * from NW10D_TEMA_SUMARIO_EXECUTIVO with(nolock) where ID_CLIENTE = " + ddCliente.SelectedValue;

            DataTable Temas = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "Jesus");

            ddTemas.DataSource = Temas;
            ddTemas.DataBind();
            ddTemas.DataTextField = "DES_TEMA_SUMARIO_EXECUTIVO";
            ddTemas.DataValueField = "ID_TEMA_SUMARIO_EXECUTIVO";
            ddTemas.DataBind();
        }

        protected void ddTemas_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        protected void ddCliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            getTemasCliente();
        }

        public String getLinha()
        {
            String Linha = "linhaX";

            if (LinhaPar)
            {
                Linha = "linhaX";
                LinhaPar = false;
            }
            else
            {
                Linha = "linhaY";
                LinhaPar = true;
            }

            return Linha;
        }

        public String getMainLink(String tipo, String file, String url)
        {
            String Link = "";

            switch (tipo)
            {
                case "Imprensa":
                    String FileHelperPath = "ficheiros";
                    if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                    {
                        if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                        {
                            FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                        }
                    }
                    Link = string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
                case "Online":
                    Link = string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                    break;
                case "Rádio":
                    Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                    break;
                case "Televisão":
                    Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                    break;
                default:
                    Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                    break;
            }

            return Link;
        }

        public String getRelacao(String ID_NEWS)
        {
            String Relacao = "";

            try
            {
                String SQL = "";
                //SQL += " EXEC	[dbo].[getNotRelacao] ";
                //SQL += " @ID_NEWS = " + ID_NEWS;
                SQL += " select * ";
                SQL += "   from NW10F_DAILY_RELATED_NEWS n with(nolock) ";
                SQL += " 	    inner join metadata m with(nolock) ";
                SQL += " 			    on n.ID_NEWS_RELATED = m.id ";
                SQL += "  where ID_NEWS = " + ID_NEWS;

                DataTable ntwRel = Global.InsigteManager[Session.SessionID].getTableQuery(SQL, "Jesus");

                foreach (DataRow x in ntwRel.Rows)
                {
                    String Link = "";
                    String tipo = x["tipo"].ToString();
                    String file = x["filepath"].ToString();
                    String url = x["page_url"].ToString();

                    switch (tipo)
                    {
                        case "Imprensa":
                            String FileHelperPath = "ficheiros";
                            if (Global.InsigteManager[Session.SessionID].CostumPDFLogo == "1")
                            {
                                if (File.Exists(@"G:\cfiles\" + Global.InsigteManager[Session.SessionID].IdClient + "\\" + file.Replace("/", "\\")))
                                {
                                    FileHelperPath = "cficheiros/" + Global.InsigteManager[Session.SessionID].IdClient;
                                }
                            }
                            Link = string.Format("<a href=\"http://insigte.com/" + FileHelperPath + "/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                            break;
                        case "Online":
                            Link = string.Format("<a href=\"{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/globe_1_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Link\" style=\"border-width:0px;\" title=\"Link\" /></a>", url);
                            break;
                        case "Rádio":
                            Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/headphones_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download MP3\" style=\"border-width:0px;\" title=\"Download MP3\" /></a>", file);
                            break;
                        case "Televisão":
                            Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/movie_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download MP4\" style=\"border-width:0px;\" title=\"Download MP4\" /></a>", file);
                            break;
                        default:
                            Link = string.Format("<a href=\"http://insigte.com/ficheiros/{0}\" target=\"_blank\"><img src=\"Imgs/icons/black/png/doc_export_icon_16.png\" width=\"12px\" height=\"12px\" alt=\"Download PDF\" style=\"border-width:0px;\" title=\"Download PDF\" /></a>", file);
                            break;
                    }

                    Relacao += ", " + Link + " " + x["editor"].ToString();
                }
            }
            catch (Exception exp)
            {
                Relacao += " " + exp.Message.ToString() + ",";
            }

            return Relacao.TrimEnd(',');
        }


        private void getPublicacoes()
        {

            String Title = "m.TITLE";
            String Preview = "c.text";
            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                Title = "m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " as TITLE";
                Preview = "c.text_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage;
            }

            String DataToShow = "m.DATE";
            String DataToShowConfig = "m.DATE";


            if (Global.InsigteManager[Session.SessionID].IdClient == "1000" || Global.InsigteManager[Session.SessionID].inUser.IdClientUser == "178")
            {
                DataToShow = "m.insert_date as DATE";
                DataToShowConfig = "m.insert_date";
            }

            String SQLTemas = "SELECT distinct m.ID as IDMETA, " + Title + ", m.EDITOR, " + DataToShow + ", m.filepath, m.page_url, replace(substring(" + Preview + ",1,500),'\"','') + '...' as text, m.tipo, m.editorId";
            SQLTemas += " FROM iadvisers.dbo.metadata m with(nolock) ";
            SQLTemas += " inner join contents c with(nolock) ";
            SQLTemas += " on m.id = c.id ";
            SQLTemas += " INNER JOIN iadvisers.dbo.clientarticles a with(nolock) ";
            SQLTemas += " ON a.id = m.ID WHERE ( ";

            //lblDataToday.Text += "<br>IdUser : " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser;
            //lblDataToday.Text += "<br>IdCliente : " + Global.InsigteManager[Session.SessionID].IdClient;
            try
            {

                foreach (var tema in Global.InsigteManager[Session.SessionID].inUser.Temas)
                {
                    SQLTemas += " ( ";
                    String filtroEditors = (tema.Value.FILTER_EDITORS_HOMEPAGE == "*" ? Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter : tema.Value.FILTER_EDITORS_HOMEPAGE);

                    //lblDataToday.Text += "<br>" + tema.Value.IdTema + ":" + filtroEditors;

                    if (tema.Value.IdTema.Substring(4, 3).ToString() != "000" && tema.Value.SubTemas.Length == 0)
                    {
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid = '" + tema.Value.IdTema.Substring(4, 3).ToString() + "' and convert(varchar," + DataToShowConfig + ",112) >= convert(varchar,getdate(),112)";
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema == tema.Value.SubTemas)
                    {
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and convert(varchar," + DataToShowConfig + ",112) >= convert(varchar,getdate(),112)";
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    if (tema.Value.IdTema.Substring(4, 3).ToString() == "000" && tema.Value.IdTema != tema.Value.SubTemas && tema.Value.IdTema.Length <= tema.Value.SubTemas.Length)
                    {
                        String[] subtemas = tema.Value.SubTemas.Split(';');
                        String AuxTemas = "";
                        foreach (String y in subtemas)
                        {
                            AuxTemas += "'" + y.Substring(4, 3) + "',";
                        }
                        AuxTemas = AuxTemas.TrimEnd(',');
                        SQLTemas += " a.clientid='" + tema.Value.IdTema.Substring(0, 4).ToString() + "' and a.subjectid in (" + AuxTemas + ") and convert(varchar," + DataToShowConfig + ",112) >= convert(varchar,getdate(),112)";
                        if (filtroEditors != "*")
                            SQLTemas += " and m.editorId in ( " + filtroEditors + " ) ";
                    }
                    SQLTemas += ") OR ";
                }
                SQLTemas = SQLTemas.Substring(0, (SQLTemas.Length - 4)) + " ) ";
            }
            catch (Exception e)
            {
            }
            //// FILTRO DAS FONTES GERAIS
            //if (Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter != "*")
            //    SQLTemas += " and m.editorId in ( " + Global.InsigteManager[Session.SessionID].inUser.UserPortalFilter + " ) ";

            if (Global.InsigteManager[Session.SessionID].inUser.CodLanguage != "pt")
            {
                SQLTemas += " and m.TITLE_" + Global.InsigteManager[Session.SessionID].inUser.CodLanguage + " is not null ";
            }


            SQLTemas += " order by " + DataToShowConfig + " desc, IDMETA desc";

            //lblDataToday.Visible = true;
            //lblDataToday.Text = SQLTemas;

            DataTable Public = Global.InsigteManager[Session.SessionID].getTableQuery(SQLTemas, "FilterEditors");
            rep_newsToday.DataSource = Public;
            rep_newsToday.DataBind();
            
        }


    }
}