﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Blogs.aspx.cs" Inherits="Insigte.Blogs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="server">
<script type="text/javascript">
    function onRemoveTag(tag) 
    {
        __doPostBack('__Page', tag);
        //alert(tag);
    }

    $(function () {
        $('textarea[id^="MainContent_ta_editores_codes"]').tagsInput({
                width: 'auto',
                height: '50px',
                interactive: false,
                onRemoveTag: onRemoveTag
        });
     });

        $(function () {
            $("#dialog-form-help_blog").dialog({
                autoOpen: false,
                height: 'auto',
                width: 450,
                modal: true
            });
        });

    $(function () {
            $("#ajuda-blog").click(function () {
                $("#dialog-form-help_blog").dialog("open");
            });
        });



</script>


<style type="text/css">
    #section.sector
    {
        float:left;
        border :1px solid Black;
        width: 100%;
        min-height: 75px;
        min-width:400px;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family:Arial;
        padding:0px;
        margin:0px;
    }
    .form_search
    {
        float:left;
        width: 100%;
        background-color: #323232;
        color: #FFFFFF;
        min-height:22px;
        padding-top:2px;
    }  
    .txt_css
    {
        border-color: White;
        border-width: 0px;
        font-family: Arial;
        font-size: 12px; 
        font-style: normal;
        width: 100%;
    } 
    
    .ui-widget-header  
    {
        border: 1px solid #aaaaaa; 
        color:White;
        font-weight: bold;
        font-size: 12px;
        font-family: Arial;                     
    }
    
    #dialog-form-help_blog
    {
        display:none;
        height:auto;
        color: #000;
        text-decoration: none;
        font-size: 12px;
        font-family: Arial;
    }
     
     #dialog-form-help_blog p.newDesc { color: #000; }
</style>




</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<div id="dialog-form-help_blog" title="Blog help">
	<p class="newDesc">&nbsp;</p>

</div>

<div id="Geral" style="float:left; padding:0px; margin:0px; height:100%; width:800px;">
    <div id="infoText" style="float:left; padding:0px; margin: 2px 0px 2px 0px; height:35px; min-height:35px; width:100%"> 
        <div style="margin-top:12px">
            <asp:Label runat="server" ID="lblDataToday" Font-Names="Arial" Font-Size="12px" Visible="false"  />
        </div>
    </div>    

    <div id="section" class="sector ui-corner-top" style="float:left;width:100%; height:100%; margin-bottom:10px;">
        <div id="titulo" class="form_search headbg ui-corner-top">
            <div id="caixa" style="float:left;width:80%;">
                &nbsp;<asp:Label runat="server" ID="lblTitleAdvSearch" Font-Names="Arial" Font-Size="12px" ForeColor="#FFFFFF" Text=" Tags" />
            </div> 
            <div id="help" style="float:left;width:20%; ">
                <div style="float:right;padding-right:5px; ">
                    <a id="ajuda-blog" href="#">
                        <img alt="ajuda" src="Imgs/icons/white/png/info_icon_16.png" />
                    </a>
                </div>
            </div>
        </div>
        <div id="formSearch" style="float:left;background-color:#ffffff; width:100%;">
            <div id="texto"  style="width:100%;">
                <div>
                <div style="float:left; margin:5px 0px 0px 5px;">
                       <asp:DropDownList runat="server" ID="DropDl_select_Tema" AutoPostBack="true" OnSelectedIndexChanged="DropDl_select_Tema_change" />
                    </div>
                    <div style="float:left; margin:5px 0px 0px 5px;">
                       <asp:DropDownList runat="server" ID="DropDl_select_tag" AutoPostBack="true" OnSelectedIndexChanged="DropDl_select_tag_change"  Visible="false" /> 
                       <%--<asp:DropDownList runat="server" ID="lst_tagsRmv"  Visible="false" /> --%>
                    </div>
                    <div style="float:left; margin:5px 0px 0px 5px;">
                       <asp:DropDownList runat="server" ID="DropDl_select_Blog" AutoPostBack="true" OnSelectedIndexChanged="DropDl_select_Blog_change" />
                     <%--  <asp:DropDownList runat="server" ID="lst_blogsRmv"  Visible="false" /> --%>
                    </div>
                    <div id="SelectedEditors" style="float:left; width:98%; margin:5px 0px 5px 5px;">
                        <div class="ui-corner-all in-cli-small-search2" style=" width:auto; height:100%">
                            <textarea cols="50" rows="2" runat="server" ID="ta_editores_codes"></textarea>
                            <input type="hidden" runat="server" id="txt_tags" />
                            <input type="hidden" runat="server" id="txt_blogs" />
                           <%-- <input type="hidden" runat="server" id="txt_tagsRmv" />
                            <input type="hidden" runat="server" id="txt_blogsRmv" />--%>
                        </div>
                     <%--   <div style="float:left; margin:5px 0px 5px 5px; float:right; visibility:hidden;">
                            <asp:LinkButton ID="btn_blog_search" runat="server" Text="Search" CssClass="btn-pdfs" ></asp:LinkButton>
                        </div>--%>
                    </div>
                </div>
            </div>
            
        </div>
    </div>

     <div class="form_caja">
        <asp:DataGrid runat="server" ID="dgNewsBLOG" AutoGenerateColumns="false" AllowPaging="True" 
            GridLines="None" BorderWidth="0px" BorderStyle="None" CellPadding="0" CellSpacing="0"
            Width="800px" Font-Names="Arial" Font-Size="11px" PageSize="40" AlternatingItemStyle-BackColor="#EEEFF0"
            PagerStyle-BackColor="#8D8D8D" PagerStyle-Font-Size="12px" OnPageIndexChanged="dgNewsBLOG_PageIndexChanged"
            PagerStyle-ForeColor="#ffffff" PagerStyle-Font-Underline="false" PagerStyle-VerticalAlign="Middle"
            PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="NumericPages">
            <ItemStyle BackColor="White" Height="24px" />
            <AlternatingItemStyle  BackColor="#EEEFF0" />
            <HeaderStyle BackColor="#8D8D8D" Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
            <FooterStyle Height="24px" Font-Names="Arial" Font-Size="12px"  CssClass="headbg" />
            <Columns>
                   
                 <asp:TemplateColumn HeaderText="&nbsp; <%$Resources:insigte.language,defColTitulo%> " HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Justify" 
                    ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left">
                    <ItemTemplate>&nbsp;
                        <a href="<%# string.Format("ArticleBlog.aspx?ID={0}", DataBinder.Eval(Container.DataItem, "ID_NEWS")) %>"
                            target="_self" style="color: #000000; text-decoration: none;" onmouseout="this.style.textDecoration='none';"
                            onmouseover="this.style.textDecoration='underline';">
                       
                            <%# DataBinder.Eval(Container.DataItem, "TITULO").ToString() %>
                     </a>
                    </ItemTemplate>
                </asp:TemplateColumn>

                <asp:BoundColumn DataField="BLOG" HeaderText="Blogue" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />

                <asp:BoundColumn DataField="AUTOR" HeaderText="<%$Resources:insigte.language,defColTitulo%>" HeaderStyle-HorizontalAlign="center"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" Visible="false" />

                <asp:BoundColumn DataField="DATA" HeaderText="<%$Resources:insigte.language,defColData%>" DataFormatString="{0:dd-MM-yyyy}" HeaderStyle-HorizontalAlign="Center"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Center"
                    ItemStyle-Width="100px" />

                 <asp:TemplateColumn HeaderText="<%$Resources:insigte.language,defColAccoes%>" HeaderStyle-ForeColor="white" ItemStyle-HorizontalAlign="Center" 
                        ItemStyle-ForeColor="#000000" HeaderStyle-HorizontalAlign="Left" HeaderStyle-VerticalAlign="Middle" >
                <ItemTemplate>
                    <center>
                    <a   href="<%# DataBinder.Eval(Container.DataItem, "URL") %>"
                        target="blank" style="color: #000000; text-decoration: none;" onmouseout="this.style.textDecoration='none';">
                        <img  src="Imgs/icons/black/png/link_icon_16.png" alt="Link"/>
                       
                       <%-- <%# DataBinder.Eval(Container.DataItem, "TITULO").ToString() %>--%>
                    </a>
                    </center>
                </ItemTemplate>
            </asp:TemplateColumn>

    
                <%--<asp:BoundColumn DataField="EDITOR" HeaderText="ACÇÕES" HeaderStyle-HorizontalAlign="Left"
                    HeaderStyle-ForeColor="white" ItemStyle-ForeColor="#000000" ItemStyle-HorizontalAlign="Left" />--%>
            </Columns>
        </asp:DataGrid>
    </div>

</div>


<style type="text/css">
   
    div.tagsinput { border:0px solid #fff; background: #FFF; padding:5px; width:300px; height:200px; overflow-y: auto;}
    
</style>

</asp:Content>
