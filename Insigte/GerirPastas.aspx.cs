﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using AjaxControlToolkit;

namespace Insigte
{
    public partial class GerirPastas : BasePage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Global.InsigteManager[Session.SessionID].inUser.UserVisability.Substring(0, 1) == "0")
            {
                Response.Redirect("Default.aspx");
            }

            if (!Page.IsPostBack)
            {
                getPastas();
            }
        }


        public string getPastaID()
        {
            if (tvPastas.SelectedIndex < 0)
                return string.Empty;
            else
                return tvPastas.SelectedItem.Value;
        }

        public void getPastas()
        {

            tvPastas.Visible = true;

            string cmdQueryPastas = "select id, nome from dbo.CL10D_PASTAS with(nolock) where id_client = " + Global.InsigteManager[Session.SessionID].IdClient + " and id_client_user = " + Global.InsigteManager[Session.SessionID].inUser.IdClientUser + " ";
            SqlConnection conn = new SqlConnection("Data Source=167.114.209.37;Initial Catalog=iadvisers;User ID=in_app;Password=in.app.2013");
            try
            {
                conn.Open();

                SqlCommand cmd = new SqlCommand(cmdQueryPastas);
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;

                DataSet tempSet;
                SqlDataAdapter iadvisers_ACN_PASTAS;

                iadvisers_ACN_PASTAS = new SqlDataAdapter(cmd);

                tempSet = new DataSet("tempSet");
                iadvisers_ACN_PASTAS.Fill(tempSet);

                tvPastas.DataSource = tempSet;
                tvPastas.DataBind();
                tvPastas.DataTextField = "nome";
                tvPastas.DataValueField = "id";
                tvPastas.DataBind();
                tvPastas.Visible = true;
            }
            catch (Exception exp)
            {
                exp.Message.ToString();
            }
        }
    }
}