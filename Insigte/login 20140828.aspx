﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="login.aspx.cs" Inherits="Insigte.login" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

<meta content="pt" http-equiv="Content-Language" />
<meta content="text/html; charset=utf-8" http-equiv="Content-Type" />
<title>insigte | media intelligence</title>

<script type="text/javascript"> !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js "; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>

<link rel="icon" type="ico" href="Imgs/insigte_icon.ico" />

<script type="text/javascript"> !function (d, s, id) { var js, fjs = d.getElementsByTagName(s)[0]; if (!d.getElementById(id)) { js = d.createElement(s); js.id = id; js.src = "//platform.twitter.com/widgets.js "; fjs.parentNode.insertBefore(js, fjs); } } (document, "script", "twitter-wjs");</script>

<style type="text/css">
    .auto-style1 {
	    text-align: left;
	    background-image: url('Imgs/fundo_default_branco_logo.png');
    }
    .auto-style2 {
	    text-align: left;
	    margin-left: 40px;
    }
    .auto-style3 {
	    text-align: left;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: x-large;
	    color: #FFFFFF;
	    margin-left: 40px;
    }
    .auto-style5 {
	    text-align: left;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
    }
    .auto-style6 {
	    text-align: left;
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
	    color: #FFFFFF;
    }
    .auto-style7 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
	    color: #FFFFFF;
    }
    .auto-style8 {
	    font-family: Arial, Helvetica, sans-serif;
	    font-size: medium;
    }
    .auto-style9 {
	    color: #00ADD9;
	    font-size: 24pt;
	    font-family: Verdana;
    }
    .auto-style11 {
	    background-color: #333333;
    }
    .auto-style13 {
	    text-align: right;
    }
    .auto-style14 {
	    font-family: Arial, Helvetica, sans-serif;
	    color: #FFFFFF;
	    font-size: 11pt;
    }
    .auto-style22 {
	    color: #00ADD9;
	    font-size: 24pt;
	    font-family: Calibri;
    }
    .auto-style23 {
	    font-size: 28pt;
	    font-family: Calibri;
    }
    .auto-style24 {
	    border-width: 0px;
    }
    a {
	    color: #00ADD9;
    }
    .auto-style25 {
	    text-align: left;
    }
    .auto-style26 {
	    text-align: left;
	    color: #FFFFFF;
	    font-family: calibri;
	    font-size: 16pt;
    }
    .auto-style27 {
	    color: #FFFFFF;
	    font-family: calibri;
    }
    .auto-style28 {
	    border-width: 0px;
	    margin-left: 0px;
    }
    .auto-style30 {
	    font-family: calibri;
	    font-size: 26pt;
	    color: #FFFFFF;
    }
    .auto-style31 {
	    color: #FFFFFF;
	    font-family: calibri;
	    font-size: 10pt;
    }
    .auto-style32 {
	    background-color: #808080;
    }
    .auto-style33 {
	    background-color: #505050;
    }
    .auto-style35 {
	    font-size: 12pt;
    }
    .auto-style38 {
	    text-align: left;
	    font-family: calibri;
	    font-size: 14pt;
    }
    .auto-style40 {
	    background-color: #333333;
    }
    .auto-style41 {
	    text-align: right;
	    font-size: large;
    }
    .auto-style42 {
	    color: #FFFFFF;
	    font-size: small;
	    font-family: calibri;
    }
    .auto-style43 {
	    font-family: calibri;
	    font-size: xx-large;
	    color: #FFFFFF;
    }
    .auto-style44 {
	    font-family: calibri;
	    font-size: 39pt;
	    color: #FFFFFF;
    }
        
    .styleB
    {
        width:18%;
        background-color:#808080;
        padding:20px;
        color:White;
        font-family:Calibri;
        background-image:url('Imgs/bg_login_quadrados.png'); 
        background-repeat:repeat;
    }
    styleSpan
    {
      height:100px;
    }
</style>

</head>
<body style="margin-top: 0;">
<a href="https://plus.google.com/107453152500726127422" rel="publisher"></a>
<form id="login" runat="server">

<table align="center" cellpadding="0" cellspacing="0" style="width: 1000px; height: 530px">
	<tr>
		<td class="auto-style1" valign="top">
		<table cellpadding="0" cellspacing="4" style="width: 100%">
			<tr>
				<td style="width: 369px">&nbsp;</td>
				<td style="width: 279px">&nbsp;</td>
				<td class="auto-style13"><br />
				    <span class="auto-style14">

                    <a href="https://www.linkedin.com/company/insigte" target="_blank"><img alt="" height="32" src="Imgs/linkedinwhite.png" width="32" class="auto-style24" /></a>
                    <a href="https://plus.google.com/107453152500726127422" target="_blank"><img alt="" height="32" src="Imgs/googlepluswhite.png" width="32" class="auto-style24" /></a>
                   

                    <%-- <a href="http://www.facebook.com/Insigte" target="_blank"><img alt="" height="32" src="Imgs/fb_white.png" width="32" class="auto-style24" /></a> --%>
                    <%-- <a href="http://twitter.com/insigte"><img alt="" height="32" src="Imgs/tw_white.png" width="32" class="auto-style24" /></a>--%> 
				    <%-- <a href="http://www.youtube.com/insigteconsulting"><img alt="" height="32" src="Imgs/yt_white.png" width="32" class="auto-style24" /></a>--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;</span>
                </td>
			</tr>
			<tr>
				<td style="width: 369px">&nbsp;</td>
				<td style="width: 279px">&nbsp;</td>
				<td class="auto-style13">
				    &nbsp;</td>
			</tr>
			<tr>
				<td style="width: 369px; height: 482px">
				<p class="auto-style2">
				    &nbsp;</p>
				<p class="auto-style2">
				    &nbsp;</p>
				<p class="auto-style2">
				&nbsp;&nbsp;&nbsp;
				<img alt="" height="108" src="Imgs/lampada_4.png" width="90" /></p>
				<p class="auto-style3"><span class="auto-style23"><strong>
				<span class="auto-style9">&nbsp;&nbsp; </span></strong>Área de 
				Clientes</span><br />
				<strong>
				<span class="auto-style9">&nbsp;&nbsp; </span>
				<span class="auto-style22">LOGIN</span></strong></p>
				
				<div align="center">
					<table border="0" width="230">
						<tr>
							<td class="auto-style6">utilizador</td>
							<td style="width: 88px"><input type="text" id="txt_user" runat="server" name="logas" size="10"/></td>
							<td>&nbsp;</td>
						</tr>
						<tr>
							<td class="auto-style5"><span class="auto-style7">
							código</span><span class="auto-style8">&nbsp;</span></td>
							<td style="width: 88px"><input type="password" id="txt_pass" runat="server" name="passas" size="10"/></td>
							<td>
                                <asp:ImageButton runat="server" ID="Ib_login" ImageUrl="Imgs/site_ok.png" OnClick="Ib_login_Click" />
							</td>
						</tr>
						<tr>
							<td>&nbsp;</td>
							<td style="width: 88px">
								<p align="left">
								
								&nbsp;</p>
							</td>
							<td>
								</td>
						</tr>
					</table>
				</div>

				
				</td>
				<td style="height: 482px; width: 279px;"></td>
				<td style="height: 482px" class="auto-style13" valign="top">
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<br />
				<span class="auto-style30"><br />
				<br />
				</span>
				<span class="auto-style43">
				<br />
				</span>
				<span class="auto-style44">
				<br />
				<br />
				</span>
				<span class="auto-style27"><br class="auto-style35" />
				</span>
				<span class="auto-style30">media intelligence</span>&nbsp;&nbsp;&nbsp;
				&nbsp;&nbsp;&nbsp;</td>
			</tr>
			</table>
		<br />
		</td>
	</tr>
</table>
<table align="center" class="auto-style11" style="width: 1000px">
	<tr>
		<td class="auto-style31" style="height: 28px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		collecting small pieces to deliver the Big Picture</td>
	</tr>
</table>
<table align="center" cellpadding="0" cellspacing="0" style="width: 1000px" class="auto-style32">
	<tr>
		<td class="auto-style33">
		<div class="auto-style41">
			&nbsp;</div>
		
		<%--<asp:ScriptManager ID="script" runat="server" ></asp:ScriptManager>--%>
		
		<table align="center" style="width: 85%">
			<tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Angola</div>
                   <%-- <asp:Timer ID="timerAO" OnTick ="timerAO_Tick" runat="server" Interval="1000"></asp:Timer>
                    <asp:UpdatePanel ID="UpdatePanel1" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAo" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>Cabo Verde</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel2" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmMz" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div>Moçambique</div>
                  <%--  <asp:UpdatePanel ID="UpdatePanel3" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmPt" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5AO() %>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5CV() %>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		 <%= getTop5MZ() %>

			    </td>
			</tr>
            <tr>
                <td><br /></td>
            </tr>

            <tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Portugal</div>
                    <%--<asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAS" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>África do Sul</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmNB" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div>Botswana</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_aux" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5PT()%>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5AS()%>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		
        <%= getTop5BW()%>

			    </td>
			</tr>
            <tr>
                <td><br /></td>
            </tr>
            <tr>
				<td class="auto-style26" style="width: 30%; height: 30px" valign="top">
                    <div>Namibia</div>
                    <%--<asp:UpdatePanel ID="UpdatePanel4" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmAS" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                    
				</td>
				<td class="auto-style38" style="width: 15px; height: 30px" valign="top">
				    &nbsp;</td>
				<td class="auto-style26" style="width: 30%; height: 30px;" valign="top">
				    <div>Zimbabwe</div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel5" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_tmNB" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
				<td style="width: 15px; height: 30px;" class="auto-style38" valign="top">
				    &nbsp;</td>
				<td style="width: 30%; height: 30px;" valign="top"" class="auto-style26">
                    <div></div>
                   <%-- <asp:UpdatePanel ID="UpdatePanel6" UpdateMode="Conditional" runat="server">
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="timerAO" EventName="Tick" />
                        </Triggers>
                        <ContentTemplate>
                            <div style="font-size: 14pt;"><asp:Label runat="server" ID="lb_aux" Text="" ForeColor="#333333"></asp:Label> </div>
                        </ContentTemplate>
                    </asp:UpdatePanel>--%>
                </td>
			</tr>
			<tr>
				<td style="width: 30%" class="auto-style25" valign="top">		
		
		<%= getTop5NB()%>
				
				</td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" valign="top" class="auto-style25">
				
        <%= getTop5ZB()%>

                </td>
				<td style="width: 15px" class="auto-style25" valign="top">&nbsp;</td>
				<td style="width: 30%" class="auto-style25" valign="top">
		

			    </td>
			</tr>

		</table>
		<br />
		</td>
	</tr>
    
    
</table>
<table align="center" cellpadding="0" cellspacing="0" style="width: 1000px">
    
    <tr style="background-color:#E5E5E5;">
        <td valign="top">
            <img src="Imgs/BannerFinal.png" height="225px" 
                style="margin-top:60px; margin-bottom:60px; margin-left:50px; width: 900px;" alt=""/>
        </td>
    </tr>
    <tr >
        <td style="background-color:#E5E5E5;">
            <table align="center" style="width:85%;" >
                <tr style="background-color:#E5E5E5; text-align:center" valign="middle" >
                    <td  class="styleB"><span>antecipar necessidades & tendências</span></td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB"><span>monitorizar o ambiente competitivo</span></td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB"><span>acompanhar a actividade dos clientes & parceiros</span></td>
                    <td style="width:2.5%;"></td>
                    <td  class="styleB"><span>conhecer o ROI da comunicação</span></td>
                    <td style="width:2.5%;"><br /><br /><br /><br /><br /><br /></td>
                    <td class="styleB" ><span>defender a reputação e aferir os resultados</span></td>
                </tr>
                <tr">
                    <td style="height:15px;"></td>
                </tr>
                <tr style="background-color:#E5E5E5; text-align:center" valign="middle">
                    <td class="styleB"><span>analisar o comportamento da marca</span></td>
                    <td style="width:2.5%;"></td>
                    <td  class="styleB"><span>recepcionar a informação em tempo real</span></td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB" >newsletters aproveitando o good will da informação</td>
                    <td style="width:2.5%;"></td>
                    <td class="styleB"><span>detectar novas oportunidades de negócio</span></td>
                    <td style="width:2.5%;"><br /><br /><br /><br /><br /><br /></td>
                    <td  class="styleB"><span>THE BIG PICTURE</span></td>                    
                </tr>
                <tr >
                    <td style="height:15px;"></td>
                </tr>   
            </table>
        </td>
    </tr>
    <%--<tr style="background-color:#E5E5E5;">
        <td valign="top"  style=" text-align:center; color:#505050; font-family:Calibri;" >
            <div style=" margin:20px 1px 20px 1px;  ">
                <span  style="font-size:18pt;">análise de impacto | performance da marca | gestão da reputação</span>
            </div>
        </td>
    </tr>--%>
</table>
<table align="center" cellpadding="0" cellspacing="0" class="auto-style40" style="width: 1000px">
	<tr>
		<td class="auto-style31" style="height: 46px">
				<span class="auto-style31">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; insigte ™ | 2014 | insigte is a registered trademark of one vision consulting</span><br />
		</td>
		<td class="auto-style42" style="height: 46px; text-align: right;">
				    <span class="auto-style14">

                    <a href="https://www.linkedin.com/company/insigte" target="_blank"><img alt="" height="32" src="Imgs/linkedinwhite.png" width="32" class="auto-style24" /></a>
                    <a href="https://plus.google.com/107453152500726127422" target="_blank"><img alt="" height="32" src="Imgs/googlepluswhite.png" width="32" class="auto-style24" /></a>
                    
                    <%-- <a href="http://www.facebook.com/Insigte" target="_blank"><img alt="" height="32" src="Imgs/fb_white.png" width="32" class="auto-style24" /></a> --%>
                    <%-- <a href="http://twitter.com/insigte"><img alt="" height="32" src="Imgs/tw_white.png" width="32" class="auto-style24" /></a>--%> 
				    <%-- <a href="http://www.youtube.com/insigteconsulting"><img alt="" height="32" src="Imgs/yt_white.png" width="32" class="auto-style24" /></a>--%>
                    &nbsp;&nbsp;&nbsp;&nbsp;</span>
        </td>
	</tr>
</table>
   <%-- <table align="center" cellpadding="0" cellspacing="0" class="style1">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>--%>
</form>
</body>
</html>
